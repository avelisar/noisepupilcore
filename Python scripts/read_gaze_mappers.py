#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics


path='C:\\Users\\avelisar\\Desktop\\eye_test_Hfree\\offline_data\\'
file_name='gaze_mappers.msgpack'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name, 'rb')
unpacker = msgpack.Unpacker(f, raw=False)

with open(path + 'gaze_mappers.csv', mode='w') as write_file:
    ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    ref_writer.writerow(['GMID', 'GMname', 'CalibID', 'RangeRefNOmin', 'RangeRefNOmax','RangeValidRefNOmin','RangeValidRefNOmax','outlierThr','AccuracyError','PrecisionError'])    
    for unpacked in unpacker:
        msg=unpacked
        ref_data=msg['data']
        
        # GMID=ref_data[0][0]
        # GMname=ref_data[0][1]
        # CalibID=ref_data[0][2]
        # Range_refNOmin=ref_data[0][3][0]
        # Range_refNOmax=ref_data[0][3][1]
        # Range_valid_refNOmin=ref_data[0][4][0]
        # Range_valid_refNOmax=ref_data[0][4][1]
        # outlier_thrsh=ref_data[0][5]
        # accuracy_error=ref_data[0][10]
        # precision_error=ref_data[0][11]
        for data in ref_data:
            GMID=data[0]
            GMname=data[1]
            CalibID=data[2]
            Range_refNOmin=data[3][0]
            Range_refNOmax=data[3][1]
            Range_valid_refNOmin=data[4][0]
            Range_valid_refNOmax=data[4][1]
            outlier_thrsh=data[5]
            accuracy_error=data[10]
            precision_error=data[11]
            ref_writer.writerow([GMID, GMname, CalibID, Range_refNOmin, Range_refNOmax, Range_valid_refNOmin, Range_valid_refNOmax, outlier_thrsh, accuracy_error, precision_error ])