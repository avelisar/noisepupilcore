#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics


#path='Z:\\DATA\\multiple validations pupil check\\preprocessed\\KPA\\head free\\far viewing\\'
path='C:\\Users\\avelisar\\Desktop\\eye_test_Hfree\\offline_data\\'
file_name='reference_locations.msgpack'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name, 'rb')
unpacker = msgpack.Unpacker(f, raw=False)


with open(path + 'refernce_locations.csv', mode='w') as write_file:
    ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    ref_writer.writerow(['RefPosX', 'RefPosY', 'RefNo', 'TimeStamp'])
    
    for unpacked in unpacker:
        msg=unpacked
        ref_data=msg['data']
        for data in ref_data:
            cam_pos=data[0]
            cam_x=cam_pos[0]
            cam_y=cam_pos[1]
            ref_no=data[1]
            timeStamp=data[2]
            ref_writer.writerow([cam_x, cam_y, ref_no, timeStamp])