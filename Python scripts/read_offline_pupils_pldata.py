# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics
import numpy as np

# path='/home/imu/Anca_tests/heave motion on platform/2021_01_27/001/offline_data/'
# file_name='offline_pupil.pldata'
# #path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
# #file_name='hs03_eyeTracker_20190327-144007'
# #f=open(path + file_name + '.msgpack', 'rb')
# f=open(path + file_name, 'rb')
# unpacker = msgpack.Unpacker(f, raw=False)
# for topic, payload in msgpack.Unpacker(f, raw=False, use_list=False):
#          print(topic)
#          print(Serialized_Dict(msgpack_bytes=payload))
         # for b in c:
         #     print(b)
         #for pupil_data in msg:
             
             #print(pupil_data[0:4])

# with open(path + 'refernce_locations.csv', mode='w') as write_file:
#     ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

#     ref_writer.writerow(['RefPosX', 'RefPosY', 'RefNo', 'TimeStamp'])
    
#     for unpacked in unpacker:
#         msg=unpacked
#         ref_data=msg['data']
#         for data in ref_data:
#             cam_pos=data[0]
#             cam_x=cam_pos[0]
#             cam_y=cam_pos[1]
#             ref_no=data[1]
#             timeStamp=data[2]
#             ref_writer.writerow([cam_x, cam_y, ref_no, timeStamp])

class _Empty(object):
    def purge_cache(self):
        pass
class _FrozenDict(dict):
    def __setitem__(self, key, value):
        raise NotImplementedError('Invalid operation')

    def clear(self):
        raise NotImplementedError()

    def update(self, *args, **kwargs):
        raise NotImplementedError()


class Serialized_Dict(object):
    __slots__ = ['_ser_data', '_data']
    cache_len = 100
    _cache_ref = [_Empty()] * cache_len
    MSGPACK_EXT_CODE = 13

    def __init__(self, python_dict=None, msgpack_bytes=None):
        if type(python_dict) is dict:
            self._ser_data = msgpack.packb(python_dict, use_bin_type=True,
                                           default=self.packing_hook)
        elif type(msgpack_bytes) is bytes:
            self._ser_data = msgpack_bytes
        else:
            raise ValueError("Neither mapping nor payload is supplied or wrong format.")
        self._data = None

    def _deser(self):
        if not self._data:
            self._data = msgpack.unpackb(self._ser_data, raw=False, use_list=False,
                                         object_hook=self.unpacking_object_hook,
                                         ext_hook=self.unpacking_ext_hook)
            self._cache_ref.pop(0).purge_cache()
            self._cache_ref.append(self)

    @classmethod
    def unpacking_object_hook(self,obj):
        if type(obj) is dict:
            return _FrozenDict(obj)

    @classmethod
    def packing_hook(self, obj):
        if isinstance(obj, self):
            return msgpack.ExtType(self.MSGPACK_EXT_CODE, obj.serialized)
        raise TypeError("can't serialize {}({})".format(type(obj), repr(obj)))

    @classmethod
    def unpacking_ext_hook(self, code, data):
        if code == self.MSGPACK_EXT_CODE:
            return self(msgpack_bytes=data)
        return msgpack.ExtType(code, data)

    def purge_cache(self):
        self._data = None

    @property
    def serialized(self):
        return self._ser_data

    def __setitem__(self, key, item):
        raise NotImplementedError()

    def __getitem__(self, key):
        self._deser()
        return self._data[key]

    def __repr__(self):
        self._deser()
        return 'Serialized_Dict({})'.format(repr(self._data))

    @property
    def len(self):
        '''Replacement implementation for __len__
        If __len__ is defined numpy will recognize this as nested structure and
        start deserializing everything instead of using this object as it is.
        '''
        self._deser()
        return len(self._data)

    def __delitem__(self, key):
        raise NotImplementedError()

    def get(self,key,default):
        try:
            return self[key]
        except KeyError:
            return default

    def clear(self):
        raise NotImplementedError()

    def copy(self):
        self._deser()
        return self._data.copy()

    def has_key(self, k):
        self._deser()
        return k in self._data

    def update(self, *args, **kwargs):
        raise NotImplementedError()

    def keys(self):
        self._deser()
        return self._data.keys()

    def values(self):
        self._deser()
        return self._data.values()

    def items(self):
        self._deser()
        return self._data.items()

    def pop(self, *args):
        raise NotImplementedError()

    def __cmp__(self, dict_):
        self._deser()
        return self._data.__cmp__(dict_)

    def __contains__(self, item):
        self._deser()
        return item in self._data

    def __iter__(self):
        self._deser()
        return iter(self._data)
    
    
path='Z:\\DATA\\TVOR\\Preprocessed data\\TVOR heave\\MIS\\session1\\far csv files\\'
file_name='offline_pupil.pldata'
ts_file='offline_pupil_timestamps.npy'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name, 'rb')
data_ts = np.load(path + ts_file)
i=0
with open(path + 'offline_pupil_data.csv', mode='w') as write_file:
    ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    ref_writer.writerow(['topic', 'PupilEllipseCenterX', 'PupilEllipseCenterY', 'PupilEllipseAxesX', 'PupilEllipseAxesY','PupilEllipseAngle',\
                         'PupilDiam','PupilLocPixX', 'PupilLocPixY', 'Confidence','PupilNormPosX', 'PupilNormPosY','Eye','Method','TimeStamp',\
                             'PupilSphereCentrX','PupilSphereCentrY','PupilSphereCentrZ','PupilSphereRadius',\
                                 'PupilProjSphereCentrX','PupilProjSphereCentrY','PupilProjSphereAxesX','PupilProjSphereAxesY','PupilProjSphereAngle',\
                                     'PupilCircle3dCentrX','PupilCircle3dCentrY','PupilCircle3dCentrZ','PupilCircle3dNormalX',\
                                         'PupilCircle3dNormalY','PupilCircle3dNormalZ','PupilCircle3dRadius',\
                                             'PupilDiam3d','PupilTheta','PupilPhi','data_ts'])
 
    for topic, payload in msgpack.Unpacker(f, raw=False, use_list=False):
        frame=Serialized_Dict(msgpack_bytes=payload)
        Pmethod=frame['method']
        if not Pmethod=='2d c++':
                
            PellCntrX=frame['ellipse']['center'][0]
            PellCntrY=frame['ellipse']['center'][1]
            PellAxX=frame['ellipse']['axes'][0]
            PellAxY=frame['ellipse']['axes'][1]
            PellAng=frame['ellipse']['angle']
            Pdiam=frame['diameter']
            PpixX=frame['location'][0]
            PpixY=frame['location'][1]
            Pconf=frame['confidence']
            PnormX=frame['norm_pos'][0]
            PnormY=frame['norm_pos'][1]
            timeStamp=frame['timestamp']
            PwhichEye=frame['id']
            Pmethod=frame['method']
            PsphCntrX=frame['sphere']['center'][0]
            PsphCntrY=frame['sphere']['center'][1]
            PsphCntrZ=frame['sphere']['center'][2]
            PsphRadius=frame['sphere']['radius']
            PprjsphCntrX=frame['projected_sphere']['center'][0]
            PprjsphCntrY=frame['projected_sphere']['center'][1]
            PprjsphAxX=frame['projected_sphere']['axes'][0]
            PprjsphAxY=frame['projected_sphere']['axes'][1]
            PprjsphAng=frame['projected_sphere']['angle']
            Pcirc3dCntrX=frame['circle_3d']['center'][0]
            Pcirc3dCntrY=frame['circle_3d']['center'][1]
            Pcirc3dCntrZ=frame['circle_3d']['center'][2]
            Pcirc3dNormX=frame['circle_3d']['normal'][0]
            Pcirc3dNormY=frame['circle_3d']['normal'][1]
            Pcirc3dNormZ=frame['circle_3d']['normal'][2]
            Pcirc3dRadius=frame['circle_3d']['radius']
            Pdiam3d=frame['diameter_3d']
            Ptheta=frame['theta']
            Pphi=frame['phi']
            ts=data_ts[i]
            i=i+1
            # print(frame['theta'])   
            ref_writer.writerow([topic, PellCntrX, PellCntrY, PellAxX, PellAxY, PellAng, Pdiam,\
                                 PpixX, PpixY, Pconf, PnormX, PnormY, PwhichEye, Pmethod, timeStamp,\
                                     PsphCntrX, PsphCntrY, PsphCntrZ, PsphRadius, PprjsphCntrX, PprjsphCntrY,\
                                         PprjsphAxX, PprjsphAxY, PprjsphAng, Pcirc3dCntrX, Pcirc3dCntrY,\
                                             Pcirc3dCntrZ, Pcirc3dNormX, Pcirc3dNormY, Pcirc3dNormZ,\
                                                 Pcirc3dRadius, Pdiam3d, Ptheta, Pphi, ts])

        # print(topic)
        # print(frame['method'])
        # print(frame.keys())   