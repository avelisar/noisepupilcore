# -*- coding: utf-8 -*-
"""
Created on Thu Nov 11 06:00:47 2021

@author: avelisar
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics


path='C:\\Users\\avelisar\\Desktop\\000\\'
file_name='Integrated_Webcam_FHD.intrinsics'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name, 'rb')
unpacker = msgpack.Unpacker(f, raw=False)
def test(dictt):
    result = list(map(list, dictt.items()))
    return result

# with open(path + 'gaze_mappers.csv', mode='w') as write_file:
#     ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

#     ref_writer.writerow(['RefPosX', 'RefPosY', 'RefNo', 'TimeStamp'])
    
for unpacked in unpacker:
    msg=unpacked
    version=msg['version']
    cam_data=test(msg)[1][1]
    cam_mat=cam_data['camera_matrix']
    fx=cam_mat[0][0]
    fy=cam_mat[1][1]
    skew=cam_mat[0][1]
    ppx=cam_mat[0][2]
    ppy=cam_mat[1][2]
    dist_coefs=cam_data['dist_coefs']
    resolution=cam_data['resolution']
    cam_type=cam_data['cam_type']
    
        # for data in ref_data:
        #     cam_pos=data[0]
        #     cam_x=cam_pos[0]
        #     cam_y=cam_pos[1]
        #     ref_no=data[1]
        #     timeStamp=data[2]
        #     ref_writer.writerow([cam_x, cam_y, ref_no, timeStamp])
    print(version)
    print(dist_coefs)
    print(cam_mat)