#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics

#path='Z:\\DATA\\multiple validations pupil check\\preprocessed\\KPA\\head free\\far viewing\\calibrations\\'
path='Z:\\DATA\\TVOR\\Preprocessed data\\TVOR heave\\SHR\\close csv files\\calibrations\\'
file_name='Clean_validation-260468a6-8e1a-499e-bd67-728688f11b5d'
ext='.plcal'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name + ext, 'rb')
unpacker = msgpack.Unpacker(f, raw=False)


with open(path + file_name + '.csv', mode='w') as write_file:
    ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

#     ref_writer.writerow(['RefPosX', 'RefPosY', 'RefNo', 'TimeStamp'])
    
    for unpacked in unpacker:
        msg=unpacked
        ref_data=msg['data']
              
        calib_data=ref_data['calib_params']
        E1only_calib_data=calib_data['left_model']
        E0only_calib_data=calib_data['right_model']
        Bi_calib_data=calib_data['binocular_model']
        E1onlyM11=E1only_calib_data['eye_camera_to_world_matrix'][0][0]
        E1onlyM12=E1only_calib_data['eye_camera_to_world_matrix'][0][1]
        E1onlyM13=E1only_calib_data['eye_camera_to_world_matrix'][0][2]
        E1onlyM14=E1only_calib_data['eye_camera_to_world_matrix'][0][3]
        E1onlyM21=E1only_calib_data['eye_camera_to_world_matrix'][1][0]
        E1onlyM22=E1only_calib_data['eye_camera_to_world_matrix'][1][1]
        E1onlyM23=E1only_calib_data['eye_camera_to_world_matrix'][1][2]
        E1onlyM24=E1only_calib_data['eye_camera_to_world_matrix'][1][3]
        E1onlyM31=E1only_calib_data['eye_camera_to_world_matrix'][2][0]
        E1onlyM32=E1only_calib_data['eye_camera_to_world_matrix'][2][1]
        E1onlyM33=E1only_calib_data['eye_camera_to_world_matrix'][2][2]
        E1onlyM34=E1only_calib_data['eye_camera_to_world_matrix'][2][3]
        E1onlyM41=E1only_calib_data['eye_camera_to_world_matrix'][3][0]
        E1onlyM42=E1only_calib_data['eye_camera_to_world_matrix'][3][1]
        E1onlyM43=E1only_calib_data['eye_camera_to_world_matrix'][3][2]
        E1onlyM44=E1only_calib_data['eye_camera_to_world_matrix'][3][3]
        
        E0onlyM11=E0only_calib_data['eye_camera_to_world_matrix'][0][0]
        E0onlyM12=E0only_calib_data['eye_camera_to_world_matrix'][0][1]
        E0onlyM13=E0only_calib_data['eye_camera_to_world_matrix'][0][2]
        E0onlyM14=E0only_calib_data['eye_camera_to_world_matrix'][0][3]
        E0onlyM21=E0only_calib_data['eye_camera_to_world_matrix'][1][0]
        E0onlyM22=E0only_calib_data['eye_camera_to_world_matrix'][1][1]
        E0onlyM23=E0only_calib_data['eye_camera_to_world_matrix'][1][2]
        E0onlyM24=E0only_calib_data['eye_camera_to_world_matrix'][1][3]
        E0onlyM31=E0only_calib_data['eye_camera_to_world_matrix'][2][0]
        E0onlyM32=E0only_calib_data['eye_camera_to_world_matrix'][2][1]
        E0onlyM33=E0only_calib_data['eye_camera_to_world_matrix'][2][2]
        E0onlyM34=E0only_calib_data['eye_camera_to_world_matrix'][2][3]
        E0onlyM41=E0only_calib_data['eye_camera_to_world_matrix'][3][0]
        E0onlyM42=E0only_calib_data['eye_camera_to_world_matrix'][3][1]
        E0onlyM43=E0only_calib_data['eye_camera_to_world_matrix'][3][2]
        E0onlyM44=E0only_calib_data['eye_camera_to_world_matrix'][3][3]
        
        Bi_E1M11=Bi_calib_data['eye_camera_to_world_matrix1'][0][0]
        Bi_E1M12=Bi_calib_data['eye_camera_to_world_matrix1'][0][1]
        Bi_E1M13=Bi_calib_data['eye_camera_to_world_matrix1'][0][2]
        Bi_E1M14=Bi_calib_data['eye_camera_to_world_matrix1'][0][3]
        Bi_E1M21=Bi_calib_data['eye_camera_to_world_matrix1'][1][0]
        Bi_E1M22=Bi_calib_data['eye_camera_to_world_matrix1'][1][1]
        Bi_E1M23=Bi_calib_data['eye_camera_to_world_matrix1'][1][2]
        Bi_E1M24=Bi_calib_data['eye_camera_to_world_matrix1'][1][3]
        Bi_E1M31=Bi_calib_data['eye_camera_to_world_matrix1'][2][0]
        Bi_E1M32=Bi_calib_data['eye_camera_to_world_matrix1'][2][1]
        Bi_E1M33=Bi_calib_data['eye_camera_to_world_matrix1'][2][2]
        Bi_E1M34=Bi_calib_data['eye_camera_to_world_matrix1'][2][3]
        Bi_E1M41=Bi_calib_data['eye_camera_to_world_matrix1'][3][0]
        Bi_E1M42=Bi_calib_data['eye_camera_to_world_matrix1'][3][1]
        Bi_E1M43=Bi_calib_data['eye_camera_to_world_matrix1'][3][2]
        Bi_E1M44=Bi_calib_data['eye_camera_to_world_matrix1'][3][3]
        
        Bi_E0M11=Bi_calib_data['eye_camera_to_world_matrix0'][0][0]
        Bi_E0M12=Bi_calib_data['eye_camera_to_world_matrix0'][0][1]
        Bi_E0M13=Bi_calib_data['eye_camera_to_world_matrix0'][0][2]
        Bi_E0M14=Bi_calib_data['eye_camera_to_world_matrix0'][0][3]
        Bi_E0M21=Bi_calib_data['eye_camera_to_world_matrix0'][1][0]
        Bi_E0M22=Bi_calib_data['eye_camera_to_world_matrix0'][1][1]
        Bi_E0M23=Bi_calib_data['eye_camera_to_world_matrix0'][1][2]
        Bi_E0M24=Bi_calib_data['eye_camera_to_world_matrix0'][1][3]
        Bi_E0M31=Bi_calib_data['eye_camera_to_world_matrix0'][2][0]
        Bi_E0M32=Bi_calib_data['eye_camera_to_world_matrix0'][2][1]
        Bi_E0M33=Bi_calib_data['eye_camera_to_world_matrix0'][2][2]
        Bi_E0M34=Bi_calib_data['eye_camera_to_world_matrix0'][2][3]
        Bi_E0M41=Bi_calib_data['eye_camera_to_world_matrix0'][3][0]
        Bi_E0M42=Bi_calib_data['eye_camera_to_world_matrix0'][3][1]
        Bi_E0M43=Bi_calib_data['eye_camera_to_world_matrix0'][3][2]
        Bi_E0M44=Bi_calib_data['eye_camera_to_world_matrix0'][3][3]
    
        ref_writer.writerow(['Software_version', ref_data['version']])
        ref_writer.writerow(['Calib ID', ref_data['unique_id']])
        ref_writer.writerow(['Calib name', ref_data['name']])
        ref_writer.writerow(['Recording UUID', ref_data['recording_uuid']])
        ref_writer.writerow(['Gazer', ref_data['gazer_class_name']])
        ref_writer.writerow(['Calib frame indx no', ref_data['frame_index_range'][0], ref_data['frame_index_range'][1]])
        ref_writer.writerow(['Calib min confidence',ref_data['minimum_confidence']])
        ref_writer.writerow(['Calib status',ref_data['status']])
        ref_writer.writerow(['Calib is offline',ref_data['is_offline_calibration']])
        ref_writer.writerow(['Calib E1 only =============='])
        ref_writer.writerow(['gaze dist', E1only_calib_data['gaze_distance']])
        ref_writer.writerow(['Eye2World_matrix', E1onlyM11, E1onlyM12, E1onlyM13, E1onlyM14,
                             E1onlyM21, E1onlyM22, E1onlyM23, E1onlyM24,
                             E1onlyM31, E1onlyM32, E1onlyM33, E1onlyM34,
                             E1onlyM41, E1onlyM42, E1onlyM43, E1onlyM44])
        ref_writer.writerow(['Calib E0 only =============='])
        ref_writer.writerow(['gaze dist', E0only_calib_data['gaze_distance']])
        ref_writer.writerow(['Eye2World_matrix', E0onlyM11, E0onlyM12, E0onlyM13, E0onlyM14,
                             E0onlyM21, E0onlyM22, E0onlyM23, E0onlyM24,
                             E0onlyM31, E0onlyM32, E0onlyM33, E0onlyM34,
                             E0onlyM41, E0onlyM42, E0onlyM43, E0onlyM44])
        ref_writer.writerow(['Calib Binocular E1 =============='])
        ref_writer.writerow(['Eye2World_matrix', Bi_E1M11, Bi_E1M12, Bi_E1M13, Bi_E1M14,
                             Bi_E1M21, Bi_E1M22, Bi_E1M23, Bi_E1M24,
                             Bi_E1M31, Bi_E1M32, Bi_E1M33, Bi_E1M34,
                             Bi_E1M41, Bi_E1M42, Bi_E1M43, Bi_E1M44])
        ref_writer.writerow(['Calib Binocular E0 =============='])
        ref_writer.writerow(['Eye2World_matrix', Bi_E0M11, Bi_E0M12, Bi_E0M13, Bi_E0M14,
                             Bi_E0M21, Bi_E0M22, Bi_E0M23, Bi_E0M24,
                             Bi_E0M31, Bi_E0M32, Bi_E0M33, Bi_E0M34,
                             Bi_E0M41, Bi_E0M42, Bi_E0M43, Bi_E0M44])
    