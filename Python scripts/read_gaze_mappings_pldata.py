# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 05:29:53 2020

@author: imu
"""

import csv
import msgpack
#import statistics
import numpy as np



class _Empty(object):
    def purge_cache(self):
        pass
class _FrozenDict(dict):
    def __setitem__(self, key, value):
        raise NotImplementedError('Invalid operation')

    def clear(self):
        raise NotImplementedError()

    def update(self, *args, **kwargs):
        raise NotImplementedError()


class Serialized_Dict(object):
    __slots__ = ['_ser_data', '_data']
    cache_len = 100
    _cache_ref = [_Empty()] * cache_len
    MSGPACK_EXT_CODE = 13

    def __init__(self, python_dict=None, msgpack_bytes=None):
        if type(python_dict) is dict:
            self._ser_data = msgpack.packb(python_dict, use_bin_type=True,
                                           default=self.packing_hook)
        elif type(msgpack_bytes) is bytes:
            self._ser_data = msgpack_bytes
        else:
            raise ValueError("Neither mapping nor payload is supplied or wrong format.")
        self._data = None

    def _deser(self):
        if not self._data:
            self._data = msgpack.unpackb(self._ser_data, raw=False, use_list=False,
                                         object_hook=self.unpacking_object_hook,
                                         ext_hook=self.unpacking_ext_hook)
            self._cache_ref.pop(0).purge_cache()
            self._cache_ref.append(self)

    @classmethod
    def unpacking_object_hook(self,obj):
        if type(obj) is dict:
            return _FrozenDict(obj)

    @classmethod
    def packing_hook(self, obj):
        if isinstance(obj, self):
            return msgpack.ExtType(self.MSGPACK_EXT_CODE, obj.serialized)
        raise TypeError("can't serialize {}({})".format(type(obj), repr(obj)))

    @classmethod
    def unpacking_ext_hook(self, code, data):
        if code == self.MSGPACK_EXT_CODE:
            return self(msgpack_bytes=data)
        return msgpack.ExtType(code, data)

    def purge_cache(self):
        self._data = None

    @property
    def serialized(self):
        return self._ser_data

    def __setitem__(self, key, item):
        raise NotImplementedError()

    def __getitem__(self, key):
        self._deser()
        return self._data[key]

    def __repr__(self):
        self._deser()
        return 'Serialized_Dict({})'.format(repr(self._data))

    @property
    def len(self):
        '''Replacement implementation for __len__
        If __len__ is defined numpy will recognize this as nested structure and
        start deserializing everything instead of using this object as it is.
        '''
        self._deser()
        return len(self._data)

    def __delitem__(self, key):
        raise NotImplementedError()

    def get(self,key,default):
        try:
            return self[key]
        except KeyError:
            return default

    def clear(self):
        raise NotImplementedError()

    def copy(self):
        self._deser()
        return self._data.copy()

    def has_key(self, k):
        self._deser()
        return k in self._data

    def update(self, *args, **kwargs):
        raise NotImplementedError()

    def keys(self):
        self._deser()
        return self._data.keys()

    def values(self):
        self._deser()
        return self._data.values()

    def items(self):
        self._deser()
        return self._data.items()

    def pop(self, *args):
        raise NotImplementedError()

    def __cmp__(self, dict_):
        self._deser()
        return self._data.__cmp__(dict_)

    def __contains__(self, item):
        self._deser()
        return item in self._data

    def __iter__(self):
        self._deser()
        return iter(self._data)
    
    
path='C:\\Users\\avelisar\\Desktop\\eye_test_Hfree\\offline_data\\gaze-mappings\\'
file_name='GMO6-48cffc30-e102-437a-961e-49d133ad2e96.pldata'
ts_file='GMO6-48cffc30-e102-437a-961e-49d133ad2e96_timestamps.npy'
#path='Z:\\DATA\\Head Free DATA\\hsfull\\HS03\\'
#file_name='hs03_eyeTracker_20190327-144007'
#f=open(path + file_name + '.msgpack', 'rb')
f=open(path + file_name, 'rb')
data_ts = np.load(path + ts_file)
i=0
with open(path + 'GMO6_data.csv', mode='w') as write_file:
    ref_writer = csv.writer(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    ref_writer.writerow(['topic', 'GazeTimeStamp', 'data_ts', 'GazeConfidence ',
                         'E0CenterX', 'E0CenterY', 'E0CenterZ',
                         'E0normalX', 'E0normalY', 'E0normalZ',
                         'E1CenterX', 'E1CenterY', 'E1CenterZ',
                         'E1normalX', 'E1normalY', 'E1normalZ',
                         'GZPX', 'GZPY', 'GZPZ',
                         'GZPnormposX', 'GZPnormposY',
                         'P0topic', 'P0id', 'P0timestamp', 'P0method', 'P0Confidence ',
                         'P0Circ3dCenterX','P0Circ3dCenterY', 'P0Circ3dCenterZ',
                         'P0Circ3dNormalX','P0Circ3dNormalY', 'P0Circ3dNormalZ',
                         'P0diam3d', 'P0ellipseCntrX', 'P0ellipseCntrY',
                         'P0ellipseAxesX', 'P0ellipseAxesY',
                         'P0ellipseAngle','P0diamPix','P0LocPixX', 'P0LocPixY',
                         'P0NormPosX', 'P0NormPosY',
                         'P0SphereCenterX','P0SphereCenterY', 'P0SphereCenterZ', 'P0SphereRadius',
                         'P0projsphCntrX', 'P0projsphCntrY',
                         'P0projsphAxesX', 'P0projsphAxesY', 'P0projsphAngle',
                         'P0theta', 'P0phi',
                         'P0modelConfidence', 
                         'P1topic', 'P1id', 'P1timestamp', 'P1method', 'P1Confidence ',
                         'P1Circ3dCenterX','P1Circ3dCenterY', 'P1Circ3dCenterZ',
                         'P1Circ3dNormalX','P1Circ3dNormalY', 'P1Circ3dNormalZ',
                         'P1diam3d', 'P1ellipseCntrX', 'P1ellipseCntrY',
                         'P1ellipseAxesX', 'P1ellipseAxesY',
                         'P1ellipseAngle','P1diamPix','P1LocPixX', 'P1LocPixY',
                         'P1NormPosX', 'P1NormPosY',
                         'P1SphereCenterX','P1SphereCenterY', 'P1SphereCenterZ', 'P1SphereRadius',
                         'P1projsphCntrX', 'P1projsphCntrY',
                         'P1projsphAxesX', 'P1projsphAxesY', 'P1projsphAngle',
                         'P1theta', 'P1phi',
                         'P1modelConfidence'])
 
    for topic, payload in msgpack.Unpacker(f, raw=False, use_list=False):
        frame=Serialized_Dict(msgpack_bytes=payload)
        Gtype=frame['topic']
        pupil_data=frame['base_data']
        if Gtype == 'gaze.3d.01.':
            GE0CntrX=frame['eye_centers_3d']['0'][0]
            GE0CntrY=frame['eye_centers_3d']['0'][1]
            GE0CntrZ=frame['eye_centers_3d']['0'][2]
            GE1CntrX=frame['eye_centers_3d']['1'][0]
            GE1CntrY=frame['eye_centers_3d']['1'][1]
            GE1CntrZ=frame['eye_centers_3d']['1'][2]
            GE0NormalX=frame['gaze_normals_3d']['0'][0]
            GE0NormalY=frame['gaze_normals_3d']['0'][1]
            GE0NormalZ=frame['gaze_normals_3d']['0'][2]
            GE1NormalX=frame['gaze_normals_3d']['1'][0]
            GE1NormalY=frame['gaze_normals_3d']['1'][1]
            GE1NormalZ=frame['gaze_normals_3d']['1'][2]
            (P0data,P1data)=frame['base_data']
            P0cir3dcntrx=P0data['circle_3d']['center'][0]
            P0cir3dcntry=P0data['circle_3d']['center'][1]
            P0cir3dcntrz=P0data['circle_3d']['center'][2]
            P0cir3dnormx=P0data['circle_3d']['normal'][0]
            P0cir3dnormy=P0data['circle_3d']['normal'][1]
            P0cir3dnormz=P0data['circle_3d']['normal'][2]
            P0conf=P0data['confidence']
            P0timeStamp=P0data['timestamp']
            P0diam3d=P0data['diameter_3d']
            P0ellCntrX=P0data['ellipse']['center'][0]
            P0ellCntrY=P0data['ellipse']['center'][1]
            P0ellAxX=P0data['ellipse']['axes'][0]
            P0ellAxY=P0data['ellipse']['axes'][1]
            P0ellAng=P0data['ellipse']['angle']
            P0diam=P0data['diameter']
            P0pixX=P0data['location'][0]
            P0pixY=P0data['location'][1]
            P0sphCntrX=P0data['sphere']['center'][0]
            P0sphCntrY=P0data['sphere']['center'][1]
            P0sphCntrZ=P0data['sphere']['center'][2]
            P0sphR=P0data['sphere']['radius']
            P0prjsphCntrX=P0data['projected_sphere']['center'][0]
            P0prjsphCntrY=P0data['projected_sphere']['center'][1]
            P0prjsphAxX=P0data['projected_sphere']['axes'][0]
            P0prjsphAxY=P0data['projected_sphere']['axes'][1]
            P0prjsphAng=P0data['projected_sphere']['angle']
            P0modelConf=P0data['model_confidence']
            P0method=P0data['method']
            P0ID=P0data['id']
            P0topic=P0data['topic']
            P0theta=P0data['theta']
            P0phi=P0data['phi']
            P0normposX=P0data['norm_pos'][0]
            P0normposY=P0data['norm_pos'][1]
            P1cir3dcntrx=P1data['circle_3d']['center'][0]
            P1cir3dcntry=P1data['circle_3d']['center'][1]
            P1cir3dcntrz=P1data['circle_3d']['center'][2]
            P1cir3dnormx=P1data['circle_3d']['normal'][0]
            P1cir3dnormy=P1data['circle_3d']['normal'][1]
            P1cir3dnormz=P1data['circle_3d']['normal'][2]
            P1conf=P1data['confidence']
            P1timeStamp=P1data['timestamp']
            P1diam3d=P1data['diameter_3d']
            P1ellCntrX=P1data['ellipse']['center'][0]
            P1ellCntrY=P1data['ellipse']['center'][1]
            P1ellAxX=P1data['ellipse']['axes'][0]
            P1ellAxY=P1data['ellipse']['axes'][1]
            P1ellAng=P1data['ellipse']['angle']
            P1diam=P1data['diameter']
            P1pixX=P1data['location'][0]
            P1pixY=P1data['location'][1]
            P1sphCntrX=P1data['sphere']['center'][0]
            P1sphCntrY=P1data['sphere']['center'][1]
            P1sphCntrZ=P1data['sphere']['center'][2]
            P1sphR=P1data['sphere']['radius']
            P1prjsphCntrX=P1data['projected_sphere']['center'][0]
            P1prjsphCntrY=P1data['projected_sphere']['center'][1]
            P1prjsphAxX=P1data['projected_sphere']['axes'][0]
            P1prjsphAxY=P1data['projected_sphere']['axes'][1]
            P1prjsphAng=P1data['projected_sphere']['angle']
            P1modelConf=P1data['model_confidence']
            P1method=P1data['method']
            P1ID=P1data['id']
            P1topic=P1data['topic']
            P1theta=P1data['theta']
            P1phi=P1data['phi']
            P1normposX=P1data['norm_pos'][0]
            P1normposY=P1data['norm_pos'][1]
        elif Gtype == 'gaze.3d.0.':
            GE0CntrX=frame['eye_center_3d'][0]
            GE0CntrY=frame['eye_center_3d'][1]
            GE0CntrZ=frame['eye_center_3d'][2]
            GE1CntrX=[]
            GE1CntrY=[]
            GE1CntrZ=[]
            GE0NormalX=frame['gaze_normal_3d'][0]
            GE0NormalY=frame['gaze_normal_3d'][1]
            GE0NormalZ=frame['gaze_normal_3d'][2]
            GE1NormalX=[]
            GE1NormalY=[]
            GE1NormalZ=[]
            (P0data,)=frame['base_data']
            P0cir3dcntrx=P0data['circle_3d']['center'][0]
            P0cir3dcntry=P0data['circle_3d']['center'][1]
            P0cir3dcntrz=P0data['circle_3d']['center'][2]
            P0cir3dnormx=P0data['circle_3d']['normal'][0]
            P0cir3dnormy=P0data['circle_3d']['normal'][1]
            P0cir3dnormz=P0data['circle_3d']['normal'][2]
            P0conf=P0data['confidence']
            P0timeStamp=P0data['timestamp']
            P0diam3d=P0data['diameter_3d']
            P0ellCntrX=P0data['ellipse']['center'][0]
            P0ellCntrY=P0data['ellipse']['center'][1]
            P0ellAxX=P0data['ellipse']['axes'][0]
            P0ellAxY=P0data['ellipse']['axes'][1]
            P0ellAng=P0data['ellipse']['angle']
            P0diam=P0data['diameter']
            P0pixX=P0data['location'][0]
            P0pixY=P0data['location'][1]
            P0sphCntrX=P0data['sphere']['center'][0]
            P0sphCntrY=P0data['sphere']['center'][1]
            P0sphCntrZ=P0data['sphere']['center'][2]
            P0sphR=P0data['sphere']['radius']
            P0prjsphCntrX=P0data['projected_sphere']['center'][0]
            P0prjsphCntrY=P0data['projected_sphere']['center'][1]
            P0prjsphAxX=P0data['projected_sphere']['axes'][0]
            P0prjsphAxY=P0data['projected_sphere']['axes'][1]
            P0prjsphAng=P0data['projected_sphere']['angle']
            P0modelConf=P0data['model_confidence']
            P0method=P0data['method']
            P0ID=P0data['id']
            P0topic=P0data['topic']
            P0theta=P0data['theta']
            P0phi=P0data['phi']
            P0normposX=P0data['norm_pos'][0]
            P0normposY=P0data['norm_pos'][1]
            P1cir3dcntrx=[]
            P1cir3dcntry=[]
            P1cir3dcntrz=[]
            P1cir3dnormx=[]
            P1cir3dnormy=[]
            P1cir3dnormz=[]
            P1conf=[]
            P1timeStamp=[]
            P1diam3d=[]
            P1ellCntrX=[]
            P1ellCntrY=[]
            P1ellAxX=[]
            P1ellAxY=[]
            P1ellAng=[]
            P1diam=[]
            P1pixX=[]
            P1pixY=[]
            P1sphCntrX=[]
            P1sphCntrY=[]
            P1sphCntrZ=[]
            P1sphR=[]
            P1prjsphCntrX=[]
            P1prjsphCntrY=[]
            P1prjsphAxX=[]
            P1prjsphAxY=[]
            P1prjsphAng=[]
            P1modelConf=[]
            P1method=[]
            P1ID=[]
            P1topic=[]
            P1theta=[]
            P1phi=[]
            P1normposX=[]
            P1normposY=[]
        elif Gtype == 'gaze.3d.1.':
            
            GE1CntrX=frame['eye_center_3d'][0]
            GE1CntrY=frame['eye_center_3d'][1]
            GE1CntrZ=frame['eye_center_3d'][2]
            GE0CntrX=[]
            GE0CntrY=[]
            GE0CntrZ=[]
            GE1NormalX=frame['gaze_normal_3d'][0]
            GE1NormalY=frame['gaze_normal_3d'][1]
            GE1NormalZ=frame['gaze_normal_3d'][2]
            GE0NormalX=[]
            GE0NormalY=[]
            GE0NormalZ=[]
            (P1data,)=frame['base_data']
            P1cir3dcntrx=P1data['circle_3d']['center'][0]
            P1cir3dcntry=P1data['circle_3d']['center'][1]
            P1cir3dcntrz=P1data['circle_3d']['center'][2]
            P1cir3dnormx=P1data['circle_3d']['normal'][0]
            P1cir3dnormy=P1data['circle_3d']['normal'][1]
            P1cir3dnormz=P1data['circle_3d']['normal'][2]
            P1conf=P1data['confidence']
            P1timeStamp=P1data['timestamp']
            P1diam3d=P1data['diameter_3d']
            P1ellCntrX=P1data['ellipse']['center'][0]
            P1ellCntrY=P1data['ellipse']['center'][1]
            P1ellAxX=P1data['ellipse']['axes'][0]
            P1ellAxY=P1data['ellipse']['axes'][1]
            P1ellAng=P1data['ellipse']['angle']
            P1diam=P1data['diameter']
            P1pixX=P1data['location'][0]
            P1pixY=P1data['location'][1]
            P1sphCntrX=P1data['sphere']['center'][0]
            P1sphCntrY=P1data['sphere']['center'][1]
            P1sphCntrZ=P1data['sphere']['center'][2]
            P1sphR=P1data['sphere']['radius']
            P1prjsphCntrX=P1data['projected_sphere']['center'][0]
            P1prjsphCntrY=P1data['projected_sphere']['center'][1]
            P1prjsphAxX=P1data['projected_sphere']['axes'][0]
            P1prjsphAxY=P1data['projected_sphere']['axes'][1]
            P1prjsphAng=P1data['projected_sphere']['angle']
            P1modelConf=P1data['model_confidence']
            P1method=P1data['method']
            P1ID=P1data['id']
            P1topic=P1data['topic']
            P1theta=P1data['theta']
            P1phi=P1data['phi']
            P1normposX=P1data['norm_pos'][0]
            P1normposY=P1data['norm_pos'][1]
            P0cir3dcntrx=[]
            P0cir3dcntry=[]
            P0cir3dcntrz=[]
            P0cir3dnormx=[]
            P0cir3dnormy=[]
            P0cir3dnormz=[]
            P0conf=[]
            P0timeStamp=[]
            P0diam3d=[]
            P0ellCntrX=[]
            P0ellCntrY=[]
            P0ellAxX=[]
            P0ellAxY=[]
            P0ellAng=[]
            P0diam=[]
            P0pixX=[]
            P0pixY=[]
            P0sphCntrX=[]
            P0sphCntrY=[]
            P0sphCntrZ=[]
            P0sphR=[]
            P0prjsphCntrX=[]
            P0prjsphCntrY=[]
            P0prjsphAxX=[]
            P0prjsphAxY=[]
            P0prjsphAng=[]
            P0modelConf=[]
            P0method=[]
            P0ID=[]
            P0topic=[]
            P0theta=[]
            P0phi=[]
            P0normposX=[]
            P0normposY=[]
        GPX=frame['gaze_point_3d'][0]
        GPY=frame['gaze_point_3d'][1]
        GPZ=frame['gaze_point_3d'][2]
        GPnormPosX=frame['norm_pos'][0]
        GPnormPosY=frame['norm_pos'][1]
        Gconf=frame['confidence']
        timeStamp=frame['timestamp']
        
        ts=data_ts[i]
        i=i+1
        ref_writer.writerow([Gtype, timeStamp, ts, Gconf, 
                             GE0CntrX, GE0CntrY, GE0CntrZ, 
                             GE0NormalX, GE0NormalY, GE0NormalZ, 
                             GE1CntrX, GE1CntrY, GE1CntrZ, 
                             GE1NormalX, GE1NormalY, GE1NormalZ, 
                             GPX, GPY, GPZ, GPnormPosX, GPnormPosY, 
                             P0topic, P0ID, P0timeStamp, P0method, P0conf, 
                             P0cir3dcntrx, P0cir3dcntry, P0cir3dcntrz, 
                             P0cir3dnormx, P0cir3dnormy, P0cir3dnormz, 
                             P0diam3d, P0ellCntrX, P0ellCntrY, P0ellAxX, P0ellAxY, P0ellAng,
                             P0diam, P0pixX, P0pixY, P0normposX, P0normposY, 
                             P0sphCntrX, P0sphCntrY, P0sphCntrZ, P0sphR, 
                             P0prjsphCntrX, P0prjsphCntrY, P0prjsphAxX, P0prjsphAxY, P0prjsphAng,
                             P0theta, P0phi, P0modelConf,
                             P1topic, P1ID, P1timeStamp, P1method, P1conf, 
                             P1cir3dcntrx, P1cir3dcntry, P1cir3dcntrz, 
                             P1cir3dnormx, P1cir3dnormy, P1cir3dnormz, 
                             P1diam3d, P1ellCntrX, P1ellCntrY, P1ellAxX, P1ellAxY, P1ellAng,
                             P1diam, P1pixX, P1pixY, P1normposX, P1normposY, 
                             P1sphCntrX, P1sphCntrY, P1sphCntrZ, P1sphR, 
                             P1prjsphCntrX, P1prjsphCntrY, P1prjsphAxX, P1prjsphAxY, P1prjsphAng,
                             P1theta, P1phi, P1modelConf])

    # print(P0data.keys())
    # print(P0data['norm_pos'])
    # try:
    #     print(P0data.keys()) 
    # except:
    #     print(Gtype)
      