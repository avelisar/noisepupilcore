function [IntersectP,nearestP0,nearestP1,shortest_dist]=nearestIntersectionOF2vectors(vect1, vect2, OrigVect1, OrigVect2)

% explain algorithm
nearestP0=OrigVect1+(dot(OrigVect2-OrigVect1,cross(vect2,cross(vect1,vect2)))/dot(vect1,cross(vect2,cross(vect1,vect2))))*vect1;
nearestP1=OrigVect2+(dot(OrigVect1-OrigVect2,cross(vect1,cross(vect1,vect2)))/dot(vect2,cross(vect1,cross(vect1,vect2))))*vect2;
IntersectP=(nearestP1(:)'+nearestP0(:)')./2;
shortest_dist=norm(nearestP0-nearestP1);