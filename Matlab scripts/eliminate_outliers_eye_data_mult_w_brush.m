function [time,data,isNew]=eliminate_outliers_eye_data_mult_w_brush(time,data)

pause('on');
isMore=1;
isNew=0;

[rows,cols]=size(data);
colors=['b','r','g','c','m'];
while isMore

    
    
    
ff=figure;

for i=1:cols
    sp(i)=subplot(cols,1,i);
    ll(i)=plot(time,data(:,i),'.:','Color',colors(i));
end
subplot(cols,1,1);
title({'Start by pressing Return once','CHOOSE bad point by draging rectangle over','for multiple points keep shift key pressed while clicking','then turn off brush mode.'});
linkaxes(sp,'x');
pause
brush on;
pause
% Wait while the user does this.
waitforbuttonpress
brush_samples = false(size(data(:,1)));
for i=1:cols
    if ~isempty(ll(i).BrushData)
        brush_samples = or(brush_samples(:), logical(ll(i).BrushData(:)));
    end
end    
if sum(brush_samples)~=0
    isNew=1;
    data(brush_samples,:)=[];
    time(brush_samples)=[];
    clear brush_samples
else
    isMore=0;
end
close(ff);
end
pause('off');