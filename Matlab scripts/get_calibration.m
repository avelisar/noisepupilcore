function calibration=get_calibration(filename)
% get calibration structure

if nargin==0
% get file 
[file,path] = uigetfile('*.csv');
f = filesep;
calib_raw = import_calibration_csv_file_see_all_info([path f file]);
elseif nargin ==1
    calib_raw = import_calibration_csv_file_see_all_info(filename);
end


calibration.range=[calib_raw{6,2} calib_raw{6,3}];
calibration.min_conf=calib_raw{7,2};
if isnan(calib_raw{3,2})
    calibration.name='Recording';
else
    calibration.name=calib_raw{3,2};
end
if isnan(calib_raw{9,2})
    calibration.isoffline=0;
else
    calibration.isoffline=1;
end

calibration.mset(1).type='monocular';
calibration.mset(1).eye(1).name='E0';
calibration.mset(1).eye(1).distance=calib_raw{14,2};
calibration.mset(1).eye(1).RTmat=reshape(cell2mat(calib_raw(15,2:17)),4,4)';
calibration.mset(1).eye(1).Rmat=calibration.mset(1).eye(1).RTmat(1:3,1:3);
calibration.mset(1).eye(2).name='E1';
calibration.mset(1).eye(2).distance=calib_raw{11,2};
calibration.mset(1).eye(2).RTmat=reshape(cell2mat(calib_raw(12,2:17)),4,4)';
calibration.mset(1).eye(2).Rmat=calibration.mset(1).eye(2).RTmat(1:3,1:3);
calibration.mset(2).type='binocular';
calibration.mset(2).eye(1).name='E0';
calibration.mset(2).eye(1).distance=nan;
calibration.mset(2).eye(1).RTmat=reshape(cell2mat(calib_raw(19,2:17)),4,4)';
calibration.mset(2).eye(1).Rmat=calibration.mset(2).eye(1).RTmat(1:3,1:3);
calibration.mset(2).eye(2).name='E1';
calibration.mset(2).eye(2).distance=nan;
calibration.mset(2).eye(2).RTmat=reshape(cell2mat(calib_raw(17,2:17)),4,4)';
calibration.mset(2).eye(2).Rmat=calibration.mset(2).eye(2).RTmat(1:3,1:3);