function [PK2PK_series]=get_peak2peak_amplitude(time,signal,neg_pk_height)






[PKS_n,LOCS_n]=findpeaks(-signal(signal<0),time(signal<0),'MinPeakHeight',neg_pk_height);

PKS_n=PKS_n(:);

LOCS_n=LOCS_n(:);
for i=1:length(LOCS_n)-1
    [PKS_p(i),indx_p]=max(signal(time>LOCS_n(i)&time<LOCS_n(i+1)));
    sm_time=time(time>LOCS_n(i)&time<LOCS_n(i+1));
    LOCS_p(i)=sm_time(indx_p);
end
[PKS_p(length(LOCS_n)),indx_p]=max(signal(time>LOCS_n(length(LOCS_n))));
sm_time=time(time>LOCS_n(length(LOCS_n)));
LOCS_p(length(LOCS_n))=sm_time(indx_p);

PK2PK_series=[ (PKS_p(:)+PKS_n(:)) LOCS_p(:) LOCS_n(:) PKS_p(:) PKS_n(:)];