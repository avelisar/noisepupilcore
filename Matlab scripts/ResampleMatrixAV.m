% ResampleMatrixAV.m
% function dataMatrixRs = ResampleMatrix(dataMatrix, targetRateHz, padLengthSamples)
% resample a data matrix at a fixed rate, with advanced edge padding to avoid artifacts
% currently uses **LINEAR** interpolation since that seems to best reproduce the shape, avoid spurious peaks
% original time vector is assumed to be first column of data matrix
% JBB 10-2017
% AV modified so the resampled data starts from start_timestamp and goes to
% end_timestamp
function dataMatrixRs = ResampleMatrixAV(dataMatrix, targetRateHz, padLengthSamples,start_time, end_time)

% get original time vector
timeVector = dataMatrix(:,1);
reverseTimeFlag = diff(timeVector) < 0;
if any(reverseTimeFlag)
	warning('Timestamp vector not monotonically increasing! Is time the first column of your data matrix?');
	fprintf('We will eliminate all of those samples (n = %d)\n', sum(reverseTimeFlag));
	dataMatrix( find(reverseTimeFlag)+1, : ) = [];
end

% generate dummy timestamps for padded data
deltaT = timeVector(padLengthSamples+1) - timeVector(1);			% time pre-pad should traverse
timeVectorPrepad = timeVector(1:padLengthSamples) - deltaT;
deltaT = timeVector(end) - timeVector(end-padLengthSamples);		% time post-pad should traverse
timeVectorPostpad = timeVector(end-padLengthSamples+1:end) + deltaT;

% (check)
% plot( [timeVectorPrepad; timeVector(1:50)], 'marker', '.')
% plot( diff([timeVectorPrepad; timeVector(1:50)]), 'marker', '.')
% plot( [timeVector(end-50:end); timeVectorPostpad], 'marker', '.')
% plot( diff([timeVector(end-50:end); timeVectorPostpad]), 'marker', '.')

% pad data, using mirrored samples from both ends
matrixPrepad = dataMatrix(padLengthSamples:-1:1, :);
matrixPostpad = dataMatrix(end : -1 : end-padLengthSamples+1, :);
dataMatrixPadded = [matrixPrepad; dataMatrix; matrixPostpad];

% copy in dummy timestamps
dataMatrixPadded(1:padLengthSamples, 1) = timeVectorPrepad;
dataMatrixPadded(end-padLengthSamples+1 : end, 1) = timeVectorPostpad;

% resample padded data matrix (including timestamps) 10 times more than the
% desired data
[dataMatrixRs, timeVectorRs] = resample(dataMatrixPadded(:, 2:end), dataMatrixPadded(:,1), 10*targetRateHz, 'linear');
dataMatrixRs = [timeVectorRs, dataMatrixRs];	% reincorporate time vector

% trim data before start_timestamp
keepSamples = timeVectorRs >= start_time;
dataMatrixRs = dataMatrixRs(keepSamples, :);
%shift time to start_timestamp
dataMatrixRs(:,1) = dataMatrixRs(:,1)-dataMatrixRs(1,1)+start_time;

%take dc out before downsampling
dc_vector=dataMatrixRs(1,2:end);
dataMatrixRs(:,2:end)=dataMatrixRs(:,2:end)-repmat(dc_vector,size(dataMatrixRs,1),1);
[dataMatrixRds, timeVectorRds] = resample(dataMatrixRs(:, 2:end), dataMatrixRs(:,1), targetRateHz);
dataMatrixRds=dataMatrixRds+repmat(dc_vector,size(dataMatrixRds,1),1);
dataMatrixRs = [timeVectorRds, dataMatrixRds];	% reincorporate time vector
% trim data after end_timestamp
keepSamples = timeVectorRds <= end_time;
dataMatrixRs = dataMatrixRs(keepSamples, :);


% (check)
% figure(1); clf; hold on;
% plot(dataMatrix(:,1), dataMatrix(:, 2:end), 'b');
% plot(dataMatrixRs(:,1), dataMatrixRs(:, 2:end), 'r');
