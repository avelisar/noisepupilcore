function [mon_orient_in_global, mon_orient_in_scene_cam, imarker_angs, E0EC_inscenecam,E1EC_inscenecam,marker5_inscenecam]=find_markers_pos(period_time,IMU_data,valid_grid,m_width,m_height,viewing_dist,E0_center_scene_cam,E1_center_scene_cam,sbj_IO)

% calculate the position of the markers in meters from  marker 5
% (center)
valid_grid_m=valid_grid;
valid_grid_m(:,2)=(valid_grid(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid(:,3)-50)*m_height/100;

% select the imu data for the current period only

IMU_data_period=IMU_data(IMU_data.time>=period_time(1) & IMU_data.time<=period_time(2),:);

% IMU orientation during presentation of the current set of markers
imu_orient_RotMat=rotationMATfromEulers4LPMS_IMU(mean(IMU_data_period.EulerXdeg), mean(IMU_data_period.EulerYdeg), mean(IMU_data_period.EulerZdeg));



% estimate monitor orientation in global


% calculate the norm of the monitor as cross product between global Z and
% x_local_vector reprezented in global frame (take only the projection in
% the XY plane)
% calculate x_sensor_vector reprezented in global frame
sensor_x_axis_in_global=(imu_orient_RotMat'*[1 0 0]')';
%take only the projection on XY and calculate the unit vector
dummy_mag_vect=sqrt(sum((sensor_x_axis_in_global(1:2)).^2));
dummy_U_xvect=[sensor_x_axis_in_global(1:2) 0]./dummy_mag_vect;
%norm2monitor_inglobal=cross(dummy_U_xvect,[0 0 1]);
norm2monitor_inglobal=cross([0 0 1],dummy_U_xvect);
dummy_U_yvect=[0 0 -1];
mon_orient_in_global=[dummy_U_xvect(:) dummy_U_yvect(:) norm2monitor_inglobal(:)];

% estimate monitor orientation in scene camera

% express the norm2monitor_inglobal in the scenecam frame
norm2monitor_inscenecam=(imu_orient_RotMat*norm2monitor_inglobal')';
% calculate the point of intersection of the normal2monitor starting from
% the eye_center ending on the monitor plane in scenecam
%make the vector of length == viewing distance
% gravity is oposite global Z axis
gravity_inscenecam=(imu_orient_RotMat*[0 0 -1]')';

dummy_long_vect=norm2monitor_inscenecam.*viewing_dist;
% scale the inter ocular distance to the measured one
IO=sqrt(sum((E0_center_scene_cam(1:3)-E1_center_scene_cam(1:3)).^2))/1000;
% recalculate the eyes centers position in the scenecam for custom sbj_IO
Eye_line_vect=(E0_center_scene_cam(1:3)-E1_center_scene_cam(1:3));
Eye_line_vect2add=Eye_line_vect*(((sbj_IO/IO)-1)/2);
E0EC_inscenecam=(E0_center_scene_cam(1:3)+Eye_line_vect2add)./1000;
E1EC_inscenecam=(E1_center_scene_cam(1:3)-Eye_line_vect2add)./1000;

% calculate the projection of eye centers on monnitor plane in scenecam
projE0EC_inscenecam=E0EC_inscenecam +dummy_long_vect(:);
projE1EC_inscenecam=E1EC_inscenecam+dummy_long_vect(:);
marker5_inscenecam=(projE0EC_inscenecam+projE1EC_inscenecam)./2;
% the unit vectors of the monitor translated in scenecam ref frame
monUvect_x_inscenecam=(imu_orient_RotMat*dummy_U_xvect')';
monUvect_y_inscenecam=gravity_inscenecam;

mon_orient_in_scene_cam=[monUvect_x_inscenecam(:) monUvect_y_inscenecam(:) norm2monitor_inscenecam(:)];



imarker_vect_positions_E0_in_scenecam=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam+valid_grid_m(:,3)*monUvect_y_inscenecam;
imarker_vect_positions_E1_in_scenecam=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam+valid_grid_m(:,3)*monUvect_y_inscenecam;

% homography refrences positions
imarker_E0_horiz_vis_angle=atand(((imarker_vect_positions_E0_in_scenecam-repmat(E0EC_inscenecam',size(imarker_vect_positions_E0_in_scenecam,1),1))*monUvect_x_inscenecam')./viewing_dist);
imarker_E0_vert_vis_angle=atand(((imarker_vect_positions_E0_in_scenecam-repmat(E0EC_inscenecam',size(imarker_vect_positions_E0_in_scenecam,1),1))*monUvect_y_inscenecam')./viewing_dist);
imarker_E1_horiz_vis_angle=atand(((imarker_vect_positions_E1_in_scenecam-repmat(E1EC_inscenecam',size(imarker_vect_positions_E1_in_scenecam,1),1))*monUvect_x_inscenecam')./viewing_dist);
imarker_E1_vert_vis_angle=atand(((imarker_vect_positions_E1_in_scenecam-repmat(E1EC_inscenecam',size(imarker_vect_positions_E1_in_scenecam,1),1))*monUvect_y_inscenecam')./viewing_dist);

imarker_angs=[valid_grid(:,1) imarker_E0_horiz_vis_angle(:) imarker_E0_vert_vis_angle(:) imarker_E1_horiz_vis_angle(:) imarker_E1_vert_vis_angle(:)];
