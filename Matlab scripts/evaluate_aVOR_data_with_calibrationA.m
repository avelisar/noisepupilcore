function [horizDATA,vertDATA,aVORdata]=evaluate_aVOR_data_with_calibrationA(aVORdata, calibrationA,extrinsicsMat,E0EC_inscenecam_calibrationA,E1EC_inscenecam_calibrationA,scenecameraParams)

IMURateHz=400;
sm_coef=5;

% roatate IMU data with the positional adjustment between scene camera and IMU
if ~isempty(extrinsicsMat)
    corrected_gyro1=extrinsicsMat*[table2array(aVORdata(1).imu(:,1:3))';ones(1,height(aVORdata(1).imu))];
    corrected_acc1=extrinsicsMat*[table2array(aVORdata(1).imu(:,7:9))';ones(1,height(aVORdata(1).imu))];
    corrected_gyro2=extrinsicsMat*[table2array(aVORdata(2).imu(:,1:3))';ones(1,height(aVORdata(2).imu))];
    corrected_acc2=extrinsicsMat*[table2array(aVORdata(2).imu(:,7:9))';ones(1,height(aVORdata(2).imu))];

aVORdata(1).cimu=table(corrected_gyro1(1,:)',corrected_gyro1(2,:)',corrected_gyro1(3,:)',...
    corrected_acc1(1,:)',corrected_acc1(2,:)',corrected_acc1(3,:)',aVORdata(1).imu.time,...
    'VariableNames',{'GyroXdegs','GyroYdegs','GyroZdegs','LinAccXg','LinAccYg','LinAccZg','time'});
aVORdata(2).cimu=table(corrected_gyro2(1,:)',corrected_gyro2(2,:)',corrected_gyro2(3,:)',...
    corrected_acc2(1,:)',corrected_acc2(2,:)',corrected_acc2(3,:)',aVORdata(2).imu.time,...
    'VariableNames',{'GyroXdegs','GyroYdegs','GyroZdegs','LinAccXg','LinAccYg','LinAccZg','time'});
end

%Get rotation angle for head around local Y for horiz aVOR and around local X for vert aVOR
% horiz aVOR
[IMU_data_angle_pos_horz,vel_vect]=get_IMU_rot_angle_and_shift_no_filtpvel(aVORdata(1).cimu,IMURateHz,sm_coef);
horizDATA.H.pos=IMU_data_angle_pos_horz(:,2);
horizDATA.time=aVORdata(1).imu.time;
horizDATA.H.vel=vel_vect(:,2);
clear vel_vect
% vertical aVOR
[IMU_data_angle_pos_vert,vel_vect]=get_IMU_rot_angle_and_shift_no_filtpvel(aVORdata(2).cimu,IMURateHz,sm_coef);
vertDATA.H.pos=IMU_data_angle_pos_vert(:,1);
vertDATA.time=aVORdata(2).imu.time;
vertDATA.H.vel=vel_vect(:,1);


% horiz
st=1;
% get gaze data using calibrationA
E0_GZ_normals=(calibrationA.mset(2).eye(1).Rmat*table2array(aVORdata(st).E0(:,7:9))')';
E1_GZ_normals=(calibrationA.mset(2).eye(2).Rmat*table2array(aVORdata(st).E1(:,7:9))')';

%Interpolate the IMU and pupil streams of data to a common sampling
%frequency (IMU sampling freq)
clear dataMatrixRs dataMatrixFilt dataMatrixRs_in_head_time
dataMatrixRs = ResampleMatrixAV([aVORdata(st).E0.time E0_GZ_normals], IMURateHz, 50, aVORdata(st).E0.time(1), aVORdata(st).E0.time(end));
dataMatrixRs_in_head_time(:,1)=aVORdata(st).imu.time;
dataMatrixRs_in_head_time(:,2)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,2),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,3)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,3),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,4)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,4),aVORdata(st).imu.time,'linear','extrap');
E0_GZ_normals_HT=dataMatrixRs_in_head_time(:,2:4)./vecnorm(dataMatrixRs_in_head_time(:,2:4),2,2);

clear dataMatrixRs dataMatrixFilt dataMatrixRs_in_head_time
dataMatrixRs = ResampleMatrixAV([aVORdata(st).E1.time E1_GZ_normals], IMURateHz, 50, aVORdata(st).E1.time(1), aVORdata(st).E1.time(end));
dataMatrixRs_in_head_time(:,1)=aVORdata(st).imu.time;
dataMatrixRs_in_head_time(:,2)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,2),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,3)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,3),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,4)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,4),aVORdata(st).imu.time,'linear','extrap');
E1_GZ_normals_HT=dataMatrixRs_in_head_time(:,2:4)./vecnorm(dataMatrixRs_in_head_time(:,2:4),2,2);

% calculate the instantaneus angle between the projection of the gaze vector on the XZ plane and the X axis
E0_GZangle_around_Y_sc=asind(E0_GZ_normals_HT(:,1)./sqrt((E0_GZ_normals_HT(:,1)).^2+(E0_GZ_normals_HT(:,3)).^2));
E1_GZangle_around_Y_sc=asind(E1_GZ_normals_HT(:,1)./sqrt((E1_GZ_normals_HT(:,1)).^2+(E1_GZ_normals_HT(:,3)).^2));
E0_GZangle_around_Y_sc_ch=E0_GZangle_around_Y_sc-mean(E0_GZangle_around_Y_sc);
E1_GZangle_around_Y_sc_ch=E1_GZangle_around_Y_sc-mean(E1_GZangle_around_Y_sc);

% filter the excursion signal
[b,a]=butter(2,3/(IMURateHz/2));
E0_GZangle_around_Y_sc_ch=filtfilt(b,a,E0_GZangle_around_Y_sc_ch);
E1_GZangle_around_Y_sc_ch=filtfilt(b,a,E1_GZangle_around_Y_sc_ch);


% calculate velocities
E0_GZang_vel_around_Y_sc_ch=diff(E0_GZangle_around_Y_sc_ch)./diff(aVORdata(st).imu.time);
E1_GZang_vel_around_Y_sc_ch=diff(E1_GZangle_around_Y_sc_ch)./diff(aVORdata(st).imu.time);

horizDATA.E0.pos=E0_GZangle_around_Y_sc_ch;
horizDATA.E1.pos=E1_GZangle_around_Y_sc_ch;

horizDATA.E0.vel=E0_GZang_vel_around_Y_sc_ch;
horizDATA.E1.vel=E1_GZang_vel_around_Y_sc_ch;


ff=figure;
subplot(2,2,1);
plot(aVORdata(st).imu.time,E0_GZangle_around_Y_sc_ch,'r');
hold on;
plot(aVORdata(st).imu.time,E1_GZangle_around_Y_sc_ch,'b');
plot(aVORdata(st).imu.time,horizDATA.H.pos,'k.-');
plot(aVORdata(st).imu.time,horizDATA.H.pos+E0_GZangle_around_Y_sc_ch,'m.-');
plot(aVORdata(st).imu.time,horizDATA.H.pos+E1_GZangle_around_Y_sc_ch,'c.-');
title ('Yaw');
ylabel('angle');
ylim([-30 30]);
% calculate velocities
subplot(2,2,3);
plot(aVORdata(st).imu.time(2:end),E0_GZang_vel_around_Y_sc_ch,'r');
hold on;
plot(aVORdata(st).imu.time(2:end),E1_GZang_vel_around_Y_sc_ch,'b');
plot(aVORdata(st).imu.time,horizDATA.H.vel,'k.-');
plot(aVORdata(st).imu.time(2:end),horizDATA.H.vel(2:end)+E0_GZang_vel_around_Y_sc_ch,'m.-');
plot(aVORdata(st).imu.time(2:end),horizDATA.H.vel(2:end)+E1_GZang_vel_around_Y_sc_ch,'c.-');
ylabel('angular velocity');
ylim([-120 120]);

clear E0_GZ_normals E1_GZ_normals  E0_GZangle_around_Y_sc E1_GZangle_around_Y_sc E0_GZangle_around_Y_sc_ch E1_GZangle_around_Y_sc_ch

cyclop_point=(E0EC_inscenecam_calibrationA+E1EC_inscenecam_calibrationA)./2;
% calculate gaze point in 3d
eye_line_vect=(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA)./norm(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA);
i=1;
%near intersection
vect1=E0_GZ_normals_HT(i,:)';
vect2=E1_GZ_normals_HT(i,:)';
GZPpupil0=E0EC_inscenecam_calibrationA+(dot(E1EC_inscenecam_calibrationA-E0EC_inscenecam_calibrationA,cross(vect2,cross(vect1,vect2)))/dot(vect1,cross(vect2,cross(vect1,vect2))))*vect1;
GZPpupil1=E1EC_inscenecam_calibrationA+(dot(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA,cross(vect1,cross(vect1,vect2)))/dot(vect2,cross(vect1,cross(vect1,vect2))))*vect2;
shortest_distanceGPs=norm(GZPpupil1-GZPpupil0);
GZPpupil=GZPpupil1(:)'-(GZPpupil1(:)'-GZPpupil0(:)')./2;
ray2GZPpupil=(GZPpupil-cyclop_point(:)')./norm(GZPpupil-cyclop_point(:)');
GZPdist2cyclop_point=norm(GZPpupil-cyclop_point(:)');
%distance to the eyeline
GZPdist2eyeline=norm(GZPpupil-dot(GZPpupil,eye_line_vect)*eye_line_vect');
% projectGZP on the scene camera image
GZPpupil_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil,'ApplyDistortion',true));
GZPpupil0_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil0','ApplyDistortion',true));
GZPpupil1_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil1','ApplyDistortion',true));
vergence=abs(angle_btw_2_vectors(vect1,vect2));
gazeHT=table(vect1',vect2',...
    GZPpupil0',GZPpupil1',...
    shortest_distanceGPs,GZPpupil,ray2GZPpupil,...
    GZPdist2cyclop_point,GZPdist2eyeline,vergence,...
    GZPpupil_scenecam_image,GZPpupil0_scenecam_image,GZPpupil1_scenecam_image,aVORdata(st).imu.time(i),...
    'VariableNames',{'E0_GZnormal_HT','E1_GZnormal_HT','GZP0','GZP1','shortestdistbtwGZPs','cyGZP','cy_GZnormal_HT',...
    'GZPdist2cyclop_point','GZPdist2eyeline','vergence_angle','GZP_ScCamImg','GZP0_ScCamImg','GZP1_ScCamImg','time'});
for i=2:length(aVORdata(st).imu.time)
    %near intersection
    vect1=E0_GZ_normals_HT(i,:)';
    vect2=E1_GZ_normals_HT(i,:)';
    GZPpupil0=E0EC_inscenecam_calibrationA+(dot(E1EC_inscenecam_calibrationA-E0EC_inscenecam_calibrationA,cross(vect2,cross(vect1,vect2)))/dot(vect1,cross(vect2,cross(vect1,vect2))))*vect1;
    GZPpupil1=E1EC_inscenecam_calibrationA+(dot(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA,cross(vect1,cross(vect1,vect2)))/dot(vect2,cross(vect1,cross(vect1,vect2))))*vect2;
    shortest_distanceGPs=norm(GZPpupil1-GZPpupil0);
    GZPpupil=GZPpupil1(:)'-(GZPpupil1(:)'-GZPpupil0(:)')./2;
    ray2GZPpupil=(GZPpupil-cyclop_point(:)')./norm(GZPpupil-cyclop_point(:)');
    GZPdist2cyclop_point=norm(GZPpupil-cyclop_point(:)');
    %distance to the eyeline
    GZPdist2eyeline=norm(GZPpupil-dot(GZPpupil,eye_line_vect)*eye_line_vect');
    % projectGZP on the scene camera image
    GZPpupil_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil,'ApplyDistortion',true));
    GZPpupil0_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil0','ApplyDistortion',true));
    GZPpupil1_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil1','ApplyDistortion',true));
    vergence=abs(angle_btw_2_vectors(vect1,vect2));
    gazeHT=[gazeHT ; table(vect1',vect2',...
    GZPpupil0',GZPpupil1',...
    shortest_distanceGPs,GZPpupil,ray2GZPpupil,...
    GZPdist2cyclop_point,GZPdist2eyeline,vergence,...
    GZPpupil_scenecam_image,GZPpupil0_scenecam_image,GZPpupil1_scenecam_image,aVORdata(st).imu.time(i),...
    'VariableNames',{'E0_GZnormal_HT','E1_GZnormal_HT','GZP0','GZP1','shortestdistbtwGZPs','cyGZP','cy_GZnormal_HT',...
    'GZPdist2cyclop_point','GZPdist2eyeline','vergence_angle','GZP_ScCamImg','GZP0_ScCamImg','GZP1_ScCamImg','time'})];
end

aVORdata(st).gazeHT=gazeHT;

clear E0_GZ_normals_HT E1_GZ_normals_HT

% vert
st=2;
% get gaze data using calibrationA
E0_GZ_normals=(calibrationA.mset(2).eye(1).Rmat*table2array(aVORdata(st).E0(:,7:9))')';
E1_GZ_normals=(calibrationA.mset(2).eye(2).Rmat*table2array(aVORdata(st).E1(:,7:9))')';

%Interpolate the IMU and pupil streams of data to a common sampling
%frequency (IMU sampling freq)
clear dataMatrixRs dataMatrixFilt dataMatrixRs_in_head_time
dataMatrixRs = ResampleMatrixAV([aVORdata(st).E0.time E0_GZ_normals], IMURateHz, 50, aVORdata(st).E0.time(1), aVORdata(st).E0.time(end));
dataMatrixRs_in_head_time(:,1)=aVORdata(st).imu.time;
dataMatrixRs_in_head_time(:,2)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,2),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,3)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,3),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,4)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,4),aVORdata(st).imu.time,'linear','extrap');
E0_GZ_normals_HT=dataMatrixRs_in_head_time(:,2:4);

clear dataMatrixRs dataMatrixFilt dataMatrixRs_in_head_time
dataMatrixRs = ResampleMatrixAV([aVORdata(st).E1.time E1_GZ_normals], IMURateHz, 50, aVORdata(st).E1.time(1), aVORdata(st).E1.time(end));
dataMatrixRs_in_head_time(:,1)=aVORdata(st).imu.time;
dataMatrixRs_in_head_time(:,2)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,2),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,3)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,3),aVORdata(st).imu.time,'linear','extrap');
dataMatrixRs_in_head_time(:,4)=interp1(dataMatrixRs(:,1),dataMatrixRs(:,4),aVORdata(st).imu.time,'linear','extrap');
E1_GZ_normals_HT=dataMatrixRs_in_head_time(:,2:4);

% calculate the instantaneus angle between the projection of the gaze vector on the YZ plane and the Y axis
E0_GZangle_around_Y_sc=acosd(E0_GZ_normals_HT(:,2)./sqrt((E0_GZ_normals_HT(:,2)).^2+(E0_GZ_normals_HT(:,3)).^2));
E1_GZangle_around_Y_sc=acosd(E1_GZ_normals_HT(:,2)./sqrt((E1_GZ_normals_HT(:,2)).^2+(E1_GZ_normals_HT(:,3)).^2));
E0_GZangle_around_Y_sc_ch=E0_GZangle_around_Y_sc-mean(E0_GZangle_around_Y_sc);
E1_GZangle_around_Y_sc_ch=E1_GZangle_around_Y_sc-mean(E1_GZangle_around_Y_sc);
[b,a]=butter(2,3/(IMURateHz/2));

E0_GZangle_around_Y_sc_ch=filtfilt(b,a,E0_GZangle_around_Y_sc_ch);
E1_GZangle_around_Y_sc_ch=filtfilt(b,a,E1_GZangle_around_Y_sc_ch);


E0_GZang_vel_around_Y_sc_ch=diff(E0_GZangle_around_Y_sc_ch)./diff(aVORdata(st).imu.time);
E1_GZang_vel_around_Y_sc_ch=diff(E1_GZangle_around_Y_sc_ch)./diff(aVORdata(st).imu.time);

vertDATA.E0.pos=E0_GZangle_around_Y_sc_ch;
vertDATA.E1.pos=E1_GZangle_around_Y_sc_ch;

vertDATA.E0.vel=E0_GZang_vel_around_Y_sc_ch;
vertDATA.E1.vel=E1_GZang_vel_around_Y_sc_ch;


subplot(2,2,2);
plot(aVORdata(st).imu.time,E0_GZangle_around_Y_sc_ch,'r');
hold on;
plot(aVORdata(st).imu.time,E1_GZangle_around_Y_sc_ch,'b');
plot(aVORdata(st).imu.time,vertDATA.H.pos,'k.-');
plot(aVORdata(st).imu.time,vertDATA.H.pos+E0_GZangle_around_Y_sc_ch,'m.-');
plot(aVORdata(st).imu.time,vertDATA.H.pos+E1_GZangle_around_Y_sc_ch,'c.-');
title ('Pitch');

%head_position_data=head_position_in_global_timeseries;
ylabel('angle');
ylim([-30 30]);
% calculate velocities
subplot(2,2,4);
plot(aVORdata(st).imu.time(2:end),E0_GZang_vel_around_Y_sc_ch,'r');
hold on;
plot(aVORdata(st).imu.time(2:end),E1_GZang_vel_around_Y_sc_ch,'b');
plot(aVORdata(st).imu.time,vertDATA.H.vel,'k.-');
plot(aVORdata(st).imu.time(2:end),vertDATA.H.vel(2:end)+E0_GZang_vel_around_Y_sc_ch,'m.-');
plot(aVORdata(st).imu.time(2:end),vertDATA.H.vel(2:end)+E1_GZang_vel_around_Y_sc_ch,'c.-');

ylabel('angular velocity');
ylim([-120 120]);

cyclop_point=(E0EC_inscenecam_calibrationA+E1EC_inscenecam_calibrationA)./2;
% calculate gaze point in 3d
eye_line_vect=(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA)./norm(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA);
i=1;
%near intersection
vect1=E0_GZ_normals_HT(i,:)';
vect2=E1_GZ_normals_HT(i,:)';
GZPpupil0=E0EC_inscenecam_calibrationA+(dot(E1EC_inscenecam_calibrationA-E0EC_inscenecam_calibrationA,cross(vect2,cross(vect1,vect2)))/dot(vect1,cross(vect2,cross(vect1,vect2))))*vect1;
GZPpupil1=E1EC_inscenecam_calibrationA+(dot(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA,cross(vect1,cross(vect1,vect2)))/dot(vect2,cross(vect1,cross(vect1,vect2))))*vect2;
shortest_distanceGPs=norm(GZPpupil1-GZPpupil0);
GZPpupil=GZPpupil1(:)'-(GZPpupil1(:)'-GZPpupil0(:)')./2;
ray2GZPpupil=(GZPpupil-cyclop_point(:)')./norm(GZPpupil-cyclop_point(:)');
GZPdist2cyclop_point=norm(GZPpupil-cyclop_point(:)');
%distance to the eyeline
GZPdist2eyeline=norm(GZPpupil-dot(GZPpupil,eye_line_vect)*eye_line_vect');
% projectGZP on the scene camera image
GZPpupil_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil,'ApplyDistortion',true));
GZPpupil0_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil0','ApplyDistortion',true));
GZPpupil1_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil1','ApplyDistortion',true));
vergence=abs(angle_btw_2_vectors(vect1,vect2));
gazeHT=table(vect1',vect2',...
    GZPpupil0',GZPpupil1',...
    shortest_distanceGPs,GZPpupil,ray2GZPpupil,...
    GZPdist2cyclop_point,GZPdist2eyeline,vergence,...
    GZPpupil_scenecam_image,GZPpupil0_scenecam_image,GZPpupil1_scenecam_image,aVORdata(st).imu.time(i),...
    'VariableNames',{'E0_GZnormal_HT','E1_GZnormal_HT','GZP0','GZP1','shortestdistbtwGZPs','cyGZP','cy_GZnormal_HT',...
    'GZPdist2cyclop_point','GZPdist2eyeline','vergence_angle','GZP_ScCamImg','GZP0_ScCamImg','GZP1_ScCamImg','time'});
for i=2:length(aVORdata(st).imu.time)
    %near intersection
    vect1=E0_GZ_normals_HT(i,:)';
    vect2=E1_GZ_normals_HT(i,:)';
    GZPpupil0=E0EC_inscenecam_calibrationA+(dot(E1EC_inscenecam_calibrationA-E0EC_inscenecam_calibrationA,cross(vect2,cross(vect1,vect2)))/dot(vect1,cross(vect2,cross(vect1,vect2))))*vect1;
    GZPpupil1=E1EC_inscenecam_calibrationA+(dot(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA,cross(vect1,cross(vect1,vect2)))/dot(vect2,cross(vect1,cross(vect1,vect2))))*vect2;
    shortest_distanceGPs=norm(GZPpupil1-GZPpupil0);
    GZPpupil=GZPpupil1(:)'-(GZPpupil1(:)'-GZPpupil0(:)')./2;
    ray2GZPpupil=(GZPpupil-cyclop_point(:)')./norm(GZPpupil-cyclop_point(:)');
    GZPdist2cyclop_point=norm(GZPpupil-cyclop_point(:)');
    %distance to the eyeline
    GZPdist2eyeline=norm(GZPpupil-dot(GZPpupil,eye_line_vect)*eye_line_vect');
    % projectGZP on the scene camera image
    GZPpupil_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil,'ApplyDistortion',true));
    GZPpupil0_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil0','ApplyDistortion',true));
    GZPpupil1_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil1','ApplyDistortion',true));
    vergence=abs(angle_btw_2_vectors(vect1,vect2));
    gazeHT=[gazeHT ; table(vect1',vect2',...
        GZPpupil0',GZPpupil1',...
        shortest_distanceGPs,GZPpupil,ray2GZPpupil,...
        GZPdist2cyclop_point,GZPdist2eyeline,vergence,...
        GZPpupil_scenecam_image,GZPpupil0_scenecam_image,GZPpupil1_scenecam_image,aVORdata(st).imu.time(i),...
        'VariableNames',{'E0_GZnormal_HT','E1_GZnormal_HT','GZP0','GZP1','shortestdistbtwGZPs','cyGZP','cy_GZnormal_HT',...
        'GZPdist2cyclop_point','GZPdist2eyeline','vergence_angle','GZP_ScCamImg','GZP0_ScCamImg','GZP1_ScCamImg','time'})];
end

aVORdata(st).gazeHT=gazeHT;

