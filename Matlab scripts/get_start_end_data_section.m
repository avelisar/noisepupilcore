function [start_section,end_section]=get_start_end_data_section(data2plot,text,time_of_line,time_of_line2)
% data2plot is a cell array of two column tables in which first column in the
% pair is the time and the second the data to be plotted
pause('on');

ff0=figure;
set(gcf,'Visible','on');
data_plots=max(size(data2plot));
for i=1:data_plots
    sp(i)=subplot(data_plots,1,i);
    data_subplot=data2plot{i};
    data_name=data_subplot.Properties.VariableNames{2};
    data_subplot=table2array(data_subplot);
    plot(data_subplot(:,1),data_subplot(:,2),'k.:');
    if nargin>2
        YL=ylim;
        hold on;
        line([time_of_line time_of_line],YL,'Color','m')
        if nargin>3
            line([time_of_line2 time_of_line2],YL,'Color','m')
        end
    end
    ylabel(data_name);
end
subplot(data_plots,1,data_plots);
xlabel('Time [s]');
linkaxes(sp,'x');
subplot(data_plots,1,1);
title(text);

pause
dcm_obj = datacursormode(ff0);
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','off','Enable','on')
    % Wait while the user does this.

pause
c_info = getCursorInfo(dcm_obj);

if ~isempty(c_info)
    start_section= c_info.Position(1);
else
    start_section=table2array(data2plot{1}(1,1));
end
dcm_obj.removeAllDataCursors()
clear dcm_obj c_info

pause


dcm_obj = datacursormode(ff0);
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','off','Enable','on')
    % Wait while the user does this.

pause
c_info = getCursorInfo(dcm_obj);

if ~isempty(c_info)
    end_section= c_info.Position(1);
else
    end_section=table2array(data2plot{1}(end,1));
end


close(ff0); 

pause('off');
