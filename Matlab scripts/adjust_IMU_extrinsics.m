function [new_mon_orient_in_scene_cam,new_marker5_inscenecam, imarker_angs]=adjust_IMU_extrinsics(extrinsicsMat,old_mon_orient_in_scene_cam,old_marker5_inscenecam, valid_grid, markers_used, m_width, m_height, E0EC_inscenecam, E1EC_inscenecam)


N_markers=length([markers_used{:}]);
new_mon_orient_in_scene_cam=extrinsicsMat*[old_mon_orient_in_scene_cam; 1 1 1];
new_mon_orient_in_scene_cam=new_mon_orient_in_scene_cam(1:3,1:3);
new_marker5_inscenecam=extrinsicsMat*[old_marker5_inscenecam*1000 ; 1 ];
new_marker5_inscenecam=new_marker5_inscenecam(1:3)./1000;
valid_grid_m=valid_grid([markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=new_mon_orient_in_scene_cam(:,1);
monUvect_y_inscenecam=new_mon_orient_in_scene_cam(:,2);
monUvect_z_inscenecam=new_mon_orient_in_scene_cam(:,3);
imarker_vect_positions_in_scenecam=repmat(new_marker5_inscenecam',N_markers,1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
% homography refrences positions
imarker_E0_horiz_vis_angle=atand(((imarker_vect_positions_in_scenecam-repmat(E0EC_inscenecam',N_markers,1))*monUvect_x_inscenecam)./((imarker_vect_positions_in_scenecam-repmat(E0EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E0_vert_vis_angle=atand(((imarker_vect_positions_in_scenecam-repmat(E0EC_inscenecam',N_markers,1))*monUvect_y_inscenecam)./((imarker_vect_positions_in_scenecam-repmat(E0EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E1_horiz_vis_angle=atand(((imarker_vect_positions_in_scenecam-repmat(E1EC_inscenecam',N_markers,1))*monUvect_x_inscenecam)./((imarker_vect_positions_in_scenecam-repmat(E1EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E1_vert_vis_angle=atand(((imarker_vect_positions_in_scenecam-repmat(E1EC_inscenecam',N_markers,1))*monUvect_y_inscenecam)./((imarker_vect_positions_in_scenecam-repmat(E1EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));

imarker_angs=[valid_grid_m(:,1) imarker_E0_horiz_vis_angle(:) imarker_E0_vert_vis_angle(:) imarker_E1_horiz_vis_angle(:) imarker_E1_vert_vis_angle(:)];