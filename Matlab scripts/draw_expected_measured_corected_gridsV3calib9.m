function fig_HNDLE=draw_expected_measured_corected_gridsV3calib9(imarker_angs,refpcts_angs,measured_angs,set_name)


switch set_name
    case {'9 points','original 9 points'}
        star_indx=[1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    case '5 points'
        star_indx=[1 2 1 3 1 4 1 5];
        frame_indx=[2 3 5 6 2];
    case '13 points'
        star_indx=[1 2 1 3 1 4 1 5 1 7 1 8 1 9 1 10 1 11 1 12 1 13 1 14];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    case 'star'
        star_indx=[1 2 1 3 1 5 1 6 1 8 1 9 1 11 1 12];
        frame_indx=[];
    case 'validation'
        star_indx=[1 2 1 3 1 4 1 5 1 7 1 8 1 9 1 10 1 11 1 12 1 13 1 14 1 15 1 16 1 17 1 18 1 20 1 21 1 23 1 24 1 26 1 27 1 29 1 30];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    otherwise 
        star_indx=[];
        frame_indx=[];
end

grey_color=[0.7 0.7 0.7];
fig_HNDLE=figure;
subplot(1,3,3)
plot(imarker_angs(:,2),imarker_angs(:,3),'.','MarkerSize',20,'Color',grey_color);
hold on;
plot(imarker_angs(2,2),imarker_angs(2,3),'.','MarkerSize',40,'Color',[ 0 0 0]);
plot(imarker_angs(frame_indx,2),imarker_angs(frame_indx,3),'-','Color',grey_color);
plot(imarker_angs(star_indx,2),imarker_angs(star_indx,3),'-','Color',grey_color);
% plot(refpcts_angs(:,2),refpcts_angs(:,3),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,2),refpcts_angs(frame_indx,3),'g-');
% plot(refpcts_angs(star_indx,2),refpcts_angs(star_indx,3),'g-');
plot(measured_angs(:,2),measured_angs(:,3),'r.','MarkerSize',20);
plot(measured_angs(frame_indx,2),measured_angs(frame_indx,3),'r-');
plot(measured_angs(star_indx,2),measured_angs(star_indx,3),'r-');
axis equal
set(gca,'yDir','reverse')
subplot(1,3,1);
plot(imarker_angs(:,4),imarker_angs(:,5),'.','MarkerSize',20,'Color',grey_color);
hold on;
plot(imarker_angs(2,4),imarker_angs(2,5),'.','MarkerSize',40,'Color',[ 0 0 0]);
plot(imarker_angs(frame_indx,4),imarker_angs(frame_indx,5),'-','Color',grey_color);
plot(imarker_angs(star_indx,4),imarker_angs(star_indx,5),'-','Color',grey_color);
% plot(refpcts_angs(:,4),refpcts_angs(:,5),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,4),refpcts_angs(frame_indx,5),'g-');
% plot(refpcts_angs(star_indx,4),refpcts_angs(star_indx,5),'g-');
plot(measured_angs(:,4),measured_angs(:,5),'b.','MarkerSize',20);
plot(measured_angs(frame_indx,4),measured_angs(frame_indx,5),'b-');
plot(measured_angs(star_indx,4),measured_angs(star_indx,5),'b-');
axis equal
set(gca,'yDir','reverse')
subplot(1,3,2);
plot(imarker_angs(:,6),imarker_angs(:,7),'.','MarkerSize',20,'Color',grey_color);
hold on;
plot(imarker_angs(2,6),imarker_angs(2,7),'.','MarkerSize',40,'Color',[ 0 0 0]);
plot(imarker_angs(frame_indx,6),imarker_angs(frame_indx,7),'-','Color',grey_color);
plot(imarker_angs(star_indx,6),imarker_angs(star_indx,7),'-','Color',grey_color);
% plot(refpcts_angs(:,6),refpcts_angs(:,7),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,6),refpcts_angs(frame_indx,7),'g-');
% plot(refpcts_angs(star_indx,6),refpcts_angs(star_indx,7),'g-');
plot(measured_angs(:,6),measured_angs(:,7),'m.','MarkerSize',20);
plot(measured_angs(frame_indx,6),measured_angs(frame_indx,7),'m-');
plot(measured_angs(star_indx,6),measured_angs(star_indx,7),'m-');
axis equal
set(gca,'yDir','reverse')

% 
% 
% horzpcts_indx=[5 6 7];
% vertpcts_indx=[3 6 9];
% frame_indx=[2 3 4 7 10 9 8 5 2];
% grey_color=[0.7 0.7 0.7];
% fig_HNDLE=figure;
% subplot(2,2,1)
% plot(imarker_angs(:,2),imarker_angs(:,3),'.','MarkerSize',20,'Color',grey_color);
% hold on;
% plot(imarker_angs(2,2),imarker_angs(2,3),'.','MarkerSize',40,'Color',[ 0 0 0]);
% plot(imarker_angs(frame_indx,2),imarker_angs(frame_indx,3),'-','Color',grey_color);
% plot(imarker_angs(horzpcts_indx,2),imarker_angs(horzpcts_indx,3),'-','Color',grey_color);
% plot(imarker_angs(vertpcts_indx,2),imarker_angs(vertpcts_indx,3),'-','Color',grey_color);
% plot(refpcts_angs(:,2),refpcts_angs(:,3),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,2),refpcts_angs(frame_indx,3),'g-');
% plot(refpcts_angs(horzpcts_indx,2),refpcts_angs(horzpcts_indx,3),'g-');
% plot(refpcts_angs(vertpcts_indx,2),refpcts_angs(vertpcts_indx,3),'g-');
% plot(measured_angs(:,2),measured_angs(:,3),'r.','MarkerSize',20);
% plot(measured_angs(frame_indx,2),measured_angs(frame_indx,3),'r-');
% plot(measured_angs(horzpcts_indx,2),measured_angs(horzpcts_indx,3),'r-');
% plot(measured_angs(vertpcts_indx,2),measured_angs(vertpcts_indx,3),'r-');
% axis equal
% set(gca,'yDir','reverse')
% subplot(2,2,3);
% plot(imarker_angs(:,4),imarker_angs(:,5),'.','MarkerSize',20,'Color',grey_color);
% hold on;
% plot(imarker_angs(2,4),imarker_angs(2,5),'.','MarkerSize',40,'Color',[ 0 0 0]);
% plot(imarker_angs(frame_indx,4),imarker_angs(frame_indx,5),'-','Color',grey_color);
% plot(imarker_angs(horzpcts_indx,4),imarker_angs(horzpcts_indx,5),'-','Color',grey_color);
% plot(imarker_angs(vertpcts_indx,4),imarker_angs(vertpcts_indx,5),'-','Color',grey_color);
% plot(refpcts_angs(:,4),refpcts_angs(:,5),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,4),refpcts_angs(frame_indx,5),'g-');
% plot(refpcts_angs(horzpcts_indx,4),refpcts_angs(horzpcts_indx,5),'g-');
% plot(refpcts_angs(vertpcts_indx,4),refpcts_angs(vertpcts_indx,5),'g-');
% plot(measured_angs(:,4),measured_angs(:,5),'b.','MarkerSize',20);
% plot(measured_angs(frame_indx,4),measured_angs(frame_indx,5),'b-');
% plot(measured_angs(horzpcts_indx,4),measured_angs(horzpcts_indx,5),'b-');
% plot(measured_angs(vertpcts_indx,4),measured_angs(vertpcts_indx,5),'b-');
% axis equal
% set(gca,'yDir','reverse')
% subplot(2,2,[2 4]);
% plot(imarker_angs(:,6),imarker_angs(:,7),'.','MarkerSize',20,'Color',grey_color);
% hold on;
% plot(imarker_angs(2,6),imarker_angs(2,7),'.','MarkerSize',40,'Color',[ 0 0 0]);
% plot(imarker_angs(frame_indx,6),imarker_angs(frame_indx,7),'-','Color',grey_color);
% plot(imarker_angs(horzpcts_indx,6),imarker_angs(horzpcts_indx,7),'-','Color',grey_color);
% plot(imarker_angs(vertpcts_indx,6),imarker_angs(vertpcts_indx,7),'-','Color',grey_color);
% plot(refpcts_angs(:,6),refpcts_angs(:,7),'g.','MarkerSize',20);
% plot(refpcts_angs(frame_indx,6),refpcts_angs(frame_indx,7),'g-');
% plot(refpcts_angs(horzpcts_indx,6),refpcts_angs(horzpcts_indx,7),'g-');
% plot(refpcts_angs(vertpcts_indx,6),refpcts_angs(vertpcts_indx,7),'g-');
% plot(measured_angs(:,6),measured_angs(:,7),'c.','MarkerSize',20);
% plot(measured_angs(frame_indx,6),measured_angs(frame_indx,7),'c-');
% plot(measured_angs(horzpcts_indx,6),measured_angs(horzpcts_indx,7),'c-');
% plot(measured_angs(vertpcts_indx,6),measured_angs(vertpcts_indx,7),'c-');
% axis equal
% set(gca,'yDir','reverse')

