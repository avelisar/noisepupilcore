clear;
isFar=1;
isHFree=1;
if isHFree
    path='Z:\DATA\multiple validations pupil check\preprocessed\KPA\head free\far viewing\input files\';
else
    path='Z:\DATA\multiple validations pupil check\preprocessed\KPA\head fixed\far viewing\input files\';
end
ext='.csv';
file_refs='refernce_locations';
ref_loc = import_csv_references_file([path file_refs ext]);
load([path 'mat_data.mat']);

%find the reference points during best fixation times
% calibration1
for i=1:length(ValidationMapData_calibration1)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration1(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration1(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration1(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration1(i).bestfixation_times(2),2)));
    ValidationMapData_calibration1(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end
% calibration5pcts
for i=1:length(ValidationMapData_calibration5pcts)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration5pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration5pcts(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration5pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration5pcts(i).bestfixation_times(2),2)));
    ValidationMapData_calibration5pcts(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end
% calibration9pcts
for i=1:length(ValidationMapData_calibration9pcts)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration9pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration9pcts(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration9pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration9pcts(i).bestfixation_times(2),2)));
    ValidationMapData_calibration9pcts(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end
% calibration13pcts
for i=1:length(ValidationMapData_calibration13pcts)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration13pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration13pcts(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibration13pcts(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibration13pcts(i).bestfixation_times(2),2)));
    ValidationMapData_calibration13pcts(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end
% calibrationstar
for i=1:length(ValidationMapData_calibrationstar)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibrationstar(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibrationstar(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_calibrationstar(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_calibrationstar(i).bestfixation_times(2),2)));
    ValidationMapData_calibrationstar(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end

% validation
for i=1:length(ValidationMapData_validation)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapData_validation(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_validation(i).bestfixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapData_validation(i).bestfixation_times(1) & E0pupil.time<=ValidationMapData_validation(i).bestfixation_times(2),2)));
    ValidationMapData_validation(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end

% small saccades
for i=1:length(ValidationMapDataSSac)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapDataSSac(i).fixation_times(1) & E0pupil.time<=ValidationMapDataSSac(i).fixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapDataSSac(i).fixation_times(1) & E0pupil.time<=ValidationMapDataSSac(i).fixation_times(2),2)));
    ValidationMapDataSSac(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end

% large saccades
for i=1:length(ValidationMapDataLSac)
    start_indx=min(table2array(E0pupil(E0pupil.time>=ValidationMapDataLSac(i).fixation_times(1) & E0pupil.time<=ValidationMapDataLSac(i).fixation_times(2),2)));
    end_indx=max(table2array(E0pupil(E0pupil.time>=ValidationMapDataLSac(i).fixation_times(1) & E0pupil.time<=ValidationMapDataLSac(i).fixation_times(2),2)));
    ValidationMapDataLSac(i).refs=round(mean(table2array(ref_loc(ref_loc.RefNo>=start_indx & ref_loc.RefNo<=end_indx,1:2)),1));
end

% markers' position in scene cam CF
m_width=1.58;
m_height=0.93;
valid_grid=[1 5.5479	7.7026;...
    2  50	7.7026;...
    3 94.4521	7.7026;...
    4   5.5479	50;...
    5  50  50;...
    6 94.4521	50;...
    7   5.5479	92.2974;...
    8  50	92.2974;...
    9 94.4521	92.2974;...
    10 34.8863	25.4675;...
    11 34.8863	74.5325;...
    12 65.1137	25.4675;....
    13 65.1137	74.5325;...
    14 41.1096	41.5405;...
    15 41.1096	58.4595;...
    16 58.8904	58.4595;...
    17 58.8904	41.5405;...
    18 50	33.081;...
    19 50	41.5405;...
    20 50	58.4595;...
    21 50	66.919;...
    22 40.0414	50;...
    23 45.0207	50;...
    24 54.9793	50;...
    25 59.9586	50];
viewing_dist=1.8;
% calculate screen corners
sc_corners(:,1)=([0; 0; 100; 100]-50)*m_width/100;
sc_corners(:,2)=([0; 100; 100; 0]-50)*m_height/100;

calibration1_markers_used={5 1 2 3 4 5 6 7 8 9 5};
calibration9pcts_markers_used={5 1 2 3 4 5 6 7 8 9 5};
calibration5pcts_markers_used={5 1 3 7 9 5};
calibration13pcts_markers_used={5 1 2 3 4 5 6 7 8 9 10 12 13 11 5};
calibrationstar_markers_used={5 18 19 5 20 21 5 22 23 5 24 25 5};
validation_markers_used={5 1 2 3 4 5 6 7 8 9 10 12 13 11 14 15 16 17 5 18 19 5 20 21 5 22 23 5 24 25 5};

% set up camera matrix and distortion
scene_cam_res=[1280, 720];
intrinsics_mat=[[794.3311439869655, 0.0, 633.0104437728625]; [0.0, 793.5290139393004, 397.36927353414865]; [0.0, 0.0, 1.0]];
scenecameraParams = cameraParameters('IntrinsicMatrix',intrinsics_mat');
paramStruct = toStruct(scenecameraParams);
paramStruct.RadialDistortion=[-0.3758628065070806, 0.1643326166951343, 0.03343691733865076];
paramStruct.TangentialDistortion=[0.00012182540692089567, 0.00013422608638039466];
paramStruct.ImageSize=[1280, 720];
paramStruct.NumRadialDistortionCoefficients=3;
rotationVector = rotationMatrixToVector(eye(3));
paramStruct.RotationVectors=rotationVector;
paramStruct.TranslationVectors=[0 0 0];
scenecameraParams = cameraParameters(paramStruct);

% get world position for estimated markers
%calibration1
valid_grid_m=valid_grid([calibration1_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_calibration1(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_calibration1(:,2);
marker5_inscenecam=marker5_inscenecam_calibration1;
imarker_vect_positions_in_scenecam_calibration1=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m
%calibration5pcts
valid_grid_m=valid_grid([calibration5pcts_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_calibration5pcts(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_calibration5pcts(:,2);
marker5_inscenecam=marker5_inscenecam_calibration5pcts;
imarker_vect_positions_in_scenecam_calibration5pcts=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m
%calibration9pcts
valid_grid_m=valid_grid([calibration9pcts_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_calibration9pcts(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_calibration9pcts(:,2);
marker5_inscenecam=marker5_inscenecam_calibration9pcts;
imarker_vect_positions_in_scenecam_calibration9pcts=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m
%calibration13pcts
valid_grid_m=valid_grid([calibration13pcts_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_calibration13pcts(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_calibration13pcts(:,2);
marker5_inscenecam=marker5_inscenecam_calibration13pcts;
imarker_vect_positions_in_scenecam_calibration13pcts=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m
%calibrationstar
valid_grid_m=valid_grid([calibrationstar_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_calibrationstar(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_calibrationstar(:,2);
marker5_inscenecam=marker5_inscenecam_calibrationstar;
imarker_vect_positions_in_scenecam_calibrationstar=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m
%validation
valid_grid_m=valid_grid([validation_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam_validation(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam_validation(:,2);
marker5_inscenecam=marker5_inscenecam_validation;
imarker_vect_positions_in_scenecam_validation=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m


%calibration1
refPoints=reshape([ValidationMapData_calibration1(:).refs],2,length(ValidationMapData_calibration1))';
worldPoints=imarker_vect_positions_in_scenecam_calibration1*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_calibration1=x;

%calibration5pcts
refPoints=reshape([ValidationMapData_calibration5pcts(:).refs],2,length(ValidationMapData_calibration5pcts))';
worldPoints=imarker_vect_positions_in_scenecam_calibration5pcts*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_calibration5pcts=x;

%calibration9pcts
refPoints=reshape([ValidationMapData_calibration9pcts(:).refs],2,length(ValidationMapData_calibration9pcts))';
worldPoints=imarker_vect_positions_in_scenecam_calibration9pcts*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_calibration9pcts=x;

%calibration13pcts
refPoints=reshape([ValidationMapData_calibration13pcts(:).refs],2,length(ValidationMapData_calibration13pcts))';
worldPoints=imarker_vect_positions_in_scenecam_calibration13pcts*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_calibration13pcts=x;

%calibrationstar
refPoints=reshape([ValidationMapData_calibrationstar(:).refs],2,length(ValidationMapData_calibrationstar))';
worldPoints=imarker_vect_positions_in_scenecam_calibrationstar*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_calibrationstar=x;

%validation
refPoints=reshape([ValidationMapData_validation(:).refs],2,length(ValidationMapData_validation))';
worldPoints=imarker_vect_positions_in_scenecam_validation*1000;
[x,~]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints);
x_validation=x;

% get the averaged extrinsics
mat=[x_calibration1(:) x_calibration5pcts(:) x_calibration9pcts(:) x_calibration13pcts(:) x_calibrationstar(:) x_validation(:)];
x_ave=mean(mat,2);
theta=x_ave(1);
beta=x_ave(2);
alpha=x_ave(3);
tx=x_ave(4);
ty=x_ave(5);
tz=x_ave(6);
Rz=[cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];
Rx=[1 0 0; 0 cosd(alpha) -sind(alpha); 0 sind(alpha) cosd(alpha)];
Ry=[cosd(beta) 0  sind(beta);0 1 0; -sind(beta) 0 cosd(beta)];
rot_mat=Rz*Ry*Rx;
tvect=[tx ty tz];
imagePoints_calibration1 = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));
imagePoints_calibration5pcts = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));
imagePoints_calibration9pcts = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));
imagePoints_calibration13pcts = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));
imagePoints_calibrationstar = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));
imagePoints_validation = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));

% recalculate the new worldPoints
%calibration1
undistortedPoints = undistortPoints(imagePoints_validation,scenecameraParams);
refPoints=reshape([ValidationMapData_validation(:).refs],2,length(ValidationMapData_validation))';
undistortedRefs=undistortPoints(refPoints,scenecameraParams);
worldPoints_validation = pointsToWorld(scenecameraParams,eye(3),[0 0 1],undistortedPoints);
worldRefs_validation = pointsToWorld(scenecameraParams,eye(3),[0 0 1],undistortedRefs);

ref_world_vect_validation=[worldRefs_validation ones(length(worldRefs_validation),1)]./sqrt(sum([worldRefs_validation ones(length(worldRefs_validation),1)].^2,2));


valid_grid_m=valid_grid([validation_markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
new_mon_orient_in_scene_cam_validation=[rot_mat' tvect(:); 0 0 0 1]*[mon_orient_in_scene_cam_validation; 1 1 1];
new_mon_orient_in_scene_cam_validation=new_mon_orient_in_scene_cam_validation(1:3,1:3);
monUvect_x_inscenecam=new_mon_orient_in_scene_cam_validation(:,1);
monUvect_y_inscenecam=new_mon_orient_in_scene_cam_validation(:,2);
new_marker5_inscenecam_validation=[rot_mat' tvect(:); 0 0 0 1]*[marker5_inscenecam_validation*1000 ; 1 ];
new_marker5_inscenecam_validation=new_marker5_inscenecam_validation(1:3)./1000;
marker5_inscenecam=new_marker5_inscenecam_validation;
sc_corners_vect_positions_in_scenecam_validation=repmat(marker5_inscenecam',size(sc_corners,1),1)+sc_corners(:,1)*monUvect_x_inscenecam'+sc_corners(:,2)*monUvect_y_inscenecam';
imarker_vect_positions_in_scenecam_validation=repmat(marker5_inscenecam',size(valid_grid_m,1),1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
clear valid_grid_m

imarker_vect_in_scenecam_validation=imarker_vect_positions_in_scenecam_validation./sqrt(sum(imarker_vect_positions_in_scenecam_validation.^2,2));


for i=1:length(ref_world_vect_validation)
    error_angle(i)=angle_btw_2_vectors(ref_world_vect_validation(i,:),imarker_vect_in_scenecam_validation(i,:));
end

% calculate world vectors intersect with the screen
ref_points_onscreen=zeros(size(ref_world_vect_validation));
for i=1:size(ref_world_vect_validation,1)
    ref_points_onscreen(i,:)=get_coordinates_of_intersection_point_btw_plane_gazeline(new_marker5_inscenecam_validation,new_mon_orient_in_scene_cam_validation(:,3),[0 0 0],ref_world_vect_validation(i,:));
    error_dist(i)=norm(imarker_vect_positions_in_scenecam_validation(i,:)-ref_points_onscreen(i,:));
end
difRefMarkersTable=table([validation_markers_used{:}]',error_angle(:),error_dist(:),'VariableNames',{'MarkerID','err_ang_deg','err_dist_onscreen_m'});
writetable(difRefMarkersTable,[path 'difRefMarkersafterajustextrinsics.xls'] ,'FileType','spreadsheet','Sheet','errors');

ff1=figure;
for i=1:length(ValidationMapData_validation)
    ref_validation(i,:)=[ValidationMapData_validation(i).refs(:,1),ValidationMapData_validation(i).refs(:,2)];
    
end
plot(ref_validation(:,1),ref_validation(:,2),'k.');
hold on;
plot(imagePoints_validation(:,1),imagePoints_validation(:,2),'b*')
axis equal
set(gca,'ydir','reverse','ylim',[1 720],'xlim',[1 1280])
title('Markers in Scene Camera image');
legend('detected in the image', 'reprojected using IMU');

ff2=figure;
axes_mat=10*[1 0 0; -1 0 0; 0 1 0; 0 -1 0; 0 0 1; 0 0 -1];
plot3(axes_mat(1:2,1)',axes_mat(1:2,2)', axes_mat(1:2,3)','b','LineWidth',2)
hold on
plot3(axes_mat(3:4,1)',axes_mat(3:4,2)', axes_mat(3:4,3)','g','LineWidth',2)
plot3(axes_mat(5:6,1)',axes_mat(5:6,2)', axes_mat(5:6,3)','r','LineWidth',2)

axis equal
plane_color=[0.8 0.8 0.8];
fill3(1000*sc_corners_vect_positions_in_scenecam_validation(:,1),1000*sc_corners_vect_positions_in_scenecam_validation(:,2),1000*sc_corners_vect_positions_in_scenecam_validation(:,3),plane_color,'FaceAlpha',0.5);
plot3(1000*imarker_vect_positions_in_scenecam_validation(:,1),1000*imarker_vect_positions_in_scenecam_validation(:,2),1000*imarker_vect_positions_in_scenecam_validation(:,3),'.','Color',[0.8 0.6 0.6],'MarkerSize',30);
plot3(1000*imarker_vect_positions_in_scenecam_validation(2,1),1000*imarker_vect_positions_in_scenecam_validation(2,2),1000*imarker_vect_positions_in_scenecam_validation(2,3),'.','Color',[0.6 0.8 0.5],'MarkerSize',30);
plot3(1000*ref_points_onscreen(:,1),1000*ref_points_onscreen(:,2),1000*ref_points_onscreen(:,3),'.','Color',[0.1 0.1 0.1],'MarkerSize',30);
for i=1:length(ValidationMapData_validation)
    plot3([0 1000*imarker_vect_positions_in_scenecam_validation(i,1)],[0 1000*imarker_vect_positions_in_scenecam_validation(i,2)],[0 1000*imarker_vect_positions_in_scenecam_validation(i,3)],'b');
    plot3([0 1000*ref_points_onscreen(i,1)],[0 1000*ref_points_onscreen(i,2)],[0 1000*ref_points_onscreen(i,3)],'k');
end

ff3=figure;
plot(ref_points_onscreen*1000*monUvect_x_inscenecam+m_width*500,ref_points_onscreen*1000*monUvect_y_inscenecam+m_height*500,'k.');
hold on;
plot(imarker_vect_positions_in_scenecam_validation*1000*monUvect_x_inscenecam+m_width*500,imarker_vect_positions_in_scenecam_validation*1000*monUvect_y_inscenecam+m_height*500,'b*');
axis equal
set(gca,'ydir','reverse','ylim',[0 m_height*1000],'xlim',[0 m_width*1000])

added_extrinsics=[rot_mat' tvect(:); 0 0 0 1];
save([path 'mat_data.mat'], 'added_extrinsics','ValidationMapData_calibration1',...
    'ValidationMapData_calibration5pcts','ValidationMapData_calibration9pcts',...
    'ValidationMapData_calibration13pcts','ValidationMapData_calibrationstar',...
    'ValidationMapData_validation','scenecameraParams','x_ave','imagePoints_calibration1',...
    'imagePoints_calibration5pcts','imagePoints_calibration9pcts',...
    'imagePoints_calibration13pcts','imagePoints_calibrationstar',...
    'imagePoints_validation','ref_world_vect_validation','error_angle',...
    'error_dist', '-append');
savefig(ff1,[path 'scene_cam_reference_IMUextrinsics.fig']);
savefig(ff2,[path '3dscene_cam_space_screen.fig']);
savefig(ff3,[path 'screen_space_refs.fig']);
