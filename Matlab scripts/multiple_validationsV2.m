clear;
isFar=1;
isHFree=1;
if isHFree
    path='Z:\DATA\multiple validations pupil check\preprocessed\KPA\head free\far viewing\input files\';
else
    path='Z:\DATA\multiple validations pupil check\preprocessed\KPA\head fixed\far viewing\input files\';
end

ext='.csv';
file_pupil='pupil_positions';
file_IMU='imu_data';
file_notes='annotations';
file_refs='refernce_locations';
file_cal1='calib_orig_9pcts';
file_cal2='calib_5pcts';
file_cal3='calib_9pcts';
file_cal4='calib_13pcts';
file_cal5='calib_star';
file_cal6='calib_VOR';
file_valid='calib_validation';

% Synch data
IMURateHz=400;
synch_dir='y';

sbj_id='KPA';

pupil_data = get_pupil_datav2([path file_pupil ext]);
IMU_data = get_IMU_datav2([path file_IMU ext]);
annotations = import_csv_annotations_file([path file_notes ext]);
ref_loc = import_csv_references_file([path file_refs ext]);
calibration1 = get_calibration([path file_cal1 ext]);
calibration5pcts = get_calibration([path file_cal2 ext]);
calibration9pcts = get_calibration([path file_cal3 ext]);
calibration13pcts = get_calibration([path file_cal4 ext]);
calibrationstar = get_calibration([path file_cal5 ext]);
if isHFree
    calibrationVOR = get_calibration([path file_cal6 ext]);
end
validation = get_calibration([path file_valid ext]);



pupil_time_zero=pupil_data.pupil_timestamp(1);
if strcmpi(sbj_id,'NMS') || strcmpi(sbj_id,'HJS')
    E03D=pupil_data(pupil_data.method=='pye3d 0.0.4 post-hoc' & pupil_data.eye_id==0 & pupil_data.confidence>0.70,:);
    E13D=pupil_data(pupil_data.method=='pye3d 0.0.4 post-hoc' & pupil_data.eye_id==1 & pupil_data.confidence>0.70,:);
else
    E03D=pupil_data(pupil_data.method=='pye3d 0.0.6 post-hoc' & pupil_data.eye_id==0 & pupil_data.confidence>0.70,:);
    E13D=pupil_data(pupil_data.method=='pye3d 0.0.6 post-hoc' & pupil_data.eye_id==1 & pupil_data.confidence>0.70,:);
end

E0pupil_time=E03D.pupil_timestamp-pupil_time_zero;
E1pupil_time=E13D.pupil_timestamp-pupil_time_zero;

world_indx_synch=annotations.index(contains(string(annotations.label),'synch'));
for i=1:length(world_indx_synch)
    synch_markers_times(i) = E0pupil_time(find(E03D.world_index>=world_indx_synch(i),1,'first'));
end
[synch_IMU, synch_pupil]=synch_IMU_pupil_streamsV2(IMU_data,E03D,E13D,pupil_time_zero,synch_dir,IMURateHz,synch_markers_times);
imu_time=IMU_data.TimeStamps-synch_IMU+synch_pupil;


% check synch
E0pupil=E03D(E0pupil_time>=0,:);
E0pupil.time=E0pupil_time(E0pupil_time>=0 ,:);
E1pupil=E13D(E1pupil_time>=0,:);
E1pupil.time=E1pupil_time(E1pupil_time>=0 ,:);
IMU_data_s=IMU_data(imu_time>=0,:);
IMU_data_s.time=imu_time(imu_time>=0,:);

% find validation time
    
    world_indx_se=annotations.index(contains(string(annotations.label),'validation'));
    world_indx_start=world_indx_se(1);
    world_indx_end=world_indx_se(2);
    
    ref_val=ref_loc(table2array(ref_loc(:,3))>=world_indx_start & table2array(ref_loc(:,3))<=world_indx_end,:);
    validation_time=[ E0pupil.time(find(E0pupil.world_index>=world_indx_start,1,'first')) E0pupil.time(find(E0pupil.world_index<=world_indx_end,1,'last'))];
    ann_val=annotations(annotations.index>=world_indx_start & annotations.index<=world_indx_end,:);
    homography_coef=12;

% find platform  mov
world_indx_start=annotations.index(contains(string(annotations.label),'start move slow'));
world_indx_end=annotations.index(contains(string(annotations.label),'end move fast'));

ref_mov=ref_loc(table2array(ref_loc(:,3))>=world_indx_start(1) & table2array(ref_loc(:,3))<=world_indx_end(end),:);
platform_mov_time=[ E0pupil.time(find(E0pupil.world_index>=world_indx_start(1),1,'first')) E0pupil.time(find(E0pupil.world_index<=world_indx_end(end),1,'last'))];
ann_mov=annotations(annotations.index>=world_indx_start(1) & annotations.index<=world_indx_end(end),:);

figure;
sp(1)=subplot(2,1,1);
plot(E0pupil.time,E0pupil.norm_pos_y,'.-')
hold on;
YL=ylim;
line([validation_time(1) validation_time(1)],YL,'Color','r');
line([validation_time(2) validation_time(2)],YL,'Color','r');

line([platform_mov_time(1) platform_mov_time(1)],YL,'Color','m');
line([platform_mov_time(2) platform_mov_time(2)],YL,'Color','m');

sp(2)=subplot(2,1,2);
plot(IMU_data_s.time,IMU_data_s.LinAccYg,'.-')
%plot(IMU_data_s.time,IMU_data_s.GyroZdegs,'.-')
hold on;
YL=ylim;
line([validation_time(1) validation_time(1)],YL,'Color','r');
line([validation_time(2) validation_time(2)],YL,'Color','r');
line([platform_mov_time(1) platform_mov_time(1)],YL,'Color','m');
line([platform_mov_time(2) platform_mov_time(2)],YL,'Color','m');
linkaxes(sp,'x')
if isfile([path 'mat_data.mat'])
    save([path 'mat_data.mat'],'pupil_data','IMU_data','annotations','ref_loc','calibration1','calibration5pcts', 'calibration9pcts', 'calibration13pcts', 'calibrationstar','validation','pupil_time_zero','E03D','E13D', '-append');
else
    save([path 'mat_data.mat'],'pupil_data','IMU_data','annotations','ref_loc','calibration1','calibration5pcts', 'calibration9pcts', 'calibration13pcts', 'calibrationstar','validation','pupil_time_zero','E03D','E13D');
end
if isHFree
    if isfile([path 'mat_data.mat'])
        save([path 'mat_data.mat'],'calibrationVOR', '-append');
        
    else
        save([path 'mat_data.mat'],'calibrationVOR');
    end
end
clear pupil_data
save([path 'mat_data.mat'],'IMURateHz','synch_IMU','synch_pupil','E0pupil', 'E1pupil', 'IMU_data_s', '-append');
clear E03D E13D IMU_data
% get eye centers
E02scene_cam_mat=calibration1.mset(2).eye(1).RTmat;
E0_center_scene_cam=E02scene_cam_mat*[mean(E0pupil.sphere_center_x) mean(E0pupil.sphere_center_y) mean(E0pupil.sphere_center_z) 1]';

E12scene_cam_mat=calibration1.mset(2).eye(2).RTmat;
E1_center_scene_cam=E12scene_cam_mat*[mean(E1pupil.sphere_center_x) mean(E1pupil.sphere_center_y) mean(E1pupil.sphere_center_z) 1]';
% get _the most impotant eye data
E0_data_raw=E0pupil(:,[ 1 2 4 5 6 14 24 25 26 35]);
E1_data_raw=E1pupil(:,[ 1 2 4 5 6 14 24 25 26 35]);
% get the most important IMU_data
IMU_data_raw=IMU_data_s(:,[7:12 17:20]);
save([path 'mat_data.mat'],'E0_data_raw', 'E1_data_raw', 'IMU_data_raw',...
    'E0_center_scene_cam','E1_center_scene_cam','validation_time',...
    'platform_mov_time', 'ref_val','ann_val', 'ref_mov','ann_mov','-append');
clear E0pupil E1pupil IMU_data_s



% split data for validation and platform motion
E0_data_validation=E0_data_raw(E0_data_raw.time>=validation_time(1) & E0_data_raw.time<=validation_time(2),:);
E1_data_validation=E1_data_raw(E1_data_raw.time>=validation_time(1) & E1_data_raw.time<=validation_time(2),:);
IMU_data_validation=IMU_data_raw(IMU_data_raw.time>=validation_time(1) & IMU_data_raw.time<=validation_time(2),:);
% %%%%%
E0_data_platform_mov=E0_data_raw(E0_data_raw.time>=platform_mov_time(1) & E0_data_raw.time<=platform_mov_time(2),:);
E1_data_platform_mov=E1_data_raw(E1_data_raw.time>=platform_mov_time(1) & E1_data_raw.time<=platform_mov_time(2),:);
IMU_data_platform_mov=IMU_data_raw(IMU_data_raw.time>=platform_mov_time(1) & IMU_data_raw.time<=platform_mov_time(2),:);



% sensor direct measurements
figure;
subplot(3,2,1);
plot(IMU_data_validation.time,IMU_data_validation.GyroXdegs,'b.-');
axis tight
ylabel('GyroX');
subplot(3,2,3);
plot(IMU_data_validation.time,IMU_data_validation.GyroYdegs,'g.-');
axis tight
ylabel('GyroY');
subplot(3,2,5);
plot(IMU_data_validation.time,IMU_data_validation.GyroZdegs,'r.-');
axis tight
ylabel('GyroZ');
xlabel('Time [s]');

subplot(3,2,2);
plot(IMU_data_validation.time,IMU_data_validation.LinAccXg,'b.-');
axis tight
ylabel('LinAccX');
subplot(3,2,4);
plot(IMU_data_validation.time,IMU_data_validation.LinAccYg,'g.-');
axis tight
ylabel('LinAccY');
subplot(3,2,6);
plot(IMU_data_validation.time,IMU_data_validation.LinAccZg,'r.-');
axis tight
ylabel('LinAccZ');
xlabel('Time [s]');

[IMU_validation_angle_pos]=get_IMU_rot_angle_and_shift(IMU_data_validation,IMURateHz,5);
figure;
subplot(3,2,1);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,1),'b.-');
axis tight
ylabel('RotAngX');
subplot(3,2,3);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,2),'g.-');
axis tight
ylabel('RotAngY');
subplot(3,2,5);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,3),'r.-');
axis tight
ylabel('RotAngZ');
xlabel('Time [s]');

subplot(3,2,2);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,4),'b.-');
axis tight
ylabel('shiftX');
subplot(3,2,4);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,5),'g.-');
axis tight
ylabel('shiftY');
subplot(3,2,6);
plot(IMU_data_validation.time,IMU_validation_angle_pos(:,6),'r.-');
axis tight
ylabel('shiftZ');
xlabel('Time [s]');

