function [IMU_angle_pos_mat]=get_IMU_rot_angle_and_shift(IMU_data,IMURateHz, sm_coef)


% check if IMU_data table has the necessary columns
G_ct=9.81;
which_col=contains(IMU_data.Properties.VariableNames,'LinAcc');
TF1=sum(which_col);
which_col1=contains(IMU_data.Properties.VariableNames,'Gyro');
TF2=sum(which_col1);
f_cutoff=25;
[b,a]=butter(8,f_cutoff/IMURateHz/2);

time=IMU_data.time;
time=time-time(1);
lin_pos_mat=zeros(length(time),3);
rot_ang_mat=lin_pos_mat;
if TF1==3
    % detrend 
    lin_acc_mat=table2array(IMU_data(:,which_col));
    for i=1:3
        lin_acc_vect=G_ct*lin_acc_mat(:,i);
        lin_acc_dt=movmean(lin_acc_vect,sm_coef*IMURateHz);
        lin_acc_vect_dt=lin_acc_vect-lin_acc_dt;
        lin_acc_filt=filtfilt(b,a,lin_acc_vect_dt);
        lin_vel_vect=cumtrapz(time,lin_acc_filt);
        lin_vel_dt=movmean(lin_vel_vect,sm_coef*IMURateHz);
        lin_vel_vect_dt=lin_vel_vect-lin_vel_dt;
        lin_vel_filt=filtfilt(b,a,lin_vel_vect_dt);
        lin_pos_vect=cumtrapz(time,lin_vel_filt);
        lin_pos_dt=movmean(lin_pos_vect,sm_coef*IMURateHz);
        lin_pos_vect_dt=lin_pos_vect-lin_pos_dt;
        lin_pos_filt=filtfilt(b,a,lin_pos_vect_dt);
        lin_pos_mat(:,i)=lin_pos_filt(:);
    end
end

f_cutoff=40;
[b,a]=butter(8,f_cutoff/IMURateHz/2);


if TF2==3
    % detrend 
    rot_vel_mat=table2array(IMU_data(:,which_col1));
    for i=1:3
        rot_vel_vect=rot_vel_mat(:,i);
        rot_vel_dt=movmean(rot_vel_vect,sm_coef*IMURateHz);
        rot_vel_vect_dt=rot_vel_vect-rot_vel_dt;
        rot_vel_filt=filtfilt(b,a,rot_vel_vect_dt);
        rot_ang_vect=cumtrapz(time,rot_vel_filt);
        rot_ang_dt=movmean(rot_ang_vect,sm_coef*IMURateHz);
        rot_ang_vect_dt=rot_ang_vect-rot_ang_dt;
        rot_ang_filt=filtfilt(b,a,rot_ang_vect_dt);
        rot_ang_mat(:,i)=rot_ang_filt(:);
    end
end   
IMU_angle_pos_mat=[rot_ang_mat lin_pos_mat];