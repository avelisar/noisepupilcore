function [all_stats,DataAgain,all_precision]=get_pop_statsV5v1(DataTable,fig_title,fig_ylabel)


DataAgain=[];
all_stats=[];
all_precision=[];


for cal=1:(width(DataTable)-1)
if lillietest(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1)))
        m5=median(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))));
    q5=quantile(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))),[ 0.25 0.75]);
    mads = mad(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))),1);
    s5=[q5(2)-q5(1) mads 0 q5(1) q5(2) 1];
else
    m5=mean(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))));
    std5=std(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))));
    mads = mad(abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1))),0);
    cvs=std5./m5;
    s5=[std5 mads cvs 0 0 0];
end
otherms=abs(table2array(DataTable(str2num(table2array(DataTable(:,1)))~=5,cal+1)));
all_ms=[m5; otherms(:)];
if lillietest(all_ms)
    qs=quantile(all_ms,[ 0.5 0.25 0.75]);
    mads = mad(all_ms,1);
    stats=[qs(1) qs(3)-qs(2) mads 0 qs(2) qs(3) 1];
else
    mads = mad(all_ms,1);
    cvs= std(all_ms)./mean(all_ms);
    stats=[mean(all_ms) std(all_ms) mads cvs 0 0 0];
end
all_stats=[all_stats; stats];
DataAgain=[ DataAgain all_ms(:)];
all_precision=[all_precision; s5];
end


figure;
boxplot(DataAgain);
title(fig_title);
ylabel(fig_ylabel);
set(gca,'XTickLabel',DataTable.Properties.VariableNames(2:end));