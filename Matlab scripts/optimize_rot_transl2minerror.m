function [x,fval]=optimize_rot_transl2minerror(scenecameraParams,worldPoints,refPoints)

f = @(x)imagePoints2ref_error(x,scenecameraParams,worldPoints,refPoints);
[x,fval] = fminsearch(f,[3,4,-5,0,0,0]);