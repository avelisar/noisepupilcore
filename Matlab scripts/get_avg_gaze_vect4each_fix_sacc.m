function [ValidationMapDataSacc]=get_avg_gaze_vect4each_fix_sacc(period_time, E0_data_raw, E1_data_raw, IMU_data_raw, calibration1)


E0_data_validation=E0_data_raw(E0_data_raw.time>=period_time(1) & E0_data_raw.time<=period_time(2),:);
E1_data_validation=E1_data_raw(E1_data_raw.time>=period_time(1) & E1_data_raw.time<=period_time(2),:);


% check validation times
ff=figure;
sp(1)=subplot(2,1,1);
plot(E0_data_validation.time,E0_data_validation.norm_pos_x,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_x,'.-')
YL=ylim;

sp(2)=subplot(2,1,2);
plot(E0_data_validation.time,E0_data_validation.norm_pos_y,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_y,'.-')

YL=ylim;
linkaxes(sp,'x')


% choose the best fixation period during each marker presentation and
% calculate the mean gaze vectors
keep_going=1;
start_time=period_time(1);
end_time=period_time(2);
p=1;
while keep_going
    indx_imu=IMU_data_raw.time>=start_time & IMU_data_raw.time<=end_time;
    indx_E0=E0_data_raw.time>=start_time & E0_data_raw.time<=end_time;
    indx_E1=E1_data_raw.time>=start_time & E1_data_raw.time<=end_time;
    data2plot={IMU_data_raw(indx_imu,[10 1]), E0_data_raw(indx_E0,[10 4]), E0_data_raw(indx_E0,[10 5]), E1_data_raw(indx_E1,[10 4]), E1_data_raw(indx_E1,[10 5])};
    [start_fixation,end_fixation]=get_start_end_data_section(data2plot,'Choose best section for fixation');
    E0_val_Pnormals=[mean(table2array(E0_data_raw(E0_data_raw.time>=start_fixation & E0_data_raw.time<=end_fixation,7))) ...
        mean(table2array(E0_data_raw(E0_data_raw.time>=start_fixation & E0_data_raw.time<=end_fixation,8))) ...
        mean(table2array(E0_data_raw(E0_data_raw.time>=start_fixation & E0_data_raw.time<=end_fixation,9)))];
    E0_GZ_normals=calibration1.mset(2).eye(1).Rmat*E0_val_Pnormals(:);
    E1_val_Pnormals=[mean(table2array(E1_data_raw(E1_data_raw.time>=start_fixation & E1_data_raw.time<=end_fixation,7))) ...
        mean(table2array(E1_data_raw(E1_data_raw.time>=start_fixation & E1_data_raw.time<=end_fixation,8))) ...
        mean(table2array(E1_data_raw(E1_data_raw.time>=start_fixation & E1_data_raw.time<=end_fixation,9)))];
    E1_GZ_normals=calibration1.mset(2).eye(2).Rmat*E1_val_Pnormals(:);
    ValidationMapDataSacc(p).Eye0PUPILGazeNormal=E0_GZ_normals(:);
    ValidationMapDataSacc(p).Eye1PUPILGazeNormal=E1_GZ_normals(:);
    ValidationMapDataSacc(p).Eye0PUPILNormal=E0_val_Pnormals(:);
    ValidationMapDataSacc(p).Eye1PUPILNormal=E1_val_Pnormals(:);
    start_time=end_fixation;
    p=p+1;
    ButtonName = questdlg('Keep going?', ...
        'Continue Question', ...
        'Yes', 'No');
    switch ButtonName
        case 'Yes'
            keep_going=1;
        case 'No'
            keep_going=0;
    end % switch
    if end_time-start_time < 0.1
        keep_going=0;
    end
end
close(ff);