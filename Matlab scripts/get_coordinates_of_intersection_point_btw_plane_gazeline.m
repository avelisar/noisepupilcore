function [intersect_pt_coord]=get_coordinates_of_intersection_point_btw_plane_gazeline(known_plane_point_coord,normal2plane_vector,point_of_origin_of_gazeline_coord,gaze_vector)
% monitor's plane equation
% plane eq: a*x+b*y+c*z+d=0 where d=-(a*x0+b*y0+c*z0) and (a,b,c) normal to
% the plane and P0(x0,y0,z0) a known point on the plane
% let marker 5 be point P0
x0=known_plane_point_coord(1);
y0=known_plane_point_coord(2);
z0=known_plane_point_coord(3);
a=normal2plane_vector(1);
b=normal2plane_vector(2);
c=normal2plane_vector(3);
d=-(a*x0+b*y0+c*z0);
ECx=point_of_origin_of_gazeline_coord(1);
ECy=point_of_origin_of_gazeline_coord(2);
ECz=point_of_origin_of_gazeline_coord(3);
GZx=gaze_vector(1);
GZy=gaze_vector(2);
GZz=gaze_vector(3);
% calculate point of PUPIL gaze on the monitor plane
% gaze line
% x= ECx+GZx*t ; % where t = -inf:inf 
% y= ECy+GZy*t ; 
% z= ECz+GZz*t ; 
% same point on the plane and on the gaze line leads to solving for t
t=-(a*ECx+b*ECy+c*ECz+d)./(a*GZx+b*GZy+c*GZz);

x= ECx+GZx*t ;
y= ECy+GZy*t ;
z= ECz+GZz*t ;

intersect_pt_coord=[x y z];
