function [all_stats,DataAgain]=get_pop_statsV2(DataTable,fig_title,fig_ylabel)


DataAgain=[];
all_stats=[];
if lillietest(DataTable.RefPoints2imarker(str2num(DataTable.markerID)==5))
    m5=median(DataTable.RefPoints2imarker(str2num(DataTable.markerID)==5));
else
    m5=mean(DataTable.RefPoints2imarker(str2num(DataTable.markerID)==5));
end
otherms=DataTable.RefPoints2imarker(str2num(DataTable.markerID)~=5);
all_ms=[m5; otherms(:)];
if lillietest(all_ms)
    qs=quantile(all_ms,[ 0.5 0.25 0.75]);
    mads = mad(all_ms,1);
    stats=[qs(1) qs(3)-qs(2) mads 0 qs(2) qs(3) 1];
else
    mads = mad(all_ms,1);
    cvs= std(all_ms)./mean(all_ms);
    stats=[mean(all_ms) std(all_ms) mads cvs 0 0 0];
end
all_stats=[all_stats; stats];
DataAgain=[ DataAgain all_ms(:)];


figure;
boxplot(DataAgain);
title(fig_title);
ylabel(fig_ylabel);