function [imarker_scenecam_vect,imarker_onScreen_pos,imarker_angs,imarker_scenecam_image]=calculate_imarker_metrics(mon_orient_in_scene_cam,marker5_inscenecam, valid_grid, markers_used, m_width, m_height, E0EC_inscenecam, E1EC_inscenecam, scenecameraParams)

    
N_markers=length([markers_used{:}]);
valid_grid_m=valid_grid([markers_used{:}],:);
valid_grid_m(:,2)=(valid_grid_m(:,2)-50)*m_width/100;
valid_grid_m(:,3)=(valid_grid_m(:,3)-50)*m_height/100;
monUvect_x_inscenecam=mon_orient_in_scene_cam(:,1);
monUvect_y_inscenecam=mon_orient_in_scene_cam(:,2);
monUvect_z_inscenecam=mon_orient_in_scene_cam(:,3);
imarker_scenecam_vect=repmat(marker5_inscenecam',N_markers,1)+valid_grid_m(:,2)*monUvect_x_inscenecam'+valid_grid_m(:,3)*monUvect_y_inscenecam';
imarker_onScreen_pos=imarker_scenecam_vect;
% homography refrences positions
imarker_E0_horiz_vis_angle=atand(((imarker_scenecam_vect-repmat(E0EC_inscenecam',N_markers,1))*monUvect_x_inscenecam)./((imarker_scenecam_vect-repmat(E0EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E0_vert_vis_angle=atand(((imarker_scenecam_vect-repmat(E0EC_inscenecam',N_markers,1))*monUvect_y_inscenecam)./((imarker_scenecam_vect-repmat(E0EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E1_horiz_vis_angle=atand(((imarker_scenecam_vect-repmat(E1EC_inscenecam',N_markers,1))*monUvect_x_inscenecam)./((imarker_scenecam_vect-repmat(E1EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));
imarker_E1_vert_vis_angle=atand(((imarker_scenecam_vect-repmat(E1EC_inscenecam',N_markers,1))*monUvect_y_inscenecam)./((imarker_scenecam_vect-repmat(E1EC_inscenecam',N_markers,1))*monUvect_z_inscenecam));

imarker_cyclop_horiz_vis_angle=atand(((imarker_scenecam_vect-repmat(((E1EC_inscenecam+E0EC_inscenecam)./2)',N_markers,1))*monUvect_x_inscenecam)./((imarker_scenecam_vect-repmat(((E1EC_inscenecam+E0EC_inscenecam)./2)',N_markers,1))*monUvect_z_inscenecam));
imarker_cyclop_vert_vis_angle=atand(((imarker_scenecam_vect-repmat(((E1EC_inscenecam+E0EC_inscenecam)./2)',N_markers,1))*monUvect_y_inscenecam)./((imarker_scenecam_vect-repmat(((E1EC_inscenecam+E0EC_inscenecam)./2)',N_markers,1))*monUvect_z_inscenecam));


imarker_angs=[valid_grid_m(:,1) imarker_E0_horiz_vis_angle(:) imarker_E0_vert_vis_angle(:) imarker_E1_horiz_vis_angle(:) imarker_E1_vert_vis_angle(:) imarker_cyclop_horiz_vis_angle(:) imarker_cyclop_vert_vis_angle(:)];
imarker_onScreen_pos=[valid_grid_m(:,1) imarker_onScreen_pos];
imarker_scenecam_vect=imarker_scenecam_vect./vecnorm(imarker_scenecam_vect,2,2);
imarker_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],imarker_scenecam_vect,'ApplyDistortion',true));
imarker_scenecam_vect=[valid_grid_m(:,1) imarker_scenecam_vect];

imarker_scenecam_image=[valid_grid_m(:,1) imarker_scenecam_image];