clear;
sbj_id='NMS';
condition='jumps';

path_in=['Z:\DATA\Pupil EyeCam motion test\preprocessed\' sbj_id '\' condition '\input_files\'];
path_out=['Z:\DATA\Pupil EyeCam motion test\preprocessed\' sbj_id '\' condition '\output_files\'];

ext='.csv';
file_pupil='pupil_positions';
file_IMU='imu_data';
file_notes='annotations';

IMURateHz=400;
synch_dir='y';

pupil_data = get_pupil_datav2([path_in file_pupil ext]);
IMU_data = get_IMU_datav2([path_in file_IMU ext]);
annotations = import_csv_annotations_file([path_in file_notes ext]);
pupil_time_zero=pupil_data.pupil_timestamp(1);
E03D=pupil_data(pupil_data.method=='pye3d 0.0.6 post-hoc' & pupil_data.eye_id==0 & pupil_data.confidence>0.70,:);
E13D=pupil_data(pupil_data.method=='pye3d 0.0.6 post-hoc' & pupil_data.eye_id==1 & pupil_data.confidence>0.70,:);

E0pupil_time=E03D.pupil_timestamp-pupil_time_zero;
E1pupil_time=E13D.pupil_timestamp-pupil_time_zero;

world_indx_synch=annotations.index(contains(string(annotations.label),'synch'));
% check if two IMU sensors
noIMUs=length(unique(IMU_data.SensorId));
sensor_IDs=unique(IMU_data.SensorId);

if noIMUs>1
    IMU_data_orig=IMU_data;
    IMU_datab=IMU_data_orig(IMU_data_orig.SensorId==sensor_IDs(1),:);
    IMU_data=IMU_data_orig(IMU_data_orig.SensorId==sensor_IDs(2),:);
    IMURateHz=1./mean(diff(IMU_data.TimeStamps));
    IMURateHzb=1./mean(diff(IMU_datab.TimeStamps));
else
    IMU_datab=IMU_data(IMU_data.SensorId==55,:);
    IMURateHzb=0;
end

for i=1:length(world_indx_synch)
    synch_markers_times(i) = E0pupil_time(find(E03D.world_index>=world_indx_synch(i),1,'first'));
end
[synch_IMU, synch_pupil]=synch_IMU_pupil_streamsV2(IMU_data,E03D,E13D,pupil_time_zero,synch_dir,IMURateHz,synch_markers_times);
imu_time=IMU_data.TimeStamps-synch_IMU+synch_pupil;
if ~isempty(IMU_datab)
    imub_time=IMU_datab.TimeStamps-synch_IMU+synch_pupil;
    IMU_datab_s=IMU_datab(imub_time>=0,:);
    IMU_datab_s.time=imub_time(imub_time>=0,:);
    
else
    IMU_datab_s=IMU_datab;
    IMU_datab_s.time=[];
    
end
% check synch
E0pupil=E03D(E0pupil_time>=0,:);
E0pupil.time=E0pupil_time(E0pupil_time>=0 ,:);
E1pupil=E13D(E1pupil_time>=0,:);
E1pupil.time=E1pupil_time(E1pupil_time>=0 ,:);
IMU_data_s=IMU_data(imu_time>=0,:);
IMU_data_s.time=imu_time(imu_time>=0,:);
% find walking sections
if ~strcmpi(condition,'jumps')
    world_indx_start=annotations.index(contains(string(annotations.label),'start move slow'));
    world_indx_end=annotations.index(contains(string(annotations.label),'end move slow'));
    for j=1:length(world_indx_start)
        platform_mov_time(j,:)=[ E0pupil.time(find(E0pupil.world_index>=world_indx_start(j),1,'first')) E0pupil.time(find(E0pupil.world_index<=world_indx_end(j),1,'last'))];
    end
else 
     world_indx_start=annotations.index(contains(string(annotations.label),'start move slow'));
    world_indx_end=annotations.index(contains(string(annotations.label),'end move slow'));
    
        platform_mov_time=[ E0pupil.time(find(E0pupil.world_index>=world_indx_start,1,'first')) E0pupil.time(find(E0pupil.world_index<=world_indx_end,1,'last'))];
    
end
figure;
if ~isempty(IMU_datab)
    sp(1)=subplot(3,1,1);
    plot(E0pupil.time,E0pupil.norm_pos_y,'.-')
    hold on;
    YL=ylim;
    for j=1:length(world_indx_start)
        line([platform_mov_time(j,1) platform_mov_time(j,1)],YL,'Color','m');
        line([platform_mov_time(j,2) platform_mov_time(j,2)],YL,'Color','m');
    end
    sp(2)=subplot(3,1,2);
    plot(IMU_data_s.time,IMU_data_s.LinAccYg,'.-')
    %plot(IMU_data_s.time,IMU_data_s.GyroZdegs,'.-')
    hold on;
    YL=ylim;
    for j=1:length(world_indx_start)
        line([platform_mov_time(j,1) platform_mov_time(j,1)],YL,'Color','m');
        line([platform_mov_time(j,2) platform_mov_time(j,2)],YL,'Color','m');
    end
    sp(3)=subplot(3,1,3);
    plot(IMU_datab_s.time,IMU_datab_s.LinAccYg,'.-')
    hold on;
    plot(IMU_datab_s.time,IMU_datab_s.GyroZdegs,'r.-')
   
    YL=ylim;
    for j=1:length(world_indx_start)
        line([platform_mov_time(j,1) platform_mov_time(j,1)],YL,'Color','m');
        line([platform_mov_time(j,2) platform_mov_time(j,2)],YL,'Color','m');
    end
    linkaxes(sp,'x')
else
    sp(1)=subplot(2,1,1);
    plot(E0pupil.time,E0pupil.norm_pos_y,'.-')
    hold on;
    YL=ylim;
    for j=1:length(world_indx_start)
        line([platform_mov_time(j,1) platform_mov_time(j,1)],YL,'Color','m');
        line([platform_mov_time(j,2) platform_mov_time(j,2)],YL,'Color','m');
    end
    sp(2)=subplot(2,1,2);
    plot(IMU_data_s.time,IMU_data_s.LinAccYg,'.-')
    %plot(IMU_data_s.time,IMU_data_s.GyroZdegs,'.-')
    hold on;
    YL=ylim;
    for j=1:length(world_indx_start)
        line([platform_mov_time(j,1) platform_mov_time(j,1)],YL,'Color','m');
        line([platform_mov_time(j,2) platform_mov_time(j,2)],YL,'Color','m');
    end
    linkaxes(sp,'x')
end

% pick up walking sections precisely
E0_data_raw=E0pupil(:,[ 1 2 4 5 6 14 24 25 26 35]);
E1_data_raw=E1pupil(:,[ 1 2 4 5 6 14 24 25 26 35]);
% get the most important IMU_data
IMU_data_raw=IMU_data_s(:,[7:12 17:20]);
[CleanMotionData,tsplit]=get_best_times_and_clean_data4each_motion_special(platform_mov_time,E0_data_raw, E1_data_raw, IMU_data_raw,condition);
%Acc_latPK2PK_series_all=[];
Acc_vertPK2PK_series_all=[];
E0normx_all=[];
E0normy_all=[];
E1normx_all=[];
E1normy_all=[];
t_space=0;
ff0=figure;
sp(5)=subplot(3,4,7);
sp(6)=subplot(3,4,8);
sp(1)=subplot(3,4,[1 5 9]);
sp(3)=subplot(3,4,3);
sp(4)=subplot(3,4,4);
sp(2)=subplot(3,4,[2 6 10]);
sp(7)=subplot(3,4,11);
sp(8)=subplot(3,4,12);

for st=1:length(CleanMotionData)

E0pupil_walk=CleanMotionData(st).E0_mov_data;
E0pupil_walk.time=E0pupil_walk.time-tsplit(st,1);
E1pupil_walk=CleanMotionData(st).E1_mov_data;
E1pupil_walk.time=E1pupil_walk.time-tsplit(st,1);
IMU_data_walk=CleanMotionData(st).IMU_mov_data;
IMU_data_walk.time=IMU_data_walk.time-tsplit(st,1);


% resample first
targetRateHz=1000;
padLengthSamples=100;
E0Matrix=[E0pupil_walk.time(:) E0pupil_walk.norm_pos_x(:) E0pupil_walk.norm_pos_y(:)];
start_time=0;
end_time=tsplit(st,2)-tsplit(st,1);
E0MatrixRs = ResampleMatrixAV(E0Matrix, targetRateHz, padLengthSamples,start_time, end_time);
E1Matrix=[E1pupil_walk.time(:) E1pupil_walk.norm_pos_x(:) E1pupil_walk.norm_pos_y(:)];
E1MatrixRs = ResampleMatrixAV(E1Matrix, targetRateHz, padLengthSamples,start_time, end_time);
IMUMatrix=[IMU_data_walk.time(:) IMU_data_walk.LinAccYg(:) IMU_data_walk.LinAccXg(:)];
IMUMatrixRs = ResampleMatrixAV(IMUMatrix, targetRateHz, padLengthSamples,start_time, end_time);
% filter for the imu
cut_off=25;%Hz
N=4;
Kct=9.81;
[b,a]=butter(N,cut_off/targetRateHz/2,'low');
E0_filtx=filtfilt(b,a,E0MatrixRs(:,2));
E0_filty=filtfilt(b,a,E0MatrixRs(:,3));
E0filt=[E0MatrixRs(:,1) E0_filtx(:) E0_filty(:)];
E1_filtx=filtfilt(b,a,E1MatrixRs(:,2));
E1_filty=filtfilt(b,a,E1MatrixRs(:,3));
E1filt=[E1MatrixRs(:,1) E1_filtx(:) E1_filty(:)];
IMU_filty=filtfilt(b,a,IMUMatrixRs(:,2));
IMU_filtz=filtfilt(b,a,IMUMatrixRs(:,3));
IMUfilt=[IMUMatrixRs(:,1) 9.81.*IMU_filty(:) 9.81.*IMU_filtz(:)];
% % calculate velocity and displacement
% IMUvel=cumsum(IMUfilt(:,2))./targetRateHz;
% trend_vel=movmean(IMUvel,2*targetRateHz);
% IMUvelD=IMUvel-trend_vel;
% IMUdisp=cumsum(IMUvelD)./targetRateHz;
% trend_disp=movmean(IMUdisp,2*targetRateHz);
% IMUdispD=IMUdisp-trend_disp;

% calculate the range of motion in the eye cameras
% detrand first
trend_E0y=movmean(E0filt(:,3),2*targetRateHz);
trend_E0x=movmean(E0filt(:,2),2*targetRateHz);
E0normx=E0filt(:,2)-trend_E0x;
E0normy=E0filt(:,3)-trend_E0y;
trend_E1y=movmean(E1filt(:,3),2*targetRateHz);
trend_E1x=movmean(E1filt(:,2),2*targetRateHz);
E1normx=E1filt(:,2)-trend_E1x;
E1normy=E1filt(:,3)-trend_E1y;
E0normx_orig=E0normx;
E0normy_orig=E0normy;
E1normx_orig=E1normx;
E1normy_orig=E1normy;
fun = @(x)sum(abs(E0normx_orig.*cos(x)-E0normy_orig.*sin(x)));
x0 = 0;
x = fminsearch(fun,x0);
E0normx=E0normx_orig.*cos(x)-E0normy_orig.*sin(x);
E0normy=E0normx_orig.*sin(x)+E0normy_orig.*cos(x);

% rotate E1 path to align the most motion on the y axis

fun = @(x)sum(abs(E1normx_orig.*cos(x)-E1normy_orig.*sin(x)));
x0 = 0;
x = fminsearch(fun,x0);
E1normx=E1normx_orig.*cos(x)-E1normy_orig.*sin(x);
E1normy=E1normx_orig.*sin(x)+E1normy_orig.*cos(x);






E0normx_all=[E0normx_all;E0normx];
E0normy_all=[E0normy_all;E0normy];
E1normx_all=[E1normx_all;E1normx];
E1normy_all=[E1normy_all;E1normy];

figure;
% left eye
subplot(3,2,1);
plot(E1pupil_walk.time,E1pupil_walk.norm_pos_y,'m.-')
hold on;
plot(E1filt(:,1),E1filt(:,3),'k.-')
axis tight
%ylim([0.496 0.501]);
title('Left Eye camera')
ylabel('y axis')
subplot(3,2,3);
plot(E1pupil_walk.time,E1pupil_walk.norm_pos_x,'m.-')
hold on;
plot(E1filt(:,1),E1filt(:,2),'k.-')
axis tight
%ylim([0.434 0.439]);
ylabel('x axis')
subplot(3,2,5);
plot(IMU_data_walk.time,9.81.*IMU_data_walk.LinAccYg,'m.-')
hold on;
plot(IMUfilt(:,1),IMUfilt(:,2),'k.-')
axis tight
ylabel('IMU vertical lin acc')
xlabel('Time')
YL=ylim;
subplot(3,2,2);
plot(E0pupil_walk.time,E0pupil_walk.norm_pos_y,'m.-')
hold on;
plot(E0filt(:,1),E0filt(:,3),'k.-')
axis tight
%ylim([0.306 0.311]);
title('Right Eye camera')

subplot(3,2,4);
plot(E0pupil_walk.time,E0pupil_walk.norm_pos_x,'m.-')
hold on;
plot(E0filt(:,1),E0filt(:,2),'k.-')
axis tight
%ylim([0.466 0.471]);

subplot(3,2,6);
plot(IMU_data_walk.time,9.81.*IMU_data_walk.LinAccXg,'m.-')
hold on;
plot(IMUfilt(:,1),IMUfilt(:,3),'k.-')
axis tight
ylim(YL);
xlabel('Time')
savefig([path_out sbj_id '_' condition '_cycle' num2str(st) '_data.fig']);

figure(ff0);
% left eye
subplot(sp(5));
plot(E1filt(:,1)+t_space,E1normy,'-');
hold on
axis tight
ylim([-0.065 0.065]);
ylabel('Left Eye camera')
title('y axis')
subplot(sp(6));
plot(E1filt(:,1)+t_space,E1normx,'-');
hold on
axis tight
ylim([-0.065 0.065]);
title('x axis')
xlabel('Time')
subplot(sp(1));
plot(E1normx,E1normy,'k.');
hold on
axis tight
ylim([-0.065 0.065]);
xlim([-0.065 0.065]);
ylabel('y axis')
xlabel('x axis')
title('Left Eye camera');
set(gca,'DataAspectRatio',[1 1 1])
subplot(sp(3));
plot(E0filt(:,1)+t_space,E0normy,'-');
hold on
axis tight
ylim([-0.065 0.065]);
ylabel('Right Eye camera')
title('y axis')
subplot(sp(4));
plot(E0filt(:,1)+t_space,E0normx,'-');
hold on
axis tight
ylim([-0.065 0.065]);

title('x axis')
subplot(sp(2));
plot(E0normx,E0normy,'k.');
hold on
axis tight
ylim([-0.065 0.065]);
xlim([-0.065 0.065]);
xlabel('x axis')
title('Right Eye camera')
set(gca,'DataAspectRatio',[1 1 1])

subplot(sp(7));
plot(IMUfilt(:,1)+t_space,IMUfilt(:,2),'-');
hold on
axis tight
ylim([-15 15]);
ylabel('IMU linear motion')
title('Acc vert')
xlabel('Time')
subplot(sp(8));
plot(IMUfilt(:,1)+t_space,IMUfilt(:,3),'-');
hold on
axis tight
ylim([-15 15]);
title('Acc lat')
xlabel('Time')


%Imu_time_to_draw=[Imu_time_to_draw t_space+IMUfilt(:,1)];
t_space=IMUfilt(end,1)+t_space+2;
[Acc_vertPK2PK_series]=get_peak2peak_amplitude(IMUfilt(:,1),IMUfilt(:,2),4);
Acc_vertPK2PK_series_all=[Acc_vertPK2PK_series_all ;Acc_vertPK2PK_series];
% [Acc_latPK2PK_series]=get_peak2peak_amplitude(IMUfilt(:,1),IMUfilt(:,3),2);
% Acc_latPK2PK_series_all=[Acc_latPK2PK_series_all ;Acc_latPK2PK_series];
end
subplot(sp(7));
YL=ylim;
subplot(sp(8));
ylim(YL);
savefig(ff0,[path_out sbj_id '_' condition '_all_cycles_data.fig']);
close(ff0);
clear ff0


E0maxrange=range([E0normx_all,E0normy_all])
E1maxrange=range([E1normx_all,E1normy_all])
Accpk2pk_average=mean(Acc_vertPK2PK_series_all(:,1))
save([path_out sbj_id '_' condition '_all_cycles_data.mat'])
