function [ValidationMapData]=get_avg_pupil_norm_vect4each_markerV1(period_time, markers_used, annotations, indx_start_end_period,E0_data_raw, E1_data_raw, IMU_data_raw)


E0_data_validation=E0_data_raw(E0_data_raw.time>=period_time(1) & E0_data_raw.time<=period_time(2),:);
E1_data_validation=E1_data_raw(E1_data_raw.time>=period_time(1) & E1_data_raw.time<=period_time(2),:);

% find approx marker times from annotations
markers_times(:,1)=markers_used;
ann_period=annotations(annotations.index>=indx_start_end_period(1) & annotations.index<=indx_start_end_period(2),:);
indx_start_m=ann_period.index(contains(string(ann_period.label),'start marker'));
indx_end_m=ann_period.index(contains(string(ann_period.label),'end marker'));
%indx_start_m(1)=E0_data_validation.world_index(1);
for i=1:length(markers_times(:,1))
    markers_times{i,2}=E0_data_raw.time(find(E0_data_raw.world_index>=indx_start_m(i),1,'first'));
    markers_times{i,3}=E0_data_raw.time(find(E0_data_raw.world_index<=indx_end_m(i)-1,1,'last'));
end

% check validation times
ff=figure;
sp(1)=subplot(2,1,1);
plot(E0_data_validation.time,E0_data_validation.norm_pos_x,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_x,'.-')
YL=ylim;
for i=1:length(markers_times(:,1))
    line([markers_times{i,2} markers_times{i,2}],YL,'Color','r');
    line([markers_times{i,3} markers_times{i,3}],YL,'Color','g');
end

sp(2)=subplot(2,1,2);
plot(E0_data_validation.time,E0_data_validation.norm_pos_y,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_y,'.-')

YL=ylim;
for i=1:length(markers_times(:,1))
    line([markers_times{i,2} markers_times{i,2}],YL,'Color','r');
    line([markers_times{i,3} markers_times{i,3}],YL,'Color','g');
    
end
linkaxes(sp,'x')


% clean the validation data
data2clean=table2array(E0_data_validation(:,[7 8 9]));
time_before=table2array(E0_data_validation(:,10));
[time_after,~,~]=eliminate_outliers_eye_data_mult_w_brush(time_before,data2clean);
[~,indx_time_before_cl,~] = intersect(time_before,time_after);
E0_data_validation_cleaned=E0_data_validation(indx_time_before_cl,:);
clear data2clean time_before time_after indx_time_before_cl
data2clean=table2array(E1_data_validation(:,[7 8 9]));
time_before=table2array(E1_data_validation(:,10));
[time_after,~,~]=eliminate_outliers_eye_data_mult_w_brush(time_before,data2clean);
[~,indx_time_before_cl,~] = intersect(time_before,time_after);
E1_data_validation_cleaned=E1_data_validation(indx_time_before_cl,:);
clear data2clean time_before time_after indx_time_before_cl
% choose the best fixation period during each marker presentation and
% calculate the mean gaze vectors
for i=1:length(markers_times(:,1))
    if i==1
        indx_imu=IMU_data_raw.time>=markers_times{i,2} & IMU_data_raw.time<=markers_times{i+1,2};
        indx_E0=E0_data_validation_cleaned.time>=markers_times{i,2} & E0_data_validation_cleaned.time<=markers_times{i+1,2};
        indx_E1=E1_data_validation_cleaned.time>=markers_times{i,2} & E1_data_validation_cleaned.time<=markers_times{i+1,2};
    elseif i== length(markers_times(:,1))
        indx_imu=IMU_data_raw.time>=markers_times{i-1,3} & IMU_data_raw.time<=markers_times{i,3};
        indx_E0=E0_data_validation_cleaned.time>=markers_times{i-1,3} & E0_data_validation_cleaned.time<=markers_times{i,3};
        indx_E1=E1_data_validation_cleaned.time>=markers_times{i-1,3} & E1_data_validation_cleaned.time<=markers_times{i,3};
    else
        indx_imu=IMU_data_raw.time>=markers_times{i-1,3} & IMU_data_raw.time<=markers_times{i+1,2};
        indx_E0=E0_data_validation_cleaned.time>=markers_times{i-1,3} & E0_data_validation_cleaned.time<=markers_times{i+1,2};
        indx_E1=E1_data_validation_cleaned.time>=markers_times{i-1,3} & E1_data_validation_cleaned.time<=markers_times{i+1,2};
    end
    data2plot={IMU_data_raw(indx_imu,[10 1]), E0_data_validation_cleaned(indx_E0,[10 4]), E0_data_validation_cleaned(indx_E0,[10 5]), E1_data_validation_cleaned(indx_E1,[10 4]), E1_data_validation_cleaned(indx_E1,[10 5])};
    [start_fixation,end_fixation]=get_start_end_data_section(data2plot,'Choose fixation');
    ValidationMapData(i).fixation_times=[start_fixation,end_fixation];
    ValidationMapData(i).E0_fix_data=E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation,:);
    ValidationMapData(i).E1_fix_data=E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation,:);
    E0_val_Pnormals_all=[mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation,7))) ...
        mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation,8))) ...
        mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation,9)))];
    E1_val_Pnormals_all=[mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation,7))) ...
        mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation,8))) ...
        mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation,9)))];
    ValidationMapData(i).Eye0PUPILNormal_all=E0_val_Pnormals_all(:);
    ValidationMapData(i).Eye1PUPILNormal_all=E1_val_Pnormals_all(:);
    clear indx_imu indx_E0 indx_E1 data2plot
    indx_imu=IMU_data_raw.time>=start_fixation & IMU_data_raw.time<=end_fixation;
    indx_E0=E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation;
    indx_E1=E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation;
    data2plot={IMU_data_raw(indx_imu,[10 1]), E0_data_validation_cleaned(indx_E0,[10 4]), E0_data_validation_cleaned(indx_E0,[10 5]), E1_data_validation_cleaned(indx_E1,[10 4]), E1_data_validation_cleaned(indx_E1,[10 5])};
    [start_ch_fixation,end_ch_fixation]=get_start_end_data_section(data2plot,'Choose best section of fixation');
    ValidationMapData(i).bestfixation_times=[start_ch_fixation,end_ch_fixation];
    ValidationMapData(i).E0_bfix_data=E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_ch_fixation & E0_data_validation_cleaned.time<=end_ch_fixation,:);
    ValidationMapData(i).E1_bfix_data=E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_ch_fixation & E1_data_validation_cleaned.time<=end_ch_fixation,:);
    E0_val_Pnormals=[mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_ch_fixation & E0_data_validation_cleaned.time<=end_ch_fixation,7))) ...
        mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_ch_fixation & E0_data_validation_cleaned.time<=end_ch_fixation,8))) ...
        mean(table2array(E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_ch_fixation & E0_data_validation_cleaned.time<=end_ch_fixation,9)))];
    E1_val_Pnormals=[mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_ch_fixation & E1_data_validation_cleaned.time<=end_ch_fixation,7))) ...
        mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_ch_fixation & E1_data_validation_cleaned.time<=end_ch_fixation,8))) ...
        mean(table2array(E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_ch_fixation & E1_data_validation_cleaned.time<=end_ch_fixation,9)))];
    ValidationMapData(i).markerID=markers_times{i,1};
    ValidationMapData(i).Eye0PUPILNormal=E0_val_Pnormals(:);
    ValidationMapData(i).Eye1PUPILNormal=E1_val_Pnormals(:);
end
close(ff)