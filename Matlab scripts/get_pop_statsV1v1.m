function [all_stats,DataAgain]=get_pop_statsV1v1(DataTable,fig_title,fig_ylabel)


DataAgain=[];
all_stats=[];

for cal=1:(width(DataTable)-1)
    
    if lillietest(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1)))
        m5=median(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1)));
    else
        m5=mean(table2array(DataTable(str2num(table2array(DataTable(:,1)))==5,cal+1)));
    end
    otherms=(table2array(DataTable(str2num(table2array(DataTable(:,1)))~=5,cal+1)));
    all_ms=[m5; otherms(:)];
    if lillietest(all_ms)
        qs=quantile(all_ms,[ 0.5 0.25 0.75]);
        mads = mad(all_ms,1);
        stats=[qs(1) qs(3)-qs(2) mads 0 qs(2) qs(3) 1];
    else
        mads = mad(all_ms,1);
        cvs= std(all_ms)./mean(all_ms);
        stats=[mean(all_ms) std(all_ms) mads cvs 0 0 0];
    end
    all_stats=[all_stats; stats];
    DataAgain=[ DataAgain all_ms(:)];
end

figure;
bp=boxplot(DataAgain);
title(fig_title);
ylabel(fig_ylabel);
set(gca,'XTickLabel',DataTable.Properties.VariableNames(2:end));