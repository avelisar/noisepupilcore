function angle=angle_btw_2_vectors(v1,v2)

mag_v1=sqrt(sum(v1.^2));
mag_v2=sqrt(sum(v2.^2));

angle=acosd(dot(v1(:),v2(:))./(mag_v1.*mag_v2));
%determine sign
if length(v1)==3
    angle1=asind(cross(v1(:),v2(:))./(mag_v1.*mag_v2));
elseif length(v1)==2
     angle1=asind(cross([v1(:);0],[v2(:);0])./(mag_v1.*mag_v2));
end
if sign(angle1(3))==-1  %  positive is clockwise
    angle=-angle;
end

