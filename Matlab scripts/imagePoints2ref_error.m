
function [output]=imagePoints2ref_error(x,scenecameraParams,worldPoints,refPoints)
theta=x(1);
beta=x(2);
alpha=x(3);
tx=x(4);
ty=x(5);
tz=x(6);
Rz=[cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];
Rx=[1 0 0; 0 cosd(alpha) -sind(alpha); 0 sind(alpha) cosd(alpha)];
Ry=[cosd(beta) 0  sind(beta);0 1 0; -sind(beta) 0 cosd(beta)];
rot_mat=Rz*Ry*Rx;
imagePoints = round(worldToImage(scenecameraParams,rot_mat,[tx ty tz],worldPoints,'ApplyDistortion',true));

output=sum(sqrt(sum((imagePoints-refPoints).^2,2)),1);