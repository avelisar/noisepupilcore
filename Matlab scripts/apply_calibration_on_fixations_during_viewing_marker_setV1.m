function [calibrated_marker_set_gazes]=apply_calibration_on_fixations_during_viewing_marker_setV1(marker_set_pupils, calibration)
% calculates the cyclop gaze too
if isfield(marker_set_pupils,'markerID')
    for i=1:length(marker_set_pupils)
        calibrated_marker_set_gazes(i).markerID=marker_set_pupils(i).markerID;
        calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal=calibration.mset(2).eye(1).Rmat*marker_set_pupils(i).Eye0PUPILNormal;
        calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal=calibration.mset(2).eye(2).Rmat*marker_set_pupils(i).Eye1PUPILNormal;
        calibrated_marker_set_gazes(i).cyclopGazeNormal=(calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal+calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal)./norm(calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal+calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal);
    end
else
    for i=1:length(marker_set_pupils)
        calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal=calibration.mset(2).eye(1).Rmat*marker_set_pupils(i).Eye0PUPILNormal;
        calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal=calibration.mset(2).eye(2).Rmat*marker_set_pupils(i).Eye1PUPILNormal;
        calibrated_marker_set_gazes(i).cyclopGazeNormal=(calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal+calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal)./norm(calibrated_marker_set_gazes(i).Eye0PUPILGazeNormal+calibrated_marker_set_gazes(i).Eye1PUPILGazeNormal);
    end
end