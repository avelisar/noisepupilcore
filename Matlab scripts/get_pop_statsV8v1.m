function [all_stats,DataAgain]=get_pop_statsV8v1(DataTable,fig_title,fig_ylabel)


DataAgain=[];
all_stats=[];
for cal=1:(width(DataTable)-1)
    vect=table2array(DataTable(:,cal+1));

if lillietest(vect)
    qs1=quantile(vect,[ 0.5 0.25 0.75]);
    m1=qs1(1);
    sm1=qs1(3)-qs1(2);
    mads = mad(vect,1);
    cvs=0;
    q1m1=qs1(2);
    q2m1=qs1(3);
    flag=1;
else
    m1=mean(vect);
    sm1=std(vect);
    mads = mad(vect,0);
    cvs=sm1./m1;
    q1m1=0;
    q2m1=0;
    flag=0;
end
stats=[m1 sm1 mads cvs q1m1 q2m1 flag];
DataAgain=[DataAgain vect(:)];
all_stats=[all_stats; stats];
end


figure;
boxplot(DataAgain);
title(fig_title);
ylabel(fig_ylabel);