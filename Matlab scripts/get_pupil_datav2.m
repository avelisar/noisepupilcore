function data=get_pupil_datav2(filename)
% get calibration structure

if nargin==0
% get file 
[file,path] = uigetfile('*.csv');
f = filesep;
data = import_csv_pupil_positions_filev2([path f file]);
elseif nargin ==1
    data = import_csv_pupil_positions_filev2(filename);
end