function [synch_IMU, synch_pupil]=synch_IMU_pupil_streamsV2(IMU_data,pupil_data,pupil_data1,pupil_time_zero,synch_dir,IMURateHz,synch_markers_times)

%integrate the IMU_data
if strcmpi(synch_dir,'x')
    trend_vel=movmean(IMU_data.GyroYdegs,2*IMURateHz);
    vel=IMU_data.GyroYdegs-trend_vel;
    pos=cumsum(vel)./IMURateHz;
    trend_pos=movmean(pos,2*IMURateHz);
    pos=pos-trend_pos;
elseif strcmpi(synch_dir,'y')
    trend_acc=movmean(IMU_data.LinAccYg,2*IMURateHz);
    acc=IMU_data.LinAccYg-trend_acc;
    vel=cumsum(acc)./IMURateHz;
    trend_vel=movmean(vel,2*IMURateHz);
    vel=vel-trend_vel;
    pos=cumsum(vel)./IMURateHz;
    trend_pos=movmean(pos,2*IMURateHz);
    pos=pos-trend_pos;
end
pause('on');
ff1=figure;
plot(IMU_data.TimeStamps, IMU_data.LinAccYg,'.-')

title({'Start by pressing Return once','CHOOSE peak of a synch movement','then press Return.'});
pause

dcm_obj = datacursormode(ff1);
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','on','Enable','on')
% Wait while the user does this.
pause
c_info = getCursorInfo(dcm_obj);

    synch_IMU=c_info.Position(1);

close(ff1);
ff2=figure;
if strcmpi(synch_dir,'x')
    plot(pupil_data.pupil_timestamp-pupil_time_zero,pupil_data.norm_pos_x,'r.-')
    hold on;
    plot(pupil_data1.pupil_timestamp-pupil_time_zero,pupil_data1.norm_pos_x,'m.-')
    if ~isempty(synch_markers_times)
        for i=1:length(synch_markers_times)
            line([synch_markers_times(i) synch_markers_times(i)],[0 1])
        end
    end
elseif strcmpi(synch_dir,'y')
    plot(pupil_data.pupil_timestamp-pupil_time_zero,pupil_data.norm_pos_y,'r.-')
    hold on;
    plot(pupil_data1.pupil_timestamp-pupil_time_zero,pupil_data1.norm_pos_y,'m.-')
    if ~isempty(synch_markers_times)
        for i=1:length(synch_markers_times)
            line([synch_markers_times(i) synch_markers_times(i)],[0 1])
        end
    end
end
title({'Start by pressing Return once','CHOOSE peak of a synch movement','then press Return.'});

pause

dcm_obj = datacursormode(ff2);
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','on','Enable','on')
% Wait while the user does this.
pause
c_info = getCursorInfo(dcm_obj);
    synch_pupil=c_info.Position(1);
close(ff2);
pause('off');

end
