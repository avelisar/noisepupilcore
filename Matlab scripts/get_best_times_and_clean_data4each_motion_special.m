function  [CleanMotionData,tsplit]=get_best_times_and_clean_data4each_motion_special(period_time, E0_data_raw, E1_data_raw, IMU_data_raw,motion_types)

E0_data_validation=E0_data_raw(E0_data_raw.time>=period_time(1,1) & E0_data_raw.time<=period_time(end,2),:);
E1_data_validation=E1_data_raw(E1_data_raw.time>=period_time(1,1) & E1_data_raw.time<=period_time(end,2),:);
IMU_data_validation=IMU_data_raw(IMU_data_raw.time>=period_time(1,1) & IMU_data_raw.time<=period_time(end,2),:);
tsplit=period_time;

ff=figure;
sp(1)=subplot(3,1,1);
plot(E0_data_validation.time,E0_data_validation.norm_pos_x,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_x,'.-')
YL=ylim;
for i=1:length(period_time(:,1))
    line([period_time(i,1) period_time(i,1)],YL,'Color','r');
    line([period_time(i,2) period_time(i,2)],YL,'Color','g');
end

sp(2)=subplot(3,1,2);
plot(E0_data_validation.time,E0_data_validation.norm_pos_y,'.-')
hold on;
plot(E1_data_validation.time,E1_data_validation.norm_pos_y,'.-')

YL=ylim;
for i=1:length(period_time(:,1))
    line([period_time(i,1) period_time(i,1)],YL,'Color','r');
    line([period_time(i,2) period_time(i,2)],YL,'Color','g');
end
sp(3)=subplot(3,1,3);
plot(IMU_data_validation.time,IMU_data_validation.LinAccYg,'.-')

hold on;
YL=ylim;
for i=1:length(period_time(:,1))
    line([period_time(i,1) period_time(i,1)],YL,'Color','r');
    line([period_time(i,2) period_time(i,2)],YL,'Color','g');
end

linkaxes(sp,'x')

% clean the validation data
data2clean=table2array(E0_data_validation(:,[7 8 9]));
time_before=table2array(E0_data_validation(:,10));
[time_after,~,~]=eliminate_outliers_eye_data_mult_w_brush(time_before,data2clean);
[~,indx_time_before_cl,~] = intersect(time_before,time_after);
E0_data_validation_cleaned=E0_data_validation(indx_time_before_cl,:);
clear data2clean time_before time_after indx_time_before_cl
data2clean=table2array(E1_data_validation(:,[7 8 9]));
time_before=table2array(E1_data_validation(:,10));
[time_after,~,~]=eliminate_outliers_eye_data_mult_w_brush(time_before,data2clean);
[~,indx_time_before_cl,~] = intersect(time_before,time_after);
E1_data_validation_cleaned=E1_data_validation(indx_time_before_cl,:);
clear data2clean time_before time_after indx_time_before_cl
% choose the best fixation period during each marker presentation and
% calculate the mean gaze vectors
for i=1:length(period_time(:,1))
    if i==1
        if length(period_time(:,1))==1
            indx_imu=IMU_data_raw.time>=period_time(i,1);
        indx_E0=E0_data_validation_cleaned.time>=period_time(i,1);
        indx_E1=E1_data_validation_cleaned.time>=period_time(i,1);
        else
        indx_imu=IMU_data_raw.time>=period_time(i,1) & IMU_data_raw.time<=period_time(i+1,1);
        indx_E0=E0_data_validation_cleaned.time>=period_time(i,1)& E0_data_validation_cleaned.time<=period_time(i+1,1);
        indx_E1=E1_data_validation_cleaned.time>=period_time(i,1) & E1_data_validation_cleaned.time<=period_time(i+1,1);
        end
    elseif i== length(period_time(:,1)) && length(period_time(:,1))>1
        indx_imu=IMU_data_raw.time>=period_time(i-1,2) & IMU_data_raw.time<=period_time(i,2);
        indx_E0=E0_data_validation_cleaned.time>=period_time(i-1,2) & E0_data_validation_cleaned.time<=period_time(i,2);
        indx_E1=E1_data_validation_cleaned.time>=period_time(i-1,2) & E1_data_validation_cleaned.time<=period_time(i,2);
    else
        indx_imu=IMU_data_raw.time>=period_time(i-1,2) & IMU_data_raw.time<=period_time(i+1,1);
        indx_E0=E0_data_validation_cleaned.time>=period_time(i-1,2) & E0_data_validation_cleaned.time<=period_time(i+1,1);
        indx_E1=E1_data_validation_cleaned.time>=period_time(i-1,2) & E1_data_validation_cleaned.time<=period_time(i+1,1);
    end
    data2plot={IMU_data_raw(indx_imu,[10 8]),IMU_data_raw(indx_imu,[10 7]), E0_data_validation_cleaned(indx_E0,[10 4]), E0_data_validation_cleaned(indx_E0,[10 5]), E1_data_validation_cleaned(indx_E1,[10 4]), E1_data_validation_cleaned(indx_E1,[10 5])};
    [start_fixation,end_fixation]=get_start_end_data_section(data2plot,'Choose motion section');
    CleanMotionData(i).motion_times=[start_fixation,end_fixation];
    CleanMotionData(i).E0_mov_data=E0_data_validation_cleaned(E0_data_validation_cleaned.time>=start_fixation & E0_data_validation_cleaned.time<=end_fixation,:);
    CleanMotionData(i).E1_mov_data=E1_data_validation_cleaned(E1_data_validation_cleaned.time>=start_fixation & E1_data_validation_cleaned.time<=end_fixation,:);
    CleanMotionData(i).IMU_mov_data=IMU_data_raw(IMU_data_raw.time>=start_fixation & IMU_data_raw.time<=end_fixation,:);
%     prompt={'Enter type of motion during this session:'};
%     name='Input for motion type';
%     numlines=1;
%     defaultanswer=motion_types(i);
%     options.Resize='on';
%     options.WindowStyle='normal';
%     options.Interpreter='tex';
%     
%     CleanMotionData(i).which_mov=inputdlg(prompt,name,numlines,defaultanswer,options);
    CleanMotionData(i).which_mov=motion_types;
    tsplit(i,:)=[start_fixation,end_fixation];
end

