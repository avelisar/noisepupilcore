function [gazesetB,error_dist_scenecamIMG2D,ErrorAngscenecam3D,error_dist_onScreen2D,error_view_ang,scaleAng,mon_orient_inscenecam3D_pupilsetB,...
    marker5_inscenecam3D_pupilsetB,error_GZP,imarkersetB,RefPointssetB]=evaluate_errors_for_calibrationA_on_markersetB(calibrationA,...
    pupilsetB,added_extrinsics,valid_grid,m_width,m_height,markers_used,...
    old_mon_orient_inscenecam3D_pupilsetB,old_marker5_inscenecam3D_pupilsetB,...
    E0EC_inscenecam_calibrationA,E1EC_inscenecam_calibrationA, scenecameraParams,set_name,close_fig)

% get gaze by applying calibration on P normals
[gazesetB]=...
    ...
    apply_calibration_on_fixations_during_viewing_marker_setV1(...
    ...
    pupilsetB, calibrationA);

% correct the extrinsics
[mon_orient_inscenecam3D_pupilsetB,...
    marker5_inscenecam3D_pupilsetB,...
    ~]=...
    ...
    adjust_IMU_extrinsics(...
    ...
    added_extrinsics,...
    old_mon_orient_inscenecam3D_pupilsetB,...
    old_marker5_inscenecam3D_pupilsetB,...
    valid_grid,...
    markers_used,...
    m_width,...
    m_height,...
    E0EC_inscenecam_calibrationA,...
    E1EC_inscenecam_calibrationA);

% calculate all imarker metrics 

[imarker_vect_inscenecam3D_pupilsetB,...
    imarker_pos_onScreen_inscenecam3D_pupilsetB,...
    imarker_angs_pupilsetB,...
    imarker_pos_scenecamIMG2D_pupilsetB]=...
    ...
    calculate_imarker_metrics(...
    ...
    mon_orient_inscenecam3D_pupilsetB,...
    marker5_inscenecam3D_pupilsetB,...
    valid_grid,...
    markers_used,...
    m_width,...
    m_height,...
    E0EC_inscenecam_calibrationA,...
    E1EC_inscenecam_calibrationA,...
    scenecameraParams);
for i=1:length(markers_used)
    imarkersetB(i).markerID=markers_used(i);
    imarkersetB(i).pos_onScreen=imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end);
    imarkersetB(i).angs=imarker_angs_pupilsetB(i,2:end);
    imarkersetB(i).pos_scenecamIMG2D=imarker_pos_scenecamIMG2D_pupilsetB(i,2:end);
end
cyclop_point=(E0EC_inscenecam_calibrationA+E1EC_inscenecam_calibrationA)./2;
% calculate Gaze points of all rays on Screen in scene camera
for i=1:length(markers_used)
    gazesetB(i).E0GZPonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),E0EC_inscenecam_calibrationA,gazesetB(i).Eye0PUPILGazeNormal);
    gazesetB(i).E1GZPonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),E1EC_inscenecam_calibrationA,gazesetB(i).Eye1PUPILGazeNormal);
    gazesetB(i).cyGZPonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),cyclop_point,gazesetB(i).cyclopGazeNormal);
end


% calculate gaze point in 3d
eye_line_vect=(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA)./norm(E0EC_inscenecam_calibrationA-E1EC_inscenecam_calibrationA);
for i=1:length(markers_used)
%near intersection
vect1=gazesetB(i).Eye0PUPILGazeNormal;
vect2=gazesetB(i).Eye1PUPILGazeNormal;
% nearest intersection
[GZPpupil,GZPpupil0,GZPpupil1,shortest_distanceGPs]=nearestIntersectionOF2vectors(vect1, vect2, E0EC_inscenecam_calibrationA, E1EC_inscenecam_calibrationA);
% projection on cyclopean gaze plane
cyclop_ray=(vect1+vect2)./2;
%define a viewing plane.
gaze_plane = cross(cyclop_ray, eye_line_vect);
gaze_plane = gaze_plane./ norm(gaze_plane);
% project lines of sight onto the gaze plane
vect1_on_plane = vect1 - dot(gaze_plane, vect1).* gaze_plane;
vect2_on_plane = vect2 - dot(gaze_plane, vect2).* gaze_plane;
%find the intersection of left and right line of sight.
[GZPpupilV2,nearestP0,nearestP1,shortest_dist]=nearestIntersectionOF2vectors(vect1_on_plane, vect2_on_plane, E0EC_inscenecam_calibrationA, E1EC_inscenecam_calibrationA);

% lines starting points
PA=[E0EC_inscenecam_calibrationA(:)'; E1EC_inscenecam_calibrationA(:)'];
% end points
PB=[E0EC_inscenecam_calibrationA(:)'+vect1(:)'; E1EC_inscenecam_calibrationA(:)'+vect2(:)'];
[GZPpupilV3,distances] = lineIntersect3D(PA,PB);

% lines starting points
PA=[E0EC_inscenecam_calibrationA(:)'; E1EC_inscenecam_calibrationA(:)'];
% end points
PB=[E0EC_inscenecam_calibrationA(:)'+vect1_on_plane(:)'; E1EC_inscenecam_calibrationA(:)'+vect2_on_plane(:)'];
[GZPpupilV4,~] = lineIntersect3D(PA,PB);

ray2GZPpupil=(GZPpupil-cyclop_point(:)')./norm(GZPpupil-cyclop_point(:)');
vect_long=norm(GZPpupil-cyclop_point(:)');
%distance to the eyeline
GZPdist2eyeline=norm(GZPpupil-dot(GZPpupil,eye_line_vect)*eye_line_vect');
gazesetB(i).GZPpupil0=GZPpupil0(:);
gazesetB(i).GZPpupil1=GZPpupil1(:);
gazesetB(i).shortest_distanceGPs=shortest_distanceGPs;
gazesetB(i).GZPpupil=GZPpupil(:);
gazesetB(i).GZPpupilV2=GZPpupilV2(:);
gazesetB(i).GZPpupilV3=GZPpupilV3(:);
gazesetB(i).GZPpupilV4=GZPpupilV4(:);
gazesetB(i).ray2GZPpupil=ray2GZPpupil(:);
gazesetB(i).GZPdist2cyclop_point=vect_long;
gazesetB(i).GZPdist2eyeline=GZPdist2eyeline;
% projectGZP on the scene camera image
GZPpupil_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil,'ApplyDistortion',true));
GZPpupil0_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil0','ApplyDistortion',true));
GZPpupil1_scenecam_image=round(worldToImage(scenecameraParams,eye(3),[0 0 0],GZPpupil1','ApplyDistortion',true));
gazesetB(i).GZPpupil_scenecam_image=GZPpupil_scenecam_image(:);
gazesetB(i).GZPpupil0_scenecam_image=GZPpupil0_scenecam_image(:);
gazesetB(i).GZPpupil1_scenecam_image=GZPpupil1_scenecam_image(:);
gazesetB(i).vergence=abs(angle_btw_2_vectors(vect1,vect2));
end

% calculate ref points vect and refPoints on Screen
refPoints_pos_scenecamIMG2D_pupilsetB=reshape([pupilsetB(:).refs],2,length(pupilsetB))';
undistortedRefs=undistortPoints(refPoints_pos_scenecamIMG2D_pupilsetB,scenecameraParams);
worldRefs = pointsToWorld(scenecameraParams,eye(3),[0 0 1],undistortedRefs);

refPoints_vect_inscenecam3D_pupilsetB=[worldRefs ones(length(worldRefs),1)]./sqrt(sum([worldRefs ones(length(worldRefs),1)].^2,2));
refPoints_pos_onScreen_inscenecam3D_pupilsetB=zeros(size(refPoints_vect_inscenecam3D_pupilsetB));
for i=1:size(refPoints_vect_inscenecam3D_pupilsetB,1)
    refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:)=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),[0 0 0],refPoints_vect_inscenecam3D_pupilsetB(i,:));
end

norm2monitor_inscenecam=mon_orient_inscenecam3D_pupilsetB(:,3);
monUvect_x_inscenecam=mon_orient_inscenecam3D_pupilsetB(:,1);
monUvect_y_inscenecam=mon_orient_inscenecam3D_pupilsetB(:,2);
N_markers=length(pupilsetB);
marker_IDvect=[pupilsetB.markerID]';

refPoints_E0_horiz_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*monUvect_x_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
refPoints_E0_vert_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*monUvect_y_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
refPoints_E1_horiz_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*monUvect_x_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
refPoints_E1_vert_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*monUvect_y_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
refPoints_cyclop_horiz_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(((E1EC_inscenecam_calibrationA+E0EC_inscenecam_calibrationA)./2)',N_markers,1))*monUvect_x_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(((E1EC_inscenecam_calibrationA+E0EC_inscenecam_calibrationA)./2)',N_markers,1))*norm2monitor_inscenecam));
refPoints_cyclop_vert_vis_angle=atand(((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(((E1EC_inscenecam_calibrationA+E0EC_inscenecam_calibrationA)./2)',N_markers,1))*monUvect_y_inscenecam)./((refPoints_pos_onScreen_inscenecam3D_pupilsetB-repmat(((E1EC_inscenecam_calibrationA+E0EC_inscenecam_calibrationA)./2)',N_markers,1))*norm2monitor_inscenecam));
refPoints_angs_pupilsetB=[marker_IDvect refPoints_E0_horiz_vis_angle(:) refPoints_E0_vert_vis_angle(:) refPoints_E1_horiz_vis_angle(:) refPoints_E1_vert_vis_angle(:) refPoints_cyclop_horiz_vis_angle(:) refPoints_cyclop_vert_vis_angle(:)];

for i=1:length(markers_used)
    RefPointssetB(i).markerID=markers_used(i);
    RefPointssetB(i).pos_onScreen=refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:);
    RefPointssetB(i).angs=refPoints_angs_pupilsetB(:,2:end);
    RefPointssetB(i).pos_scenecamIMG2D=refPoints_pos_scenecamIMG2D_pupilsetB(i,:);
end

% calculate viewing angles for each eye and cyclop
% determine gazepoint on the monitor plane
E0iGZP=reshape([gazesetB(:).E0GZPonScreen],3,length(pupilsetB))';
E1iGZP=reshape([gazesetB(:).E1GZPonScreen],3,length(pupilsetB))';
cyclopiGZP=reshape([gazesetB(:).cyGZPonScreen],3,length(pupilsetB))';
% calculate the camera origin gaze point vector intersecting on the screen
for i=1:length(markers_used)
    gazesetB(i).origsccamGZPvectonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),[0 0 0],gazesetB(i).GZPpupil);
    gazesetB(i).origsccamE0GZPvectonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),[0 0 0],gazesetB(i).GZPpupil0);
    gazesetB(i).origsccamE1GZPvectonScreen=get_coordinates_of_intersection_point_btw_plane_gazeline(marker5_inscenecam3D_pupilsetB,mon_orient_inscenecam3D_pupilsetB(:,3),[0 0 0],gazesetB(i).GZPpupil1);
end

% homography gaze positions
iGZ_E0_horiz_vis_angle=atand(((E0iGZP-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*monUvect_x_inscenecam)./((E0iGZP-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
iGZ_E0_vert_vis_angle=atand(((E0iGZP-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*monUvect_y_inscenecam)./((E0iGZP-repmat(E0EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
iGZ_E1_horiz_vis_angle=atand(((E1iGZP-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*monUvect_x_inscenecam)./((E1iGZP-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
iGZ_E1_vert_vis_angle=atand(((E1iGZP-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*monUvect_y_inscenecam)./((E1iGZP-repmat(E1EC_inscenecam_calibrationA',N_markers,1))*norm2monitor_inscenecam));
iGZ_cyclop_horiz_vis_angle=atand(((cyclopiGZP-repmat(cyclop_point(:)',N_markers,1))*monUvect_x_inscenecam)./((cyclopiGZP-repmat(cyclop_point(:)',N_markers,1))*norm2monitor_inscenecam));
iGZ_cyclop_vert_vis_angle=atand(((cyclopiGZP-repmat(cyclop_point(:)',N_markers,1))*monUvect_y_inscenecam)./((cyclopiGZP-repmat(cyclop_point(:)',N_markers,1))*norm2monitor_inscenecam));

iGZ_angs_pupilsetB = [ marker_IDvect iGZ_E0_horiz_vis_angle(:) iGZ_E0_vert_vis_angle(:) iGZ_E1_horiz_vis_angle(:) iGZ_E1_vert_vis_angle(:) iGZ_cyclop_horiz_vis_angle iGZ_cyclop_vert_vis_angle];

cyGazeRaysV1=reshape([gazesetB(:).cyGZPonScreen],3,length(pupilsetB))';
GZPOrigRaysV2=reshape([gazesetB(:).GZPpupil],3,length(pupilsetB))';

% error evaluated in scene camera image GZP vs RefPoints vs imarkers
iGZP_pos_scenecamIMG2D_pupilsetB=reshape([gazesetB(:).GZPpupil_scenecam_image],2,length(pupilsetB))';
iE0GZP_pos_scenecamIMG2D_pupilsetB=reshape([gazesetB(:).GZPpupil0_scenecam_image],2,length(pupilsetB))';
iE1GZP_pos_scenecamIMG2D_pupilsetB=reshape([gazesetB(:).GZPpupil1_scenecam_image],2,length(pupilsetB))';
cyGZPonscreeninsccamIMG=round(worldToImage(scenecameraParams,eye(3),[0 0 0],cyGazeRaysV1,'ApplyDistortion',true));
E0GZPonscreeninsccamIMG=round(worldToImage(scenecameraParams,eye(3),[0 0 0],E0iGZP,'ApplyDistortion',true));
E1GZPonscreeninsccamIMG=round(worldToImage(scenecameraParams,eye(3),[0 0 0],E1iGZP,'ApplyDistortion',true));

% GZP vs RefPoints
error_dist_scenecamIMG2D.GZP_RefPoints=vecnorm(iGZP_pos_scenecamIMG2D_pupilsetB-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);
error_dist_scenecamIMG2D.GZP_imarker=vecnorm(iGZP_pos_scenecamIMG2D_pupilsetB-imarker_pos_scenecamIMG2D_pupilsetB(:,2:end),2,2);
error_dist_scenecamIMG2D.RefPoints_imarker=vecnorm(refPoints_pos_scenecamIMG2D_pupilsetB-imarker_pos_scenecamIMG2D_pupilsetB(:,2:end),2,2);
error_dist_scenecamIMG2D.GZP_cyGZPonscreen=vecnorm(iGZP_pos_scenecamIMG2D_pupilsetB-cyGZPonscreeninsccamIMG,2,2);
error_dist_scenecamIMG2D.cyGZPonscreen_RefPoints=vecnorm(cyGZPonscreeninsccamIMG-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);
error_dist_scenecamIMG2D.E0GZP_RefPoints=vecnorm(iE0GZP_pos_scenecamIMG2D_pupilsetB-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);
error_dist_scenecamIMG2D.E1GZP_RefPoints=vecnorm(iE1GZP_pos_scenecamIMG2D_pupilsetB-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);
error_dist_scenecamIMG2D.E0GZPonscreen_RefPoints=vecnorm(E0GZPonscreeninsccamIMG-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);
error_dist_scenecamIMG2D.E1GZPonscreen_RefPoints=vecnorm(E1GZPonscreeninsccamIMG-refPoints_pos_scenecamIMG2D_pupilsetB,2,2);

if ~close_fig
figure;% points in Scene Camera Image
p(1)=plot(iGZP_pos_scenecamIMG2D_pupilsetB(:,1),iGZP_pos_scenecamIMG2D_pupilsetB(:,2),'m.');
hold on;
p(2)=plot(refPoints_pos_scenecamIMG2D_pupilsetB(:,1),refPoints_pos_scenecamIMG2D_pupilsetB(:,2),'g+');
p(3)=plot(imarker_pos_scenecamIMG2D_pupilsetB(:,2),imarker_pos_scenecamIMG2D_pupilsetB(:,3),'k*');
p(4)=plot(iE0GZP_pos_scenecamIMG2D_pupilsetB(:,1),iE0GZP_pos_scenecamIMG2D_pupilsetB(:,2),'r.');
p(5)=plot(iE1GZP_pos_scenecamIMG2D_pupilsetB(:,1),iE1GZP_pos_scenecamIMG2D_pupilsetB(:,2),'b.');
p(6)=plot(cyGZPonscreeninsccamIMG(:,1),cyGZPonscreeninsccamIMG(:,2),'c*');
plot([iGZP_pos_scenecamIMG2D_pupilsetB(:,1) refPoints_pos_scenecamIMG2D_pupilsetB(:,1)]',[iGZP_pos_scenecamIMG2D_pupilsetB(:,2) refPoints_pos_scenecamIMG2D_pupilsetB(:,2)]','c-');
plot([iGZP_pos_scenecamIMG2D_pupilsetB(:,1) cyGZPonscreeninsccamIMG(:,1)]',[iGZP_pos_scenecamIMG2D_pupilsetB(:,2) cyGZPonscreeninsccamIMG(:,2)]','m-');
axis equal
set(gca,'ydir','reverse','ylim',[0 720],'xlim',[0 1280]);
title({'Scene-camera Image ',['Calibration: ' calibrationA.name],['Set: ' set_name ]});
legend(p,{'Gaze Point','Ref Point','Marker Point','E0 Gaze Point','E1 Gaze Point','cyclop Gaze Point projected on Screen along the cyclopian ray ','starting from cyclopian point'});
% calculate the angles between vectors
end
E0GZPOrigRaysV2=reshape([gazesetB(:).GZPpupil0],3,length(pupilsetB))';
E1GZPOrigRaysV2=reshape([gazesetB(:).GZPpupil1],3,length(pupilsetB))';

for i=1:length(markers_used)
    ErrorAngscenecam3D.RefPoints_imarker(i)=abs(angle_btw_2_vectors(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:),imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:4)));
    if ErrorAngscenecam3D.RefPoints_imarker(i)>90
        ErrorAngscenecam3D.RefPoints_imarker(i)=abs(180-ErrorAngscenecam3D.RefPoints_imarker(i));
    end
    ErrorAngscenecam3D.RefPoints_GZP(i)=abs(angle_btw_2_vectors(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:),GZPOrigRaysV2(i,:)));
    if ErrorAngscenecam3D.RefPoints_GZP(i)>90
        ErrorAngscenecam3D.RefPoints_GZP(i)=abs(180-ErrorAngscenecam3D.RefPoints_GZP(i));
    end
    ErrorAngscenecam3D.GZP_imarker(i)=abs(angle_btw_2_vectors(GZPOrigRaysV2(i,:),imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:4)));
    if ErrorAngscenecam3D.GZP_imarker(i)>90
        ErrorAngscenecam3D.GZP_imarker(i)=abs(180-ErrorAngscenecam3D.GZP_imarker(i));
    end
    ErrorAngscenecam3D.RefPoints_E0GZP(i)=abs(angle_btw_2_vectors(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:),E0GZPOrigRaysV2(i,:)));
    if ErrorAngscenecam3D.RefPoints_E0GZP(i)>90
        ErrorAngscenecam3D.RefPoints_E0GZP(i)=abs(180-ErrorAngscenecam3D.RefPoints_E0GZP(i));
    end
    ErrorAngscenecam3D.RefPoints_E1GZP(i)=abs(angle_btw_2_vectors(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:),E1GZPOrigRaysV2(i,:)));
    if ErrorAngscenecam3D.RefPoints_E1GZP(i)>90
        ErrorAngscenecam3D.RefPoints_E1GZP(i)=abs(180-ErrorAngscenecam3D.RefPoints_E1GZP(i));
    end
    ErrorAngscenecam3D.E0GZP_E1GZP(i)=abs(angle_btw_2_vectors(E0GZPOrigRaysV2(i,:),E1GZPOrigRaysV2(i,:)));
    if ErrorAngscenecam3D.E0GZP_E1GZP(i)>90
        ErrorAngscenecam3D.E0GZP_E1GZP(i)=abs(180-ErrorAngscenecam3D.E0GZP_E1GZP(i));
    end
end

% calculate error distance on Screen
origsccamGZPvectonScreen=reshape([gazesetB(:).origsccamGZPvectonScreen],3,length(pupilsetB))';
origsccamE0GZPvectonScreen=reshape([gazesetB(:).origsccamE0GZPvectonScreen],3,length(pupilsetB))';
origsccamE1GZPvectonScreen=reshape([gazesetB(:).origsccamE1GZPvectonScreen],3,length(pupilsetB))';
error_dist_onScreen2D.RefPoints_imarker=vecnorm(refPoints_pos_onScreen_inscenecam3D_pupilsetB-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.GZPorigsccam_RefPoints=vecnorm(origsccamGZPvectonScreen-refPoints_pos_onScreen_inscenecam3D_pupilsetB,2,2);
error_dist_onScreen2D.GZPorigsccam_imarker=vecnorm(origsccamGZPvectonScreen-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E0GZPorigsccam_imarker=vecnorm(origsccamE0GZPvectonScreen-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E1GZPorigsccam_imarker=vecnorm(origsccamE1GZPvectonScreen-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E0GZPorigsccam_E1GZPorigsccam=vecnorm(origsccamE0GZPvectonScreen-origsccamE1GZPvectonScreen,2,2);
error_dist_onScreen2D.cyGZP_imarker=vecnorm(cyclopiGZP-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E0GZP_imarker=vecnorm(E0iGZP-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E1GZP_imarker=vecnorm(E1iGZP-imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:end),2,2);
error_dist_onScreen2D.E0GZP_E1GZP=vecnorm(E0iGZP-E1iGZP,2,2);
error_dist_onScreen2D.GZPorigsccam_cyGZP=vecnorm(origsccamGZPvectonScreen-cyclopiGZP,2,2);
% calculate points on screen in 2D
for i=1:length(markers_used)
    imarkersetB(i).pos_onScreen2D=[imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_x_inscenecam+m_width/2 imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_y_inscenecam+m_height/2];
end
    
if ~close_fig
figure; % points on the screen
for i=1:length(markers_used)
sp(1)=plot(origsccamGZPvectonScreen(i,:)*monUvect_x_inscenecam+m_width/2,origsccamGZPvectonScreen(i,:)*monUvect_y_inscenecam+m_height/2,'m.');
hold on;
sp(2)=plot(origsccamE0GZPvectonScreen(i,:)*monUvect_x_inscenecam+m_width/2,origsccamE0GZPvectonScreen(i,:)*monUvect_y_inscenecam+m_height/2,'r.');
sp(3)=plot(origsccamE1GZPvectonScreen(i,:)*monUvect_x_inscenecam+m_width/2,origsccamE1GZPvectonScreen(i,:)*monUvect_y_inscenecam+m_height/2,'b.');
sp(4)=plot(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:)*monUvect_x_inscenecam+m_width/2,refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:)*monUvect_y_inscenecam+m_height/2,'g+');
sp(5)=plot(imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_x_inscenecam+m_width/2,imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_y_inscenecam+m_height/2,'k*');
sp(6)=plot(cyclopiGZP(i,:)*monUvect_x_inscenecam+m_width/2,cyclopiGZP(i,:)*monUvect_y_inscenecam+m_height/2,'c*');
sp(7)=plot(E0iGZP(i,:)*monUvect_x_inscenecam+m_width/2,E0iGZP(i,:)*monUvect_y_inscenecam+m_height/2,'r*');
sp(8)=plot(E1iGZP(i,:)*monUvect_x_inscenecam+m_width/2,E1iGZP(i,:)*monUvect_y_inscenecam+m_height/2,'b*');
sp(9)=plot([cyclopiGZP(i,:)*monUvect_x_inscenecam+m_width/2 imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_x_inscenecam+m_width/2],[cyclopiGZP(i,:)*monUvect_y_inscenecam+m_height/2 imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:end)*monUvect_y_inscenecam+m_height/2],'m-');
sp(10)=plot([cyclopiGZP(i,:)*monUvect_x_inscenecam+m_width/2 origsccamGZPvectonScreen(i,:)*monUvect_x_inscenecam+m_width/2],[cyclopiGZP(i,:)*monUvect_y_inscenecam+m_height/2 origsccamGZPvectonScreen(i,:)*monUvect_y_inscenecam+m_height/2],'c-');
rectangle('Position',[0 0 m_width m_height])
end
axis equal
set(gca,'ydir','reverse');
title({'Screen Projection in m   ', ['Calibration: ' calibrationA.name],[ 'Set: ' set_name ]});
legend(sp,{'SCcamOrigin2GazePoint Ray intersecting the Screen',...
    'SCcamOrigin2E0GazePoint Ray intersecting the Screen',...
    'SCcamOrigin2E1GazePoint Ray intersecting the Screen',...
    'Unprojected Ref Point vect intersecting the Screen',...
    'Marker Point',...
    'cyclop Gaze Point projected on Screen along the cyclopian ray starting from cyclopian point',...
    'E0 Gaze Point projected on Screen along the E0 gaze ray starting from E0eyeball center',...
    'E1 Gaze Point projected on Screen along the E1 gaze ray starting from E1eyeball center',...
    'Meas. Error',...
    'Paralax Error'});
end
% error in viewing angles

error_view_ang.GZs_RefPoints=iGZ_angs_pupilsetB(:,2:end)-refPoints_angs_pupilsetB(:,2:end); % E0horiz/vert E1horiz/vert cyhoriz/vert
error_view_ang.GZs_imarker=iGZ_angs_pupilsetB(:,2:end)-imarker_angs_pupilsetB(:,2:end); % E0horiz/vert E1horiz/vert cyhoriz/vert
error_view_ang.imarker_RefPoints=imarker_angs_pupilsetB(:,2:end)-refPoints_angs_pupilsetB(:,2:end); % E0horiz/vert E1horiz/vert cyhoriz/vert

% calculate eye centers to scene camera orig to GZP line intersecting the
% screen unit vector
new_E0GZvect=(origsccamGZPvectonScreen-repmat(E0EC_inscenecam_calibrationA',N_markers,1))./vecnorm((origsccamGZPvectonScreen-repmat(E0EC_inscenecam_calibrationA',N_markers,1)),2,2);
new_E1GZvect=(origsccamGZPvectonScreen-repmat(E1EC_inscenecam_calibrationA',N_markers,1))./vecnorm((origsccamGZPvectonScreen-repmat(E1EC_inscenecam_calibrationA',N_markers,1)),2,2);
ideal_E0GZvect=(imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(E0EC_inscenecam_calibrationA',N_markers,1))./vecnorm((imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(E0EC_inscenecam_calibrationA',N_markers,1)),2,2);
ideal_E1GZvect=(imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(E1EC_inscenecam_calibrationA',N_markers,1))./vecnorm((imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(E1EC_inscenecam_calibrationA',N_markers,1)),2,2);
ideal_cyGZvect=(imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(cyclop_point',N_markers,1))./vecnorm((imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2:4)-repmat(cyclop_point',N_markers,1)),2,2);


% evaluate relative error

for i=1:length(markers_used)
    scaleAng.RefPoints_RefPoint5(i)=real(angle_btw_2_vectors(refPoints_pos_onScreen_inscenecam3D_pupilsetB(i,:),refPoints_pos_onScreen_inscenecam3D_pupilsetB(1,:)));
    if scaleAng.RefPoints_RefPoint5(i)>90
        scaleAng.RefPoints_RefPoint5(i)=180-scaleAng.RefPoints_RefPoint5(i);
    end
    
    scaleAng.imarker_imarker5(i)=real(angle_btw_2_vectors(imarker_pos_onScreen_inscenecam3D_pupilsetB(i,2:4),imarker_pos_onScreen_inscenecam3D_pupilsetB(1,2:4)));
    if scaleAng.imarker_imarker5(i)>90
        scaleAng.imarker_imarker5(i)=180-scaleAng.imarker_imarker5(i);
    end
    scaleAng.cyGZ_cyGZ5(i)=real(angle_btw_2_vectors(gazesetB(i).ray2GZPpupil,gazesetB(1).ray2GZPpupil));
    if scaleAng.cyGZ_cyGZ5(i)>90
        scaleAng.cyGZ_cyGZ5(i)=180-scaleAng.cyGZ_cyGZ5(i);
    end
    scaleAng.E0GZ_E0GZ5(i)=real(angle_btw_2_vectors(gazesetB(i).Eye0PUPILGazeNormal,gazesetB(1).Eye0PUPILGazeNormal));
    if scaleAng.E0GZ_E0GZ5(i)>90
        scaleAng.E0GZ_E0GZ5(i)=180-scaleAng.E0GZ_E0GZ5(i);
    end
    scaleAng.E1GZ_E1GZ5(i)=real(angle_btw_2_vectors(gazesetB(i).Eye1PUPILGazeNormal,gazesetB(1).Eye1PUPILGazeNormal));
    if scaleAng.E1GZ_E1GZ5(i)>90
        scaleAng.E1GZ_E1GZ5(i)=180-scaleAng.E1GZ_E1GZ5(i);
    end
    scaleAng.new_E0GZ_E0GZ5(i)=real(angle_btw_2_vectors(new_E0GZvect(i,:),new_E0GZvect(1,:)));
   if scaleAng.new_E0GZ_E0GZ5(i)>90
        scaleAng.new_E0GZ_E0GZ5(i)=180-scaleAng.new_E0GZ_E0GZ5(i);
     end
    scaleAng.new_E1GZ_E1GZ5(i)=real(angle_btw_2_vectors(new_E1GZvect(i,:),new_E1GZvect(1,:)));
   if scaleAng.new_E1GZ_E1GZ5(i)>90
        scaleAng.new_E1GZ_E1GZ5(i)=180-scaleAng.new_E1GZ_E1GZ5(i);
     end
    scaleAng.ideal_E0GZ_E0GZ5(i)=real(angle_btw_2_vectors(ideal_E0GZvect(i,:),ideal_E0GZvect(1,:)));
    if scaleAng.ideal_E0GZ_E0GZ5(i)>90
        scaleAng.ideal_E0GZ_E0GZ5(i)=180-scaleAng.ideal_E0GZ_E0GZ5(i);
    end
    scaleAng.ideal_E1GZ_E1GZ5(i)=real(angle_btw_2_vectors(ideal_E1GZvect(i,:),ideal_E1GZvect(1,:)));
    if scaleAng.ideal_E1GZ_E1GZ5(i)>90
        scaleAng.ideal_E1GZ_E1GZ5(i)=180-scaleAng.ideal_E1GZ_E1GZ5(i);
    end
    scaleAng.ideal_cyGZ_cyGZ5(i)=real(angle_btw_2_vectors(ideal_cyGZvect(i,:),ideal_cyGZvect(1,:)));
    if scaleAng.ideal_cyGZ_cyGZ5(i)>90
        scaleAng.ideal_cyGZ_cyGZ5(i)=180-scaleAng.ideal_cyGZ_cyGZ5(i);
    end
end
[imarker_theta,~,~] = cart2pol(imarker_pos_onScreen_inscenecam3D_pupilsetB(:,2)-imarker_pos_onScreen_inscenecam3D_pupilsetB(1,2),imarker_pos_onScreen_inscenecam3D_pupilsetB(:,3)-imarker_pos_onScreen_inscenecam3D_pupilsetB(1,3),imarker_pos_onScreen_inscenecam3D_pupilsetB(:,4)-imarker_pos_onScreen_inscenecam3D_pupilsetB(1,4));
[E0iGZP_theta,~,~]=cart2pol(E0iGZP(:,1)-E0iGZP(1,1),E0iGZP(:,2)-E0iGZP(1,2),E0iGZP(:,3)-E0iGZP(1,3));
[E1iGZP_theta,~,~]=cart2pol(E1iGZP(:,1)-E1iGZP(1,1),E1iGZP(:,2)-E1iGZP(1,2),E1iGZP(:,3)-E1iGZP(1,3));
[cyclopiGZP_theta,~,~]=cart2pol(cyclopiGZP(:,1)-cyclopiGZP(1,1),cyclopiGZP(:,2)-cyclopiGZP(1,2),cyclopiGZP(:,3)-cyclopiGZP(1,3));
[refPcts_theta,~,~] = cart2pol(refPoints_pos_onScreen_inscenecam3D_pupilsetB(:,1)-refPoints_pos_onScreen_inscenecam3D_pupilsetB(1,1),refPoints_pos_onScreen_inscenecam3D_pupilsetB(:,2)-refPoints_pos_onScreen_inscenecam3D_pupilsetB(1,2),refPoints_pos_onScreen_inscenecam3D_pupilsetB(:,3)-refPoints_pos_onScreen_inscenecam3D_pupilsetB(1,3));
[origsccamGZPvectonScreen_theta,~,~]=cart2pol(origsccamGZPvectonScreen(:,1)-origsccamGZPvectonScreen(1,1),origsccamGZPvectonScreen(:,2)-origsccamGZPvectonScreen(1,2),origsccamGZPvectonScreen(:,3)-origsccamGZPvectonScreen(1,3));

switch set_name
    case {'9 points','original 9 points'}
        star_indx=[1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    case '5 points'
        star_indx=[1 2 1 3 1 4 1 5];
        frame_indx=[2 3 4 5 2];
    case '13 points'
        star_indx=[1 2 1 3 1 4 1 5 1 7 1 8 1 9 1 10 1 11 1 12 1 13 1 14];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    case 'star'
        star_indx=[1 2 1 3 1 5 1 6 1 8 1 9 1 11 1 12];
        frame_indx=[];
    case 'validation'
        star_indx=[1 2 1 3 1 4 1 5 1 7 1 8 1 9 1 10 1 11 1 12 1 13 1 14 1 15 1 16 1 17 1 18 1 20 1 21 1 23 1 24 1 26 1 27 1 29 1 30];
        frame_indx=[2 3 4 7 10 9 8 5 2];
    case {'small_sacc','large_sacc'}
        aha=[1:length(markers_used)]';
        dummy_vect=[ones(length(markers_used)-1,1) aha(2:end)];
        
        star_indx=reshape(dummy_vect',1,2*length(dummy_vect));
        frame_indx=[];
    otherwise
        star_indx=[];
        frame_indx=[];
end

if ~close_fig
figure;% relative errorexpressed as ratio

grey_color=[0.7 0.7 0.7];
subplot(2,3,1)
polarplot(imarker_theta(star_indx),abs(scaleAng.imarker_imarker5(star_indx)'),'k.-','MarkerSize',30,'Color',grey_color,'MarkerEdgeColor',grey_color,'MarkerFaceColor',grey_color)
rlim([0 35])
hold on;

polarplot(imarker_theta(star_indx),abs(scaleAng.E1GZ_E1GZ5(star_indx)'),'b.-','MarkerSize',20)
set(gca,'ThetaDir','clockwise');
subplot(2,3,2)
polarplot(imarker_theta(star_indx),abs(scaleAng.imarker_imarker5(star_indx)'),'k.-','MarkerSize',30,'Color',grey_color,'MarkerEdgeColor',grey_color,'MarkerFaceColor',grey_color)
rlim([0 35])
hold on;

polarplot(imarker_theta(star_indx),abs(scaleAng.cyGZ_cyGZ5(star_indx)'),'m.-','MarkerSize',20)
set(gca,'ThetaDir','clockwise');
subplot(2,3,3)
polarplot(imarker_theta(star_indx),abs(scaleAng.imarker_imarker5(star_indx)'),'k.-','MarkerSize',30,'Color',grey_color,'MarkerEdgeColor',grey_color,'MarkerFaceColor',grey_color)
rlim([0 35])
hold on;
polarplot(imarker_theta(star_indx),abs(scaleAng.E0GZ_E0GZ5(star_indx)'),'r.-','MarkerSize',20)

% polarplot(refPcts_theta(star_indx),abs(scaleAng.RefPoints_RefPoint5(star_indx)'),'g+-')
% polarplot(refPcts_theta(frame_indx),abs(scaleAng.RefPoints_RefPoint5(frame_indx)'),'g+-')
% polarplot(E0iGZP_theta(star_indx),abs(scaleAng.E0GZ_E0GZ5(star_indx)'),'r.-')
% polarplot(E0iGZP_theta(frame_indx),abs(scaleAng.E0GZ_E0GZ5(frame_indx)'),'r.-')
% polarplot(E1iGZP_theta(star_indx),abs(scaleAng.E1GZ_E1GZ5(star_indx)'),'b.-')
% polarplot(E1iGZP_theta(frame_indx),abs(scaleAng.E1GZ_E1GZ5(frame_indx)'),'b.-')
% polarplot(cyclopiGZP_theta(star_indx),abs(scaleAng.cyGZ_cyGZ5(star_indx)'),'c+-')
% polarplot(cyclopiGZP_theta(frame_indx),abs(scaleAng.cyGZ_cyGZ5(frame_indx)'),'c+-')
% polarplot(origsccamGZPvectonScreen_theta(star_indx),abs(scaleAng.new_E0GZ_E0GZ5(star_indx)'),'rx-')
% polarplot(origsccamGZPvectonScreen_theta(frame_indx),abs(scaleAng.new_E0GZ_E0GZ5(frame_indx)'),'rx-')
% polarplot(origsccamGZPvectonScreen_theta(star_indx),abs(scaleAng.new_E1GZ_E1GZ5(star_indx)'),'bx-')
% polarplot(origsccamGZPvectonScreen_theta(frame_indx),abs(scaleAng.new_E1GZ_E1GZ5(frame_indx)'),'bx-')
polarplot(imarker_theta(2),abs(scaleAng.imarker_imarker5(2)'),'k.','MarkerSize',30)
set(gca,'ThetaDir','clockwise');

% figure;
subplot(2,3,4)
polarplot(imarker_theta(star_indx),abs(scaleAng.E1GZ_E1GZ5(star_indx)'./abs(scaleAng.ideal_E1GZ_E1GZ5(star_indx)')),'b.','MarkerSize',20)
rlim([0 2])
set(gca,'ThetaDir','clockwise');
subplot(2,3,5)
polarplot(imarker_theta(star_indx),abs(scaleAng.cyGZ_cyGZ5(star_indx)'./abs(scaleAng.ideal_cyGZ_cyGZ5(star_indx)')),'m.','MarkerSize',20)
rlim([0 2])
set(gca,'ThetaDir','clockwise');
subplot(2,3,6)
polarplot(imarker_theta(star_indx),abs(scaleAng.E0GZ_E0GZ5(star_indx)')./abs(scaleAng.ideal_E0GZ_E0GZ5(star_indx)'),'r.-','MarkerSize',20)
rlim([0 2])
hold on;

% polarplot(refPcts_theta(star_indx),abs(scaleAng.RefPoints_RefPoint5(star_indx)')./abs(scaleAng.imarker_imarker5(star_indx)'),'g+-')
% polarplot(E0iGZP_theta(star_indx),abs(scaleAng.E0GZ_E0GZ5(star_indx)')./abs(scaleAng.ideal_E0GZ_E0GZ5(star_indx)'),'r.-')
% polarplot(E1iGZP_theta(star_indx),abs(scaleAng.E1GZ_E1GZ5(star_indx)'./abs(scaleAng.ideal_E1GZ_E1GZ5(star_indx)')),'b.-')
% polarplot(cyclopiGZP_theta(star_indx),abs(scaleAng.cyGZ_cyGZ5(star_indx)'./abs(scaleAng.ideal_cyGZ_cyGZ5(star_indx)')),'c+-')
% polarplot(origsccamGZPvectonScreen_theta(star_indx),abs(scaleAng.new_E0GZ_E0GZ5(star_indx)')./abs(scaleAng.ideal_E0GZ_E0GZ5(star_indx)'),'rx-')
% polarplot(origsccamGZPvectonScreen_theta(star_indx),abs(scaleAng.new_E1GZ_E1GZ5(star_indx)')./abs(scaleAng.ideal_E1GZ_E1GZ5(star_indx)'),'bx-')
 polarplot(imarker_theta(2),abs(scaleAng.imarker_imarker5(2)')./abs(scaleAng.imarker_imarker5(2)'),'k.','MarkerSize',30)
set(gca,'ThetaDir','clockwise');
end

if ~close_fig
fig_HNDLE=draw_expected_measured_corected_gridsV3calib9(imarker_angs_pupilsetB,refPoints_angs_pupilsetB,iGZ_angs_pupilsetB,set_name);
end

% calculate distance of GZP to eyeline and to cyclopian point and the
% shortest distance between GZP0 and GZP1
error_GZP.shortestdistance=[gazesetB(:).shortest_distanceGPs]';
error_GZP.GZPdist2cyclop_point=[gazesetB(:).GZPdist2cyclop_point]';
error_GZP.GZPdist2eyeline=[gazesetB(:).GZPdist2eyeline]';


